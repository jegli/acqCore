-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, CDC block--
-- --
-------------------------------------------------------------------------------
--
-- unit name: CDC block
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 08/05/2018
--
-- version: 1.0
--
-- description: clock domain crossing synchronizer, works only if input signals frequency is equal of smaller that the output clock (clk_i)
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity CDC_block is
    generic(
            g_WIDTH     : positive := 1    
    );
    port(
        clk_i       : IN std_logic;
        reset_i     : IN std_logic;
        data_in_i   : IN std_logic_vector( g_WIDTH-1 downto 0);
        data_out_o  : OUT std_logic_vector( g_WIDTH-1 downto 0)
     );
end CDC_block;

architecture Behavioral of CDC_block is
    signal s_data_in    : std_logic_vector(data_in_i'range);
begin
    p_CDC:process(clk_i,reset_i)is
    begin
        if(reset_i = '1')then
            s_data_in   <= (others => '0');
            data_out_o  <= (others => '0'); 
        elsif(rising_edge(clk_i))then
            s_data_in  <= data_in_i;
            data_out_o <= s_data_in;
        end if;
    end process;
end Behavioral;
