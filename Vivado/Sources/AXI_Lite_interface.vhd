-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, AXI-Lite interface --
-- --
-------------------------------------------------------------------------------
--
-- unit name: AXI-Lite interface
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 14/03/2019
--
-- version: 1.0
--
-- description: AXI-Lite interface
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use ieee.numeric_std.all;

-- acquisition core package
library work;
use work.ACQ_CORE_PACK.all;

entity AXI_lite_interface is
	generic (	
	    -- Width of S_AXI data bus
        g_S_AXI_DATA_WIDTH    : integer  := 32;
        -- Width of S_AXI address bus
        g_S_AXI_ADDR_WIDTH    : integer  := 32;
		-- Number of buffers
		g_BUFFERS             : integer := 16;
		-- Size of the buffer
		g_BUFFER_SIZE         : integer  := 8192
	);
	port (

		-- Global Clock Signal
		S_AXI_aclk	    : in std_logic;
		-- Global Reset Signal. This Signal is Active LOW
		S_AXI_aresetn	: in std_logic;
		
		-- AXI4-Lite Interface
		
		-- Read address (issued by master, acceped by Slave)
		S_AXI_araddr	: in std_logic_vector(g_S_AXI_ADDR_WIDTH-1 downto 0);
		-- Protection type. This signal indicates the privilege
    	-- and security level of the transaction, and whether the
    	-- transaction is a data access or an instruction access.
		S_AXI_arprot	: in std_logic_vector(2 downto 0);
		-- Read address valid. This signal indicates that the channel
    	-- is signaling valid read address and control information.
		S_AXI_arvalid	: in std_logic;
		-- Read address ready. This signal indicates that the slave is
    	-- ready to accept an address and associated control signals.
		S_AXI_arready	: out std_logic;
		-- Read data (issued by slave)
		S_AXI_rdata	    : out std_logic_vector(g_S_AXI_DATA_WIDTH-1 downto 0);
		-- Read response. This signal indicates the status of the
    	-- read transfer.
		S_AXI_rresp	    : out std_logic_vector(1 downto 0);
		-- Read valid. This signal indicates that the channel is
    		-- signaling the required read data.
		S_AXI_rvalid	: out std_logic;
		-- Read ready. This signal indicates that the master can
    	-- accept the read data and response information.
		S_AXI_rready	: in std_logic;
		
		-- BRAM interface
		BRAM_rd_en_o      : out std_logic;
		BRAM_rd_addr_o    : out std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
		BRAM_rd_select_o  : out std_logic_vector(f_find_width(g_BUFFERS)-1 downto 0);
		BRAM_data_i       : in  std_logic_vector(31 downto 0)
	);
end AXI_lite_interface;

architecture arch_imp of AXI_lite_interface is

    -- constant instantation
    constant c_ADDR_RESCALING  : integer := f_find_width(g_S_AXI_DATA_WIDTH/8);

	-- AXI4LITE signals
	signal axi_araddr	: std_logic_vector(g_S_AXI_ADDR_WIDTH-c_ADDR_RESCALING-1 downto 0);
	signal axi_arready	: std_logic;
	signal axi_rresp	: std_logic_vector(1 downto 0);
	signal axi_rvalid	: std_logic;

    -- signals
    signal s_BRAM_readout_latency   : std_logic;

begin
    -- I/O Connections assignments
    S_AXI_arready      <= axi_arready;
    S_AXI_rdata	       <= BRAM_data_i;
    S_AXI_rresp	       <= axi_rresp;
    S_AXI_rvalid       <= axi_rvalid;
    axi_araddr         <= S_AXI_araddr(S_AXI_araddr'High downto c_ADDR_RESCALING);  -- AXI4-Lite addressing is in bytes whereas RAM addressing is in 32-bit word
    
    -- Implement axi_arready generation
    -- axi_arready is asserted for one S_AXI_aclk clock cycle when
    -- S_AXI_arvalid is asserted. 
    -- The read address is also latched when S_AXI_arvalid is 
    -- asserted. axi_araddr is reset to zero on reset assertion.
    
    process (S_AXI_aclk)
    begin
        if rising_edge(S_AXI_aclk) then 
            if S_AXI_aresetn = '0' then
                axi_arready         <= '0';
                BRAM_rd_en_o        <= '0';
                BRAM_rd_addr_o      <= (others => '0');
                BRAM_rd_select_o    <= (others => '0');  
            else
                if (axi_arready = '0' and S_AXI_arvalid = '1') then
                    -- indicates that the slave has acceped the valid read address
                    axi_arready <= '1';
                    -- Read Address latching 

                    BRAM_rd_en_o       <= '1';  
                    BRAM_rd_addr_o     <= axi_araddr(BRAM_rd_addr_o'range);
                    BRAM_rd_select_o   <= axi_araddr(axi_araddr'HIGH downto BRAM_rd_addr_o'length);  
                else
                    axi_arready <= '0';
                end if;
            end if;
        end if;                   
    end process; 
    
    -- Implement axi_arvalid generation
    -- s_BRAM_readout_latency is asserted for one S_AXI_aclk clock cycle when both 
    -- S_AXI_arvalid and axi_arready are asserted. The slave registers 
    -- data are available on the axi_rdata bus at this instance. The 
    -- assertion of axi_rvalid marks the validity of read data on the 
    -- bus and axi_rresp indicates the status of read transaction.axi_rvalid 
    -- is deasserted on reset (active low). axi_rresp and axi_rdata are 
    -- cleared to zero on reset (active low).  
    process (S_AXI_aclk)
    begin
        if rising_edge(S_AXI_aclk) then
            if S_AXI_aresetn = '0' then
                s_BRAM_readout_latency <= '0';
            else
                if(s_BRAM_readout_latency = '0')then
                    if (axi_arready = '1' and S_AXI_arvalid = '1' and axi_rvalid = '0') then
                        s_BRAM_readout_latency <= '1';
                    end if;
                else
                    s_BRAM_readout_latency <= '0';
                end if;            
            end if;
        end if;
    end process;
    
    -- Implement axi_arvalid generation
    -- axi_rvalid is asserted for one S_AXI_aclk clock cycle when both 
    -- S_AXI_arvalid and axi_arready are asserted. The slave registers 
    -- data are available on the axi_rdata bus at this instance. The 
    -- assertion of axi_rvalid marks the validity of read data on the 
    -- bus and axi_rresp indicates the status of read transaction.axi_rvalid 
    -- is deasserted on reset (active low). axi_rresp and axi_rdata are 
    -- cleared to zero on reset (active low).  
    process (S_AXI_aclk)
    begin
        if rising_edge(S_AXI_aclk) then
            if S_AXI_aresetn = '0' then
                axi_rvalid <= '0';
                axi_rresp  <= "00";
            else
                if (s_BRAM_readout_latency = '1') then
                    -- Valid read data is available at the read data bus
                    axi_rvalid <= '1';
                    axi_rresp  <= "00"; -- 'OKAY' response
                    
                elsif (axi_rvalid = '1' and S_AXI_rready = '1') then
                    -- Read data is accepted by the master
                    axi_rvalid <= '0';
                end if;            
            end if;
        end if;
    end process;
end arch_imp;
