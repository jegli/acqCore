--- VME memory map acqCore
--- Memory map version 20190625
--- Generated on 2019-06-25 by jegli using VHDLMap (Gena component)

library ieee;
use ieee.std_logic_1164.all;
use ieee.NUMERIC_STD.all;


package MemMap_acqCore is

	-- Ident Code
	constant C_acqCore_IdentCode : std_logic_vector(31 downto 0) := X"000000ff";

	-- Memory Map Version
	constant C_acqCore_MemMapVersion : std_logic_vector(31 downto 0) := std_logic_vector(to_unsigned(20190625,32));

	-- Semantic Memory Map Version
	constant C_acqCore_SemanticMemMapVersion : std_logic_vector(31 downto 0) := X"00000001";

	-- Register Addresses : Memory Map
	constant C_Reg_acqCore_control		:		std_logic_vector(9 downto 2) := "00001000";-- : Word address : "0" & X"08"; Byte Address : X"10"
	constant C_Reg_acqCore_status		:		std_logic_vector(9 downto 2) := "00001001";-- : Word address : "0" & X"09"; Byte Address : X"12"
	constant C_Reg_acqCore_faults		:		std_logic_vector(9 downto 2) := "00001010";-- : Word address : "0" & X"0a"; Byte Address : X"14"
	constant C_Reg_acqCore_faultsEnable		:		std_logic_vector(9 downto 2) := "00001011";-- : Word address : "0" & X"0b"; Byte Address : X"16"
	constant C_Reg_acqCore_acqLength0		:		std_logic_vector(9 downto 2) := "00001100";-- : Word address : "0" & X"0c"; Byte Address : X"18"
	constant C_Reg_acqCore_acqLength1		:		std_logic_vector(9 downto 2) := "00001101";-- : Word address : "0" & X"0d"; Byte Address : X"1a"
	constant C_Reg_acqCore_acqLength2		:		std_logic_vector(9 downto 2) := "00001110";-- : Word address : "0" & X"0e"; Byte Address : X"1c"
	constant C_Reg_acqCore_acqLength3		:		std_logic_vector(9 downto 2) := "00001111";-- : Word address : "0" & X"0f"; Byte Address : X"1e"
	constant C_Reg_acqCore_acqLength4		:		std_logic_vector(9 downto 2) := "00010000";-- : Word address : "0" & X"10"; Byte Address : X"20"
	constant C_Reg_acqCore_acqLength5		:		std_logic_vector(9 downto 2) := "00010001";-- : Word address : "0" & X"11"; Byte Address : X"22"
	constant C_Reg_acqCore_acqLength6		:		std_logic_vector(9 downto 2) := "00010010";-- : Word address : "0" & X"12"; Byte Address : X"24"
	constant C_Reg_acqCore_acqLength7		:		std_logic_vector(9 downto 2) := "00010011";-- : Word address : "0" & X"13"; Byte Address : X"26"
	constant C_Reg_acqCore_acqLength8		:		std_logic_vector(9 downto 2) := "00010100";-- : Word address : "0" & X"14"; Byte Address : X"28"
	constant C_Reg_acqCore_acqLength9		:		std_logic_vector(9 downto 2) := "00010101";-- : Word address : "0" & X"15"; Byte Address : X"2a"
	constant C_Reg_acqCore_acqLength10		:		std_logic_vector(9 downto 2) := "00010110";-- : Word address : "0" & X"16"; Byte Address : X"2c"
	constant C_Reg_acqCore_acqLength11		:		std_logic_vector(9 downto 2) := "00010111";-- : Word address : "0" & X"17"; Byte Address : X"2e"
	constant C_Reg_acqCore_acqDecimation0		:		std_logic_vector(9 downto 2) := "00011000";-- : Word address : "0" & X"18"; Byte Address : X"30"
	constant C_Reg_acqCore_acqDecimation1		:		std_logic_vector(9 downto 2) := "00011001";-- : Word address : "0" & X"19"; Byte Address : X"32"
	constant C_Reg_acqCore_acqDecimation2		:		std_logic_vector(9 downto 2) := "00011010";-- : Word address : "0" & X"1a"; Byte Address : X"34"
	constant C_Reg_acqCore_acqDecimation3		:		std_logic_vector(9 downto 2) := "00011011";-- : Word address : "0" & X"1b"; Byte Address : X"36"
	constant C_Reg_acqCore_acqDecimation4		:		std_logic_vector(9 downto 2) := "00011100";-- : Word address : "0" & X"1c"; Byte Address : X"38"
	constant C_Reg_acqCore_acqDecimation5		:		std_logic_vector(9 downto 2) := "00011101";-- : Word address : "0" & X"1d"; Byte Address : X"3a"
	constant C_Reg_acqCore_acqDecimation6		:		std_logic_vector(9 downto 2) := "00011110";-- : Word address : "0" & X"1e"; Byte Address : X"3c"
	constant C_Reg_acqCore_acqDecimation7		:		std_logic_vector(9 downto 2) := "00011111";-- : Word address : "0" & X"1f"; Byte Address : X"3e"
	constant C_Reg_acqCore_acqDecimation8		:		std_logic_vector(9 downto 2) := "00100000";-- : Word address : "0" & X"20"; Byte Address : X"40"
	constant C_Reg_acqCore_acqDecimation9		:		std_logic_vector(9 downto 2) := "00100001";-- : Word address : "0" & X"21"; Byte Address : X"42"
	constant C_Reg_acqCore_acqDecimation10		:		std_logic_vector(9 downto 2) := "00100010";-- : Word address : "0" & X"22"; Byte Address : X"44"
	constant C_Reg_acqCore_acqDecimation11		:		std_logic_vector(9 downto 2) := "00100011";-- : Word address : "0" & X"23"; Byte Address : X"46"
	constant C_Reg_acqCore_bufferStartAddress		:		std_logic_vector(9 downto 2) := "00100100";-- : Word address : "0" & X"24"; Byte Address : X"48"
	constant C_Reg_acqCore_bufferSize		:		std_logic_vector(9 downto 2) := "00100101";-- : Word address : "0" & X"25"; Byte Address : X"4a"

	-- Register Auto Clear Masks : Memory Map
	constant C_ACM_acqCore_control		:		std_logic_vector(31 downto 0) := "00000011111111111100000000000011";-- : Value : X"03ffc003"
	constant C_ACM_acqCore_status		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_faults		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_faultsEnable		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength0		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength1		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength2		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength3		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength4		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength5		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength6		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength7		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength8		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength9		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength10		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqLength11		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation0		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation1		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation2		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation3		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation4		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation5		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation6		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation7		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation8		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation9		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation10		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_acqDecimation11		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_bufferStartAddress		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_ACM_acqCore_bufferSize		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"

	-- Register Preset Masks : Memory Map
	constant C_PSM_acqCore_control		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_status		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_faults		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_faultsEnable		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength0		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength1		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength2		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength3		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength4		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength5		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength6		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength7		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength8		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength9		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength10		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqLength11		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation0		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation1		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation2		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation3		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation4		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation5		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation6		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation7		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation8		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation9		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation10		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_acqDecimation11		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_bufferStartAddress		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"
	constant C_PSM_acqCore_bufferSize		:		std_logic_vector(31 downto 0) := "00000000000000000000000000000000";-- : Value : X"00000000"

	-- CODE FIELDS
	-- Memory Data : Memory Map
	-- Submap Addresses : Memory Map
	constant C_Submap_acqCore_ipInfo		:		std_logic_vector(9 downto 5) := "00000";-- : Word address : "0" & "0" & X"0"; Byte Address : "0" & X"0"
end;
-- EOF