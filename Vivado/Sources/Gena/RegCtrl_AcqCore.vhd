-- VME registers for acqCore
-- Memory map version 20190625
-- Generated on 2019-06-25 16:56:41 by jegli using Gena+ VHDL RegCtrl factory (2018-03-16)

library CommonVisual;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.MemMap_acqCore.all;
use work.MemMap_ipInfo.all;

entity RegCtrl_acqCore is
	port (
		Clk : in std_logic;
		Rst : in std_logic;
		VMEAddr : in std_logic_vector(9 downto 2);
		VMERdData : out std_logic_vector(31 downto 0);
		VMEWrData : in std_logic_vector(31 downto 0);
		VMERdMem : in std_logic;
		VMEWrMem : in std_logic;
		VMERdDone : out std_logic;
		VMEWrDone : out std_logic;
		VMERdError : out std_logic;
		VMEWrError : out std_logic;
		ipInfo_ident : in std_logic_vector(63 downto 0);
		ipInfo_firmwareVersion : in std_logic_vector(31 downto 0);
		ipInfo_memMapVersion : in std_logic_vector(31 downto 0);
		ipInfo_sysControl : out std_logic_vector(15 downto 0);
		control_softReset : out std_logic;
		control_clrFaults : out std_logic;
		control_enable : out std_logic_vector(13 downto 2);
		control_softStartTrig : out std_logic_vector(25 downto 14);
		control_debugMode : out std_logic;
		status_busy : in std_logic;
		status_acqFinished : in std_logic;
		status_noFaults : in std_logic;
		status_noOverflow : in std_logic;
		faults_acqFault : in std_logic;
		faults_SRFF : out std_logic_vector(31 downto 0);
		faults_ClrSRFF : in std_logic;
		faultsEnable_acqFaultEnable : out std_logic;
		acqLength0 : out std_logic_vector(31 downto 0);
		acqLength1 : out std_logic_vector(31 downto 0);
		acqLength2 : out std_logic_vector(31 downto 0);
		acqLength3 : out std_logic_vector(31 downto 0);
		acqLength4 : out std_logic_vector(31 downto 0);
		acqLength5 : out std_logic_vector(31 downto 0);
		acqLength6 : out std_logic_vector(31 downto 0);
		acqLength7 : out std_logic_vector(31 downto 0);
		acqLength8 : out std_logic_vector(31 downto 0);
		acqLength9 : out std_logic_vector(31 downto 0);
		acqLength10 : out std_logic_vector(31 downto 0);
		acqLength11 : out std_logic_vector(31 downto 0);
		acqDecimation0 : out std_logic_vector(31 downto 0);
		acqDecimation1 : out std_logic_vector(31 downto 0);
		acqDecimation2 : out std_logic_vector(31 downto 0);
		acqDecimation3 : out std_logic_vector(31 downto 0);
		acqDecimation4 : out std_logic_vector(31 downto 0);
		acqDecimation5 : out std_logic_vector(31 downto 0);
		acqDecimation6 : out std_logic_vector(31 downto 0);
		acqDecimation7 : out std_logic_vector(31 downto 0);
		acqDecimation8 : out std_logic_vector(31 downto 0);
		acqDecimation9 : out std_logic_vector(31 downto 0);
		acqDecimation10 : out std_logic_vector(31 downto 0);
		acqDecimation11 : out std_logic_vector(31 downto 0);
		bufferStartAddress : out std_logic_vector(31 downto 0);
		bufferStartAddress_WrStrobe : out std_logic;
		bufferSize : in std_logic_vector(31 downto 0)
	);
end;

architecture Gena of RegCtrl_acqCore is
	component RMWReg
		generic (
			N : natural := 8
		);
		port (
			VMEWrData : in std_logic_vector(2*N-1 downto 0);
			Clk : in std_logic;
			AutoClrMsk : in std_logic_vector(N-1 downto 0);
			Rst : in std_logic;
			CRegSel : in std_logic;
			CReg : out std_logic_vector(N-1 downto 0);
			WriteMem : in std_logic;
			Preset : in std_logic_vector(N-1 downto 0)
		);
	end component;

	for all : RMWReg use entity CommonVisual.RMWReg(RMWReg);

	component CtrlRegN
		generic (
			N : integer := 16
		);
		port (
			Clk : in std_logic;
			Rst : in std_logic;
			CRegSel : in std_logic;
			WriteMem : in std_logic;
			VMEWrData : in std_logic_vector(N-1 downto 0);
			AutoClrMsk : in std_logic_vector(N-1 downto 0);
			CReg : out std_logic_vector(N-1 downto 0);
			Preset : in std_logic_vector(N-1 downto 0)
		);
	end component;

	for all : CtrlRegN use entity CommonVisual.CtrlRegN(V1);

	component SRFFxN
		generic (
			N : positive := 16
		);
		port (
			Clk : in std_logic;
			Rst : in std_logic:='0';
			Set : in std_logic_vector(N-1 downto 0);
			Clr : in std_logic:='0';
			Q : out std_logic_vector(N-1 downto 0)
		);
	end component;

	for all : SRFFxN use entity CommonVisual.SRFFxN(V1);

	signal Loc_VMERdMem : std_logic_vector(2 downto 0);
	signal Loc_VMEWrMem : std_logic_vector(1 downto 0);
	signal CRegRdData : std_logic_vector(31 downto 0);
	signal CRegRdOK : std_logic;
	signal CRegWrOK : std_logic;
	signal Loc_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_CRegRdOK : std_logic;
	signal Loc_CRegWrOK : std_logic;
	signal RegRdDone : std_logic;
	signal RegWrDone : std_logic;
	signal RegRdData : std_logic_vector(31 downto 0);
	signal RegRdOK : std_logic;
	signal Loc_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_RegRdOK : std_logic;
	signal MemRdData : std_logic_vector(31 downto 0);
	signal MemRdDone : std_logic;
	signal MemWrDone : std_logic;
	signal Loc_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_MemRdDone : std_logic;
	signal Loc_MemWrDone : std_logic;
	signal RdData : std_logic_vector(31 downto 0);
	signal RdDone : std_logic;
	signal WrDone : std_logic;
	signal RegRdError : std_logic;
	signal RegWrError : std_logic;
	signal MemRdError : std_logic;
	signal MemWrError : std_logic;
	signal Loc_MemRdError : std_logic;
	signal Loc_MemWrError : std_logic;
	signal RdError : std_logic;
	signal WrError : std_logic;
	signal ipInfo_CRegRdData : std_logic_vector(31 downto 0);
	signal ipInfo_CRegRdOK : std_logic;
	signal ipInfo_CRegWrOK : std_logic;
	signal Loc_ipInfo_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_ipInfo_CRegRdOK : std_logic;
	signal Loc_ipInfo_CRegWrOK : std_logic;
	signal ipInfo_RegRdDone : std_logic;
	signal ipInfo_RegWrDone : std_logic;
	signal ipInfo_RegRdData : std_logic_vector(31 downto 0);
	signal ipInfo_RegRdOK : std_logic;
	signal Loc_ipInfo_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_ipInfo_RegRdOK : std_logic;
	signal ipInfo_MemRdData : std_logic_vector(31 downto 0);
	signal ipInfo_MemRdDone : std_logic;
	signal ipInfo_MemWrDone : std_logic;
	signal Loc_ipInfo_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_ipInfo_MemRdDone : std_logic;
	signal Loc_ipInfo_MemWrDone : std_logic;
	signal ipInfo_RdData : std_logic_vector(31 downto 0);
	signal ipInfo_RdDone : std_logic;
	signal ipInfo_WrDone : std_logic;
	signal ipInfo_RegRdError : std_logic;
	signal ipInfo_RegWrError : std_logic;
	signal ipInfo_MemRdError : std_logic;
	signal ipInfo_MemWrError : std_logic;
	signal Loc_ipInfo_MemRdError : std_logic;
	signal Loc_ipInfo_MemWrError : std_logic;
	signal ipInfo_RdError : std_logic;
	signal ipInfo_WrError : std_logic;
	signal Loc_ipInfo_ident : std_logic_vector(63 downto 0);
	signal Loc_ipInfo_firmwareVersion : std_logic_vector(31 downto 0);
	signal Loc_ipInfo_memMapVersion : std_logic_vector(31 downto 0);
	signal Loc_ipInfo_sysControl : std_logic_vector(15 downto 0);
	signal WrSel_ipInfo_sysControl : std_logic;
	signal Loc_control : std_logic_vector(31 downto 0);
	signal WrSel_control : std_logic;
	signal Loc_status : std_logic_vector(31 downto 0);
	signal Loc_faults : std_logic_vector(31 downto 0);
	signal Loc_faults_SRFF : std_logic_vector(31 downto 0);
	signal Loc_faultsEnable : std_logic_vector(31 downto 0);
	signal WrSel_faultsEnable : std_logic;
	signal Loc_acqLength0 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength0 : std_logic;
	signal Loc_acqLength1 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength1 : std_logic;
	signal Loc_acqLength2 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength2 : std_logic;
	signal Loc_acqLength3 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength3 : std_logic;
	signal Loc_acqLength4 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength4 : std_logic;
	signal Loc_acqLength5 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength5 : std_logic;
	signal Loc_acqLength6 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength6 : std_logic;
	signal Loc_acqLength7 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength7 : std_logic;
	signal Loc_acqLength8 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength8 : std_logic;
	signal Loc_acqLength9 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength9 : std_logic;
	signal Loc_acqLength10 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength10 : std_logic;
	signal Loc_acqLength11 : std_logic_vector(31 downto 0);
	signal WrSel_acqLength11 : std_logic;
	signal Loc_acqDecimation0 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation0 : std_logic;
	signal Loc_acqDecimation1 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation1 : std_logic;
	signal Loc_acqDecimation2 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation2 : std_logic;
	signal Loc_acqDecimation3 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation3 : std_logic;
	signal Loc_acqDecimation4 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation4 : std_logic;
	signal Loc_acqDecimation5 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation5 : std_logic;
	signal Loc_acqDecimation6 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation6 : std_logic;
	signal Loc_acqDecimation7 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation7 : std_logic;
	signal Loc_acqDecimation8 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation8 : std_logic;
	signal Loc_acqDecimation9 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation9 : std_logic;
	signal Loc_acqDecimation10 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation10 : std_logic;
	signal Loc_acqDecimation11 : std_logic_vector(31 downto 0);
	signal WrSel_acqDecimation11 : std_logic;
	signal Loc_bufferStartAddress : std_logic_vector(31 downto 0);
	signal WrSel_bufferStartAddress : std_logic;
	signal Loc_bufferSize : std_logic_vector(31 downto 0);
begin
	Reg_control: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_control,
			AutoClrMsk => C_ACM_acqCore_control,
			Preset => C_PSM_acqCore_control,
			CReg => Loc_control(31 downto 0)
		);

	SRFF_faults: SRFFxN
		generic map (
			N => 32
		)
		port map (
			Clk => Clk,
			Rst => Rst,
			Set => Loc_faults,
			Clr => faults_ClrSRFF,
			Q => Loc_faults_SRFF
		);

	Reg_faultsEnable: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_faultsEnable,
			AutoClrMsk => C_ACM_acqCore_faultsEnable,
			Preset => C_PSM_acqCore_faultsEnable,
			CReg => Loc_faultsEnable(31 downto 0)
		);

	Reg_acqLength0: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength0,
			AutoClrMsk => C_ACM_acqCore_acqLength0,
			Preset => C_PSM_acqCore_acqLength0,
			CReg => Loc_acqLength0(31 downto 0)
		);

	Reg_acqLength1: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength1,
			AutoClrMsk => C_ACM_acqCore_acqLength1,
			Preset => C_PSM_acqCore_acqLength1,
			CReg => Loc_acqLength1(31 downto 0)
		);

	Reg_acqLength2: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength2,
			AutoClrMsk => C_ACM_acqCore_acqLength2,
			Preset => C_PSM_acqCore_acqLength2,
			CReg => Loc_acqLength2(31 downto 0)
		);

	Reg_acqLength3: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength3,
			AutoClrMsk => C_ACM_acqCore_acqLength3,
			Preset => C_PSM_acqCore_acqLength3,
			CReg => Loc_acqLength3(31 downto 0)
		);

	Reg_acqLength4: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength4,
			AutoClrMsk => C_ACM_acqCore_acqLength4,
			Preset => C_PSM_acqCore_acqLength4,
			CReg => Loc_acqLength4(31 downto 0)
		);

	Reg_acqLength5: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength5,
			AutoClrMsk => C_ACM_acqCore_acqLength5,
			Preset => C_PSM_acqCore_acqLength5,
			CReg => Loc_acqLength5(31 downto 0)
		);

	Reg_acqLength6: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength6,
			AutoClrMsk => C_ACM_acqCore_acqLength6,
			Preset => C_PSM_acqCore_acqLength6,
			CReg => Loc_acqLength6(31 downto 0)
		);

	Reg_acqLength7: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength7,
			AutoClrMsk => C_ACM_acqCore_acqLength7,
			Preset => C_PSM_acqCore_acqLength7,
			CReg => Loc_acqLength7(31 downto 0)
		);

	Reg_acqLength8: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength8,
			AutoClrMsk => C_ACM_acqCore_acqLength8,
			Preset => C_PSM_acqCore_acqLength8,
			CReg => Loc_acqLength8(31 downto 0)
		);

	Reg_acqLength9: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength9,
			AutoClrMsk => C_ACM_acqCore_acqLength9,
			Preset => C_PSM_acqCore_acqLength9,
			CReg => Loc_acqLength9(31 downto 0)
		);

	Reg_acqLength10: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength10,
			AutoClrMsk => C_ACM_acqCore_acqLength10,
			Preset => C_PSM_acqCore_acqLength10,
			CReg => Loc_acqLength10(31 downto 0)
		);

	Reg_acqLength11: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqLength11,
			AutoClrMsk => C_ACM_acqCore_acqLength11,
			Preset => C_PSM_acqCore_acqLength11,
			CReg => Loc_acqLength11(31 downto 0)
		);

	Reg_acqDecimation0: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation0,
			AutoClrMsk => C_ACM_acqCore_acqDecimation0,
			Preset => C_PSM_acqCore_acqDecimation0,
			CReg => Loc_acqDecimation0(31 downto 0)
		);

	Reg_acqDecimation1: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation1,
			AutoClrMsk => C_ACM_acqCore_acqDecimation1,
			Preset => C_PSM_acqCore_acqDecimation1,
			CReg => Loc_acqDecimation1(31 downto 0)
		);

	Reg_acqDecimation2: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation2,
			AutoClrMsk => C_ACM_acqCore_acqDecimation2,
			Preset => C_PSM_acqCore_acqDecimation2,
			CReg => Loc_acqDecimation2(31 downto 0)
		);

	Reg_acqDecimation3: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation3,
			AutoClrMsk => C_ACM_acqCore_acqDecimation3,
			Preset => C_PSM_acqCore_acqDecimation3,
			CReg => Loc_acqDecimation3(31 downto 0)
		);

	Reg_acqDecimation4: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation4,
			AutoClrMsk => C_ACM_acqCore_acqDecimation4,
			Preset => C_PSM_acqCore_acqDecimation4,
			CReg => Loc_acqDecimation4(31 downto 0)
		);

	Reg_acqDecimation5: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation5,
			AutoClrMsk => C_ACM_acqCore_acqDecimation5,
			Preset => C_PSM_acqCore_acqDecimation5,
			CReg => Loc_acqDecimation5(31 downto 0)
		);

	Reg_acqDecimation6: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation6,
			AutoClrMsk => C_ACM_acqCore_acqDecimation6,
			Preset => C_PSM_acqCore_acqDecimation6,
			CReg => Loc_acqDecimation6(31 downto 0)
		);

	Reg_acqDecimation7: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation7,
			AutoClrMsk => C_ACM_acqCore_acqDecimation7,
			Preset => C_PSM_acqCore_acqDecimation7,
			CReg => Loc_acqDecimation7(31 downto 0)
		);

	Reg_acqDecimation8: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation8,
			AutoClrMsk => C_ACM_acqCore_acqDecimation8,
			Preset => C_PSM_acqCore_acqDecimation8,
			CReg => Loc_acqDecimation8(31 downto 0)
		);

	Reg_acqDecimation9: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation9,
			AutoClrMsk => C_ACM_acqCore_acqDecimation9,
			Preset => C_PSM_acqCore_acqDecimation9,
			CReg => Loc_acqDecimation9(31 downto 0)
		);

	Reg_acqDecimation10: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation10,
			AutoClrMsk => C_ACM_acqCore_acqDecimation10,
			Preset => C_PSM_acqCore_acqDecimation10,
			CReg => Loc_acqDecimation10(31 downto 0)
		);

	Reg_acqDecimation11: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_acqDecimation11,
			AutoClrMsk => C_ACM_acqCore_acqDecimation11,
			Preset => C_PSM_acqCore_acqDecimation11,
			CReg => Loc_acqDecimation11(31 downto 0)
		);

	Reg_bufferStartAddress: CtrlRegN
		generic map (
			N => 32
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_bufferStartAddress,
			AutoClrMsk => C_ACM_acqCore_bufferStartAddress,
			Preset => C_PSM_acqCore_bufferStartAddress,
			CReg => Loc_bufferStartAddress(31 downto 0)
		);

	control_softReset <= Loc_control(0);
	control_clrFaults <= Loc_control(1);
	control_enable <= Loc_control(13 downto 2);
	control_softStartTrig <= Loc_control(25 downto 14);
	control_debugMode <= Loc_control(26);

	Loc_status(31 downto 16) <= C_PSM_acqCore_status(31 downto 16);
	Loc_status(15) <= status_busy;
	Loc_status(14) <= status_acqFinished;
	Loc_status(13) <= status_noFaults;
	Loc_status(12) <= status_noOverflow;
	Loc_status(11 downto 0) <= C_PSM_acqCore_status(11 downto 0);

	faults_SRFF <= Loc_faults_SRFF;
	Loc_faults(31 downto 1) <= C_PSM_acqCore_faults(31 downto 1);
	Loc_faults(0) <= faults_acqFault;

	faultsEnable_acqFaultEnable <= Loc_faultsEnable(0);

	acqLength0 <= Loc_acqLength0;

	acqLength1 <= Loc_acqLength1;

	acqLength2 <= Loc_acqLength2;

	acqLength3 <= Loc_acqLength3;

	acqLength4 <= Loc_acqLength4;

	acqLength5 <= Loc_acqLength5;

	acqLength6 <= Loc_acqLength6;

	acqLength7 <= Loc_acqLength7;

	acqLength8 <= Loc_acqLength8;

	acqLength9 <= Loc_acqLength9;

	acqLength10 <= Loc_acqLength10;

	acqLength11 <= Loc_acqLength11;

	acqDecimation0 <= Loc_acqDecimation0;

	acqDecimation1 <= Loc_acqDecimation1;

	acqDecimation2 <= Loc_acqDecimation2;

	acqDecimation3 <= Loc_acqDecimation3;

	acqDecimation4 <= Loc_acqDecimation4;

	acqDecimation5 <= Loc_acqDecimation5;

	acqDecimation6 <= Loc_acqDecimation6;

	acqDecimation7 <= Loc_acqDecimation7;

	acqDecimation8 <= Loc_acqDecimation8;

	acqDecimation9 <= Loc_acqDecimation9;

	acqDecimation10 <= Loc_acqDecimation10;

	acqDecimation11 <= Loc_acqDecimation11;

	bufferStartAddress <= Loc_bufferStartAddress;
	bufferStartAddress_WrStrobe <= WrSel_bufferStartAddress and RegWrDone;

	Loc_bufferSize <= bufferSize;

	WrSelDec: process (VMEAddr) begin
		WrSel_control <= '0';
		WrSel_faultsEnable <= '0';
		WrSel_acqLength0 <= '0';
		WrSel_acqLength1 <= '0';
		WrSel_acqLength2 <= '0';
		WrSel_acqLength3 <= '0';
		WrSel_acqLength4 <= '0';
		WrSel_acqLength5 <= '0';
		WrSel_acqLength6 <= '0';
		WrSel_acqLength7 <= '0';
		WrSel_acqLength8 <= '0';
		WrSel_acqLength9 <= '0';
		WrSel_acqLength10 <= '0';
		WrSel_acqLength11 <= '0';
		WrSel_acqDecimation0 <= '0';
		WrSel_acqDecimation1 <= '0';
		WrSel_acqDecimation2 <= '0';
		WrSel_acqDecimation3 <= '0';
		WrSel_acqDecimation4 <= '0';
		WrSel_acqDecimation5 <= '0';
		WrSel_acqDecimation6 <= '0';
		WrSel_acqDecimation7 <= '0';
		WrSel_acqDecimation8 <= '0';
		WrSel_acqDecimation9 <= '0';
		WrSel_acqDecimation10 <= '0';
		WrSel_acqDecimation11 <= '0';
		WrSel_bufferStartAddress <= '0';
		case VMEAddr(9 downto 2) is
			when C_Reg_acqCore_control =>
				WrSel_control <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_faultsEnable =>
				WrSel_faultsEnable <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength0 =>
				WrSel_acqLength0 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength1 =>
				WrSel_acqLength1 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength2 =>
				WrSel_acqLength2 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength3 =>
				WrSel_acqLength3 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength4 =>
				WrSel_acqLength4 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength5 =>
				WrSel_acqLength5 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength6 =>
				WrSel_acqLength6 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength7 =>
				WrSel_acqLength7 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength8 =>
				WrSel_acqLength8 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength9 =>
				WrSel_acqLength9 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength10 =>
				WrSel_acqLength10 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqLength11 =>
				WrSel_acqLength11 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation0 =>
				WrSel_acqDecimation0 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation1 =>
				WrSel_acqDecimation1 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation2 =>
				WrSel_acqDecimation2 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation3 =>
				WrSel_acqDecimation3 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation4 =>
				WrSel_acqDecimation4 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation5 =>
				WrSel_acqDecimation5 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation6 =>
				WrSel_acqDecimation6 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation7 =>
				WrSel_acqDecimation7 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation8 =>
				WrSel_acqDecimation8 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation9 =>
				WrSel_acqDecimation9 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation10 =>
				WrSel_acqDecimation10 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_acqDecimation11 =>
				WrSel_acqDecimation11 <= '1';
				Loc_CRegWrOK <= '1';
			when C_Reg_acqCore_bufferStartAddress =>
				WrSel_bufferStartAddress <= '1';
				Loc_CRegWrOK <= '1';
			when others =>
				Loc_CRegWrOK <= '0';
		end case;
	end process WrSelDec;

	CRegRdMux: process (VMEAddr, Loc_control, Loc_faultsEnable, Loc_acqLength0, Loc_acqLength1, Loc_acqLength2, Loc_acqLength3, Loc_acqLength4, Loc_acqLength5, Loc_acqLength6, Loc_acqLength7, Loc_acqLength8, Loc_acqLength9, Loc_acqLength10, Loc_acqLength11, Loc_acqDecimation0, Loc_acqDecimation1, Loc_acqDecimation2, Loc_acqDecimation3, Loc_acqDecimation4, Loc_acqDecimation5, Loc_acqDecimation6, Loc_acqDecimation7, Loc_acqDecimation8, Loc_acqDecimation9, Loc_acqDecimation10, Loc_acqDecimation11, Loc_bufferStartAddress) begin
		case VMEAddr(9 downto 2) is
			when C_Reg_acqCore_control =>
				Loc_CRegRdData <= Loc_control(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_faultsEnable =>
				Loc_CRegRdData <= Loc_faultsEnable(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength0 =>
				Loc_CRegRdData <= Loc_acqLength0(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength1 =>
				Loc_CRegRdData <= Loc_acqLength1(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength2 =>
				Loc_CRegRdData <= Loc_acqLength2(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength3 =>
				Loc_CRegRdData <= Loc_acqLength3(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength4 =>
				Loc_CRegRdData <= Loc_acqLength4(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength5 =>
				Loc_CRegRdData <= Loc_acqLength5(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength6 =>
				Loc_CRegRdData <= Loc_acqLength6(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength7 =>
				Loc_CRegRdData <= Loc_acqLength7(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength8 =>
				Loc_CRegRdData <= Loc_acqLength8(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength9 =>
				Loc_CRegRdData <= Loc_acqLength9(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength10 =>
				Loc_CRegRdData <= Loc_acqLength10(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqLength11 =>
				Loc_CRegRdData <= Loc_acqLength11(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation0 =>
				Loc_CRegRdData <= Loc_acqDecimation0(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation1 =>
				Loc_CRegRdData <= Loc_acqDecimation1(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation2 =>
				Loc_CRegRdData <= Loc_acqDecimation2(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation3 =>
				Loc_CRegRdData <= Loc_acqDecimation3(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation4 =>
				Loc_CRegRdData <= Loc_acqDecimation4(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation5 =>
				Loc_CRegRdData <= Loc_acqDecimation5(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation6 =>
				Loc_CRegRdData <= Loc_acqDecimation6(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation7 =>
				Loc_CRegRdData <= Loc_acqDecimation7(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation8 =>
				Loc_CRegRdData <= Loc_acqDecimation8(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation9 =>
				Loc_CRegRdData <= Loc_acqDecimation9(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation10 =>
				Loc_CRegRdData <= Loc_acqDecimation10(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_acqDecimation11 =>
				Loc_CRegRdData <= Loc_acqDecimation11(31 downto 0);
				Loc_CRegRdOK <= '1';
			when C_Reg_acqCore_bufferStartAddress =>
				Loc_CRegRdData <= Loc_bufferStartAddress(31 downto 0);
				Loc_CRegRdOK <= '1';
			when others =>
				Loc_CRegRdData <= (others => '0');
				Loc_CRegRdOK <= '0';
		end case;
	end process CRegRdMux;

	CRegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			CRegRdData <= Loc_CRegRdData;
			CRegRdOK <= Loc_CRegRdOK;
			CRegWrOK <= Loc_CRegWrOk;
		end if;
	end process CRegRdMux_DFF;

	RegRdMux: process (VMEAddr, CRegRdData, CRegRdOK, Loc_status, Loc_faults_SRFF, Loc_bufferSize) begin
		case VMEAddr(9 downto 2) is
			when C_Reg_acqCore_status =>
				Loc_RegRdData <= Loc_status(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_acqCore_faults =>
				Loc_RegRdData <= Loc_faults_SRFF(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_acqCore_bufferSize =>
				Loc_RegRdData <= Loc_bufferSize(31 downto 0);
				Loc_RegRdOK <= '1';
			when others =>
				Loc_RegRdData <= CRegRdData;
				Loc_RegRdOK <= CRegRdOK;
		end case;
	end process RegRdMux;

	RegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			RegRdData <= Loc_RegRdData;
			RegRdOK <= Loc_RegRdOK;
		end if;
	end process RegRdMux_DFF;

	RegRdDone <= Loc_VMERdMem(2) and RegRdOK;
	RegWrDone <= Loc_VMEWrMem(1) and CRegWrOK;

	RegRdError <= Loc_VMERdMem(2) and not RegRdOK;
	RegWrError <= Loc_VMEWrMem(1) and not CRegWrOK;

	Loc_MemRdData <= RegRdData;
	Loc_MemRdDone <= RegRdDone;
	Loc_MemRdError <= RegRdError;

	MemRdData <= Loc_MemRdData;
	MemRdDone <= Loc_MemRdDone;
	MemRdError <= Loc_MemRdError;

	Loc_MemWrDone <= RegWrDone;
	Loc_MemWrError <= RegWrError;

	MemWrDone <= Loc_MemWrDone;
	MemWrError <= Loc_MemWrError;

	AreaRdMux: process (VMEAddr, MemRdData, MemRdDone, ipInfo_RdData, ipInfo_RdDone, ipInfo_RdError, MemRdError) begin
		if VMEAddr(9 downto 5) = C_Submap_acqCore_ipInfo then
			RdData <= ipInfo_RdData;
			RdDone <= ipInfo_RdDone;
			RdError <= ipInfo_RdError;
		else
			RdData <= MemRdData;
			RdDone <= MemRdDone;
			RdError <= MemRdError;
		end if;
	end process AreaRdMux;

	AreaWrMux: process (VMEAddr, MemWrDone, ipInfo_WrDone, ipInfo_WrError, MemWrError) begin
		if VMEAddr(9 downto 5) = C_Submap_acqCore_ipInfo then
			WrDone <= ipInfo_WrDone;
			WrError <= ipInfo_WrError;
		else
			WrDone <= MemWrDone;
			WrError <= MemWrError;
		end if;
	end process AreaWrMux;


	Reg_ipInfo_sysControl: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_ipInfo_sysControl,
			AutoClrMsk => C_ACM_ipInfo_sysControl,
			Preset => C_PSM_ipInfo_sysControl,
			CReg => Loc_ipInfo_sysControl(15 downto 0)
		);

	Loc_ipInfo_ident <= ipInfo_ident;

	Loc_ipInfo_firmwareVersion <= ipInfo_firmwareVersion;

	Loc_ipInfo_memMapVersion <= ipInfo_memMapVersion;

	ipInfo_sysControl <= Loc_ipInfo_sysControl;

	ipInfo_WrSelDec: process (VMEAddr) begin
		WrSel_ipInfo_sysControl <= '0';
		if VMEAddr(9 downto 5) = C_Submap_acqCore_ipInfo then
			case VMEAddr(4 downto 2) is
				when C_Reg_ipInfo_sysControl =>
					WrSel_ipInfo_sysControl <= '1';
					Loc_ipInfo_CRegWrOK <= '1';
				when others =>
					Loc_ipInfo_CRegWrOK <= '0';
			end case;
		else
			Loc_ipInfo_CRegWrOK <= '0';
		end if;
	end process ipInfo_WrSelDec;

	ipInfo_CRegRdMux: process (VMEAddr, Loc_ipInfo_sysControl) begin
		if VMEAddr(9 downto 5) = C_Submap_acqCore_ipInfo then
			case VMEAddr(4 downto 2) is
				when C_Reg_ipInfo_sysControl =>
					Loc_ipInfo_CRegRdData <= std_logic_vector(resize(unsigned(Loc_ipInfo_sysControl(15 downto 0)), 32));
					Loc_ipInfo_CRegRdOK <= '1';
				when others =>
					Loc_ipInfo_CRegRdData <= (others => '0');
					Loc_ipInfo_CRegRdOK <= '0';
			end case;
		else
			Loc_ipInfo_CRegRdData <= (others => '0');
			Loc_ipInfo_CRegRdOK <= '0';
		end if;
	end process ipInfo_CRegRdMux;

	ipInfo_CRegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			ipInfo_CRegRdData <= Loc_ipInfo_CRegRdData;
			ipInfo_CRegRdOK <= Loc_ipInfo_CRegRdOK;
			ipInfo_CRegWrOK <= Loc_ipInfo_CRegWrOk;
		end if;
	end process ipInfo_CRegRdMux_DFF;

	ipInfo_RegRdMux: process (VMEAddr, ipInfo_CRegRdData, ipInfo_CRegRdOK, Loc_ipInfo_ident, Loc_ipInfo_firmwareVersion, Loc_ipInfo_memMapVersion) begin
		if VMEAddr(9 downto 5) = C_Submap_acqCore_ipInfo then
			case VMEAddr(4 downto 2) is
				when C_Reg_ipInfo_ident_1 =>
					Loc_ipInfo_RegRdData <= Loc_ipInfo_ident(63 downto 32);
					Loc_ipInfo_RegRdOK <= '1';
				when C_Reg_ipInfo_ident_0 =>
					Loc_ipInfo_RegRdData <= Loc_ipInfo_ident(31 downto 0);
					Loc_ipInfo_RegRdOK <= '1';
				when C_Reg_ipInfo_firmwareVersion =>
					Loc_ipInfo_RegRdData <= Loc_ipInfo_firmwareVersion(31 downto 0);
					Loc_ipInfo_RegRdOK <= '1';
				when C_Reg_ipInfo_memMapVersion =>
					Loc_ipInfo_RegRdData <= Loc_ipInfo_memMapVersion(31 downto 0);
					Loc_ipInfo_RegRdOK <= '1';
				when others =>
					Loc_ipInfo_RegRdData <= ipInfo_CRegRdData;
					Loc_ipInfo_RegRdOK <= ipInfo_CRegRdOK;
			end case;
		else
			Loc_ipInfo_RegRdData <= ipInfo_CRegRdData;
			Loc_ipInfo_RegRdOK <= ipInfo_CRegRdOK;
		end if;
	end process ipInfo_RegRdMux;

	ipInfo_RegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			ipInfo_RegRdData <= Loc_ipInfo_RegRdData;
			ipInfo_RegRdOK <= Loc_ipInfo_RegRdOK;
		end if;
	end process ipInfo_RegRdMux_DFF;

	ipInfo_RegRdDone <= Loc_VMERdMem(2) and ipInfo_RegRdOK;
	ipInfo_RegWrDone <= Loc_VMEWrMem(1) and ipInfo_CRegWrOK;

	ipInfo_RegRdError <= Loc_VMERdMem(2) and not ipInfo_RegRdOK;
	ipInfo_RegWrError <= Loc_VMEWrMem(1) and not ipInfo_CRegWrOK;

	Loc_ipInfo_MemRdData <= ipInfo_RegRdData;
	Loc_ipInfo_MemRdDone <= ipInfo_RegRdDone;
	Loc_ipInfo_MemRdError <= ipInfo_RegRdError;

	ipInfo_MemRdData <= Loc_ipInfo_MemRdData;
	ipInfo_MemRdDone <= Loc_ipInfo_MemRdDone;
	ipInfo_MemRdError <= Loc_ipInfo_MemRdError;

	Loc_ipInfo_MemWrDone <= ipInfo_RegWrDone;
	Loc_ipInfo_MemWrError <= ipInfo_RegWrError;

	ipInfo_MemWrDone <= Loc_ipInfo_MemWrDone;
	ipInfo_MemWrError <= Loc_ipInfo_MemWrError;

	ipInfo_RdData <= ipInfo_MemRdData;
	ipInfo_RdDone <= ipInfo_MemRdDone;
	ipInfo_WrDone <= ipInfo_MemWrDone;
	ipInfo_RdError <= ipInfo_MemRdError;
	ipInfo_WrError <= ipInfo_MemWrError;

	StrobeSeq: process (Clk) begin
		if rising_edge(Clk) then
			Loc_VMERdMem(Loc_VMERdMem'high downto 0) <= Loc_VMERdMem(Loc_VMERdMem'high-1 downto 0) & VMERdMem;
			Loc_VMEWrMem(Loc_VMEWrMem'high downto 0) <= Loc_VMEWrMem(Loc_VMEWrMem'high-1 downto 0) & VMEWrMem;
		end if;
	end process StrobeSeq;

	VMERdData <= RdData;
	VMERdDone <= RdDone;
	VMEWrDone <= WrDone;
	VMERdError <= RdError;
	VMEWrError <= WrError;

end;

-- EOF
