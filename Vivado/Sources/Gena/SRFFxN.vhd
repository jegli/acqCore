library ieee;
use ieee.std_logic_1164.all;

entity SRFFxN is
  generic(
    N   : positive:=16
  );     --Bus width
  port (
    Clk : in  std_logic;
    Rst : in  std_logic  := '0';               -- Synchronous Reset active high, port can be left open if not used.
    Set : in  std_logic_vector(N-1 downto 0);  -- Individual Set for all bits
    Clr : in  std_logic  := '0';               -- Global Clear
    Q   : out std_logic_vector(N-1 downto 0)   -- Output vector
  );
end entity;  -- SRFFxN

architecture V1 of SRFFxN is
begin

process(Clk,Rst)
  begin
    if rising_edge(Clk) then
      -- Reset
      if Rst='1' then
        Q <= (others => '0');

      else
        for j in 0 to N-1 loop
          -- Set
          if Set(j) = '1' then
            Q(j) <= '1';

          -- Clear
          elsif Clr = '1' then
            Q(j) <= '0';

          end if;
        end loop;
      end if;
    end if;
  end process;

end architecture;  -- behavioral
