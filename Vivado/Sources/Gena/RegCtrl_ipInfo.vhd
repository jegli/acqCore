-- VME registers for ipInfo
-- Memory map version 20190527
-- Generated on 2019-05-28 14:58:52 by jegli using Gena+ VHDL RegCtrl factory (2018-03-16)

library CommonVisual;

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.MemMap_ipInfo.all;

entity RegCtrl_ipInfo is
	port (
		Clk : in std_logic;
		Rst : in std_logic;
		VMEAddr : in std_logic_vector(4 downto 2);
		VMERdData : out std_logic_vector(31 downto 0);
		VMEWrData : in std_logic_vector(31 downto 0);
		VMERdMem : in std_logic;
		VMEWrMem : in std_logic;
		VMERdDone : out std_logic;
		VMEWrDone : out std_logic;
		VMERdError : out std_logic;
		VMEWrError : out std_logic;
		ident : in std_logic_vector(63 downto 0);
		firmwareVersion : in std_logic_vector(31 downto 0);
		memMapVersion : in std_logic_vector(31 downto 0);
		sysControl : out std_logic_vector(15 downto 0)
	);
end;

architecture Gena of RegCtrl_ipInfo is
	component RMWReg
		generic (
			N : natural := 8
		);
		port (
			VMEWrData : in std_logic_vector(2*N-1 downto 0);
			Clk : in std_logic;
			AutoClrMsk : in std_logic_vector(N-1 downto 0);
			Rst : in std_logic;
			CRegSel : in std_logic;
			CReg : out std_logic_vector(N-1 downto 0);
			WriteMem : in std_logic;
			Preset : in std_logic_vector(N-1 downto 0)
		);
	end component;

	for all : RMWReg use entity CommonVisual.RMWReg(RMWReg);

	signal Loc_VMERdMem : std_logic_vector(2 downto 0);
	signal Loc_VMEWrMem : std_logic_vector(1 downto 0);
	signal CRegRdData : std_logic_vector(31 downto 0);
	signal CRegRdOK : std_logic;
	signal CRegWrOK : std_logic;
	signal Loc_CRegRdData : std_logic_vector(31 downto 0);
	signal Loc_CRegRdOK : std_logic;
	signal Loc_CRegWrOK : std_logic;
	signal RegRdDone : std_logic;
	signal RegWrDone : std_logic;
	signal RegRdData : std_logic_vector(31 downto 0);
	signal RegRdOK : std_logic;
	signal Loc_RegRdData : std_logic_vector(31 downto 0);
	signal Loc_RegRdOK : std_logic;
	signal MemRdData : std_logic_vector(31 downto 0);
	signal MemRdDone : std_logic;
	signal MemWrDone : std_logic;
	signal Loc_MemRdData : std_logic_vector(31 downto 0);
	signal Loc_MemRdDone : std_logic;
	signal Loc_MemWrDone : std_logic;
	signal RdData : std_logic_vector(31 downto 0);
	signal RdDone : std_logic;
	signal WrDone : std_logic;
	signal RegRdError : std_logic;
	signal RegWrError : std_logic;
	signal MemRdError : std_logic;
	signal MemWrError : std_logic;
	signal Loc_MemRdError : std_logic;
	signal Loc_MemWrError : std_logic;
	signal RdError : std_logic;
	signal WrError : std_logic;
	signal Loc_ident : std_logic_vector(63 downto 0);
	signal Loc_firmwareVersion : std_logic_vector(31 downto 0);
	signal Loc_memMapVersion : std_logic_vector(31 downto 0);
	signal Loc_sysControl : std_logic_vector(15 downto 0);
	signal WrSel_sysControl : std_logic;
begin
	Reg_sysControl: RMWReg
		generic map (
			N => 16
		)
		port map (
			VMEWrData => VMEWrData(31 downto 0),
			Clk => Clk,
			Rst => Rst,
			WriteMem => VMEWrMem,
			CRegSel => WrSel_sysControl,
			AutoClrMsk => C_ACM_ipInfo_sysControl,
			Preset => C_PSM_ipInfo_sysControl,
			CReg => Loc_sysControl(15 downto 0)
		);

	Loc_ident <= ident;

	Loc_firmwareVersion <= firmwareVersion;

	Loc_memMapVersion <= memMapVersion;

	sysControl <= Loc_sysControl;

	WrSelDec: process (VMEAddr) begin
		WrSel_sysControl <= '0';
		case VMEAddr(4 downto 2) is
			when C_Reg_ipInfo_sysControl =>
				WrSel_sysControl <= '1';
				Loc_CRegWrOK <= '1';
			when others =>
				Loc_CRegWrOK <= '0';
		end case;
	end process WrSelDec;

	CRegRdMux: process (VMEAddr, Loc_sysControl) begin
		case VMEAddr(4 downto 2) is
			when C_Reg_ipInfo_sysControl =>
				Loc_CRegRdData <= std_logic_vector(resize(unsigned(Loc_sysControl(15 downto 0)), 32));
				Loc_CRegRdOK <= '1';
			when others =>
				Loc_CRegRdData <= (others => '0');
				Loc_CRegRdOK <= '0';
		end case;
	end process CRegRdMux;

	CRegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			CRegRdData <= Loc_CRegRdData;
			CRegRdOK <= Loc_CRegRdOK;
			CRegWrOK <= Loc_CRegWrOk;
		end if;
	end process CRegRdMux_DFF;

	RegRdMux: process (VMEAddr, CRegRdData, CRegRdOK, Loc_ident, Loc_firmwareVersion, Loc_memMapVersion) begin
		case VMEAddr(4 downto 2) is
			when C_Reg_ipInfo_ident_1 =>
				Loc_RegRdData <= Loc_ident(63 downto 32);
				Loc_RegRdOK <= '1';
			when C_Reg_ipInfo_ident_0 =>
				Loc_RegRdData <= Loc_ident(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_ipInfo_firmwareVersion =>
				Loc_RegRdData <= Loc_firmwareVersion(31 downto 0);
				Loc_RegRdOK <= '1';
			when C_Reg_ipInfo_memMapVersion =>
				Loc_RegRdData <= Loc_memMapVersion(31 downto 0);
				Loc_RegRdOK <= '1';
			when others =>
				Loc_RegRdData <= CRegRdData;
				Loc_RegRdOK <= CRegRdOK;
		end case;
	end process RegRdMux;

	RegRdMux_DFF: process (Clk) begin
		if rising_edge(Clk) then
			RegRdData <= Loc_RegRdData;
			RegRdOK <= Loc_RegRdOK;
		end if;
	end process RegRdMux_DFF;

	RegRdDone <= Loc_VMERdMem(2) and RegRdOK;
	RegWrDone <= Loc_VMEWrMem(1) and CRegWrOK;

	RegRdError <= Loc_VMERdMem(2) and not RegRdOK;
	RegWrError <= Loc_VMEWrMem(1) and not CRegWrOK;

	Loc_MemRdData <= RegRdData;
	Loc_MemRdDone <= RegRdDone;
	Loc_MemRdError <= RegRdError;

	MemRdData <= Loc_MemRdData;
	MemRdDone <= Loc_MemRdDone;
	MemRdError <= Loc_MemRdError;

	Loc_MemWrDone <= RegWrDone;
	Loc_MemWrError <= RegWrError;

	MemWrDone <= Loc_MemWrDone;
	MemWrError <= Loc_MemWrError;

	RdData <= MemRdData;
	RdDone <= MemRdDone;
	WrDone <= MemWrDone;
	RdError <= MemRdError;
	WrError <= MemWrError;

	StrobeSeq: process (Clk) begin
		if rising_edge(Clk) then
			Loc_VMERdMem(Loc_VMERdMem'high downto 0) <= Loc_VMERdMem(Loc_VMERdMem'high-1 downto 0) & VMERdMem;
			Loc_VMEWrMem(Loc_VMEWrMem'high downto 0) <= Loc_VMEWrMem(Loc_VMEWrMem'high-1 downto 0) & VMEWrMem;
		end if;
	end process StrobeSeq;

	VMERdData <= RdData;
	VMERdDone <= RdDone;
	VMEWrDone <= WrDone;
	VMERdError <= RdError;
	VMEWrError <= WrError;

end;

-- EOF
