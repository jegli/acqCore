-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, Pulse extender--
-- --
-------------------------------------------------------------------------------
--
-- unit name: Pulse extender
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 10/04/2019
--
-- version: 1.0
--
-- description: Extend the input pulse to use it with a slower frequency clock 
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- acquisition core package
library work;
use work.ACQ_CORE_PACK.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pulse_extender is
    Generic(
            g_CLK_CYCLES : integer := 4
    );
    Port ( pulse_in_i   : in    STD_LOGIC;
           pulse_out_o  : out   STD_LOGIC;
           clk_i        : in    STD_LOGIC;
           reset_i      : in    STD_LOGIC
          );
end pulse_extender;

architecture Behavioral of pulse_extender is
    --signal instantation
    signal s_cnt        : unsigned (f_find_width(g_CLK_CYCLES)-1 downto 0);
    signal s_pulse_out  : std_logic;
    
begin
    pulse_out_o <= s_pulse_out;

    p_extender:process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_cnt           <= (others => '0');
            s_pulse_out     <= '0';
        elsif rising_edge(clk_i)then
            --default
            s_cnt       <= (others => '0');
            
            --detect input pulse
            if(pulse_in_i = '1')then
                --set output to '1'
                s_pulse_out <= '1';
            else
                if(s_pulse_out = '1')then
                    -- keep output pulse to '1'
                    if(s_cnt < g_CLK_CYCLES-1)then
                        s_cnt <= s_cnt + 1; 
                    else
                        s_pulse_out <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process;

end Behavioral;
