-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, Buffer manager --
-- --
-------------------------------------------------------------------------------
--
-- unit name: Buffer manager
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 08/05/2018
--
-- version: 1.0
--
-- description: Manage data storage in the memory
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: 
-- 1) Optimize the AXI_WR_data_size_o and AXI_WR_burst_length_o length. Is AXI_WR_data_size_o required?
-- 2) Acq_length and Acq_decimation width
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- acquisition core package
library work;
use work.ACQ_CORE_PACK.all;

entity buffer_manager is
    generic (
                g_CHANNELS          : integer := 12;
                g_DATA_IN_WIDTH     : integer := 32;
                g_DATA_OUT_WIDTH    : integer := 512;
                g_M_AXI_ADDR_WIDTH  : integer := 32;
                g_FIFO_BURST        : integer := 64;--256 -- maximum value: 256
                g_BUFFER_SIZE       : integer := 8192;
                g_DSP_BLOCK         : boolean := TRUE
            );
    port    ( 
                -- acquistion control
                acq_start_trig_i        : in    std_logic_vector(g_CHANNELS-1 downto 0);                                          -- start (restart)
                acq_length_i            : in    t_STD_ARRAY_32(0 to g_CHANNELS-1);
                acq_decimation_i        : in    t_STD_ARRAY_32(0 to g_CHANNELS-1); 
                acq_debug_mode_i        : in    std_logic;
                
                -- memory buffers control
                buffer_start_addr_i     : in    std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
                buffer_enable_i         : in    std_logic_vector(g_CHANNELS-1 downto 0);
                
                -- status
                busy_o                  : out   std_logic;
                acq_finished_o          : out   std_logic;
                acq_error_o             : out   std_logic;
                fifo_full_o             : out   std_logic;
                
                -- Channel interface
                ch_0_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_1_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_2_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_3_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_4_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_5_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_6_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_7_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_8_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_9_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_10_i         : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                ch_11_i         : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                
                -- AXI command interface
                AXI_WR_start_o           : out  std_logic;
                AXI_WR_start_address_o   : out  std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
                AXI_WR_data_size_o       : out  std_logic_vector(31 downto 0);
                AXI_WR_burst_length_o    : out  std_logic_vector(8 downto 0);
                AXI_WR_data_o            : out  std_logic_vector(g_DATA_OUT_WIDTH-1 downto 0);
                AXI_WR_valid_o           : out  std_logic;
                AXI_WR_wrnext_i          : in   std_logic;
                AXI_WR_done_i            : in   std_logic;
                AXI_WR_error_i           : in   std_logic;  -- currently unused
                AXI_WR_busy_i            : in   std_logic;
                
                -- clock and reset signals
                clk_i                    : in   std_logic;
                reset_i                  : in   std_logic;
                
                AXI_clk_i                : in   std_logic;
                AXI_reset_i              : in   std_logic
            ); 
end buffer_manager;

architecture Behavioral of buffer_manager is
    ----------------------------------------------------------------------
    -- component declarations
    ----------------------------------------------------------------------
    component data_buffering
        generic (
                    g_DATA_IN_WIDTH     : integer := 32;
                    g_DATA_OUT_WIDTH    : integer := 512;
                    g_FIFO_BURST        : integer := 64;
                    g_DSP_BLOCK         : boolean := TRUE
                );
        port (
            -- GENERAL RESET
            reset_i             : in  std_logic;
            
            -- ACQUISTION PARAMETERS 
            acq_start_trig_i    : in  std_logic;
            acq_length_i        : in  std_logic_vector(31 downto 0);
            acq_decimation_i    : in  std_logic_vector(31 downto 0);
            acq_finished_o      : out std_logic;
            acq_debug_mode_i    : in  std_logic;
            
            -- DATA INPUT INTERFACE
            ch_fifo_din_i       : in  std_logic_vector (g_DATA_IN_WIDTH-1 downto 0);
            ch_fifo_wr_clk_i    : in  std_logic;
            
            -- DATA OUTPUT INTERFACE
            ch_fifo_rd_clk_i        : in    std_logic;
            ch_fifo_rd_en_i         : in    std_logic;
            ch_fifo_dout_o          : out   std_logic_vector(g_DATA_OUT_WIDTH-1 downto 0);
            ch_fifo_full_o          : out   std_logic;
            ch_fifo_empty_o         : out   std_logic_vector(((g_DATA_OUT_WIDTH/g_DATA_IN_WIDTH)/4)-1 downto 0);
            ch_fifo_prog_empty_o    : out   std_logic_vector(((g_DATA_OUT_WIDTH/g_DATA_IN_WIDTH)/4)-1 downto 0);
            ch_fifo_data_ready_o    : out   std_logic
        );
    end component data_buffering;
    ----------------------------------------------------------------------
    component data_multiplexer is
        generic (   g_CHANNELS          : positive;
                    g_DATA_OUT_WIDTH    : positive
                );
        
        port(
                data_in     : in  t_fifo_rdport_array(0 to g_CHANNELS-1);
                data_out    : out t_fifo_rdport;
                sel         : in  std_logic_vector (f_find_width(g_CHANNELS)-1 downto 0)
        );
    end component data_multiplexer;
    ----------------------------------------------------------------------
    component OR_gate is
        Generic( g_WIDTH : integer := 2);
        Port ( input_i : in STD_LOGIC_VECTOR (g_WIDTH-1 downto 0);
               output_o : out STD_LOGIC);
    end component OR_gate;
    ----------------------------------------------------------------------
    component reset_synchronizer is
    Port ( clk_i        : in STD_LOGIC;     -- clock 
           rst_a_i      : in STD_LOGIC;     -- asynchronous reset
           rst_sync_o   : out STD_LOGIC);   -- synchronized reset
    end component;
    ----------------------------------------------------------------------
    component CDC_block is
    generic(
            g_WIDTH     : positive := 1    
    );
    port(
        clk_i       : IN std_logic;
        reset_i     : IN std_logic;
        data_in_i   : IN std_logic_vector( g_WIDTH-1 downto 0);
        data_out_o  : OUT std_logic_vector( g_WIDTH-1 downto 0)
     );
    end component;
    ----------------------------------------------------------------------
    component pulse_extender is
        Generic(
                g_CLK_CYCLES : integer := 4
        );
        Port ( pulse_in_i   : in    STD_LOGIC;
               pulse_out_o  : out   STD_LOGIC;
               clk_i        : in    STD_LOGIC;
               reset_i      : in    STD_LOGIC
              );
    end component;
    ----------------------------------------------------------------------
    -- constant declarations
    ----------------------------------------------------------------------
    constant c_CHANNEL_SELECT_WIDTH     : positive := f_find_width(g_CHANNELS);
    constant c_DATA_IN_WIDTH_BYTES      : positive := g_DATA_IN_WIDTH/8;                -- [byte]
    constant c_M_AXI_DATA_WIDTH_BYTES   : positive := g_DATA_OUT_WIDTH/8;               -- [byte]
    constant c_ALL_REACHED              : std_logic_vector(g_CHANNELS-1 downto 0) := (OTHERS => '1');
    
    ----------------------------------------------------------------------
    -- signal declarations
    ----------------------------------------------------------------------
    signal s_channel_data       : t_channel_array(0 to c_MAX_CHANNELS-1);
    signal s_fifo_rd_port       : t_fifo_rdport_array(0 to g_CHANNELS-1);
    signal s_mux_out            : t_fifo_rdport;
    signal s_channel_select     : unsigned(c_CHANNEL_SELECT_WIDTH-1 downto 0);
    signal s_fifo_rd_en         : std_logic_vector(g_CHANNELS-1 downto 0);
    signal s_fifo_full_or       : std_logic;
    signal s_fifo_full_or_sync  : std_logic;
    
    signal s_busy : std_logic;
    signal s_AXI_WR_done        : std_logic;
    
    signal s_buffer_start_addr          : std_logic_vector(buffer_start_addr_i'range);
    signal s_buffer_data_cnt            : t_UNSIGN_ARRAY_32 (0 to g_CHANNELS-1);
    signal s_buffer                     : t_buffer_array (0 to g_CHANNELS-1);
    signal s_buffer_cnt_init            : std_logic;
    signal s_buffer_end_reached         : std_logic_vector(g_CHANNELS-1 downto 0);
    signal s_buffer_enable              : std_logic_vector(buffer_enable_i'range);
    signal s_buffer_acq_finished        : std_logic_vector(g_CHANNELS-1 downto 0);
    signal s_buffer_acq_finished_sync          : std_logic_vector(g_CHANNELS-1 downto 0);
    signal s_fifo_full                  : std_logic_vector(g_CHANNELS-1 downto 0);
    signal s_acq_error                  : std_logic;
    signal s_acq_finished               : std_logic;
    signal s_acq_start_trig             : std_logic_vector(g_CHANNELS-1 downto 0);
    signal s_trig_received              : std_logic;
    signal s_data_buffering_reset       : std_logic;    
    signal s_data_buffering_reset_sync  : std_logic;  
    signal s_trig_received_sync         : std_logic;
    signal s_trig_received_sync_1       : std_logic;
    signal s_acq_start_trig_sync        : std_logic_vector(g_CHANNELS-1 downto 0);
    
    
    signal s_wr_cmd             : t_AXI_WR_command;
    signal s_wr_req             : std_logic;
    signal s_wr_busy            : std_logic;
    signal s_wr_done            : std_logic;
    signal s_wr_error           : std_logic;
    
    signal s_acq_error_extend  : std_logic;
    --------------------------------------------------------------------
    -- MAIN STATE MACHINE
    --------------------------------------------------------------------
    type t_state is (   IDLE,        
                        SCANNING,
                        WAITING
                    );
    
    signal s_main_state  : t_state; 
    
    type t_cmd_sending_state is (   IDLE,
                                    AXI_BUSY,
                                    REQUEST,
                                    LOADING,
                                    DONE
                                );
    signal s_cmd_sending_state : t_cmd_sending_state;
    
begin
    --=============================================================================
    -- port map
    --=============================================================================
    fifo_full_o <= s_fifo_full_or_sync;
    
    s_acq_start_trig <= acq_start_trig_i and buffer_enable_i;
        
    -- data_buffering reset
    s_data_buffering_reset <= reset_i or s_acq_error_extend;
    --------------------------------------------------------------------
    fifo_full_or_gate: or_gate
    generic map(g_WIDTH=>g_CHANNELS)
    port map(
                input_i=>s_fifo_full,
                output_o=>s_fifo_full_or      
    );
    --------------------------------------------------------------------
    acq_trig_or_gate: or_gate
    generic map(g_WIDTH=>g_CHANNELS)
    port map(
                input_i=>s_acq_start_trig,
                output_o=>s_trig_received      
    );
    
    --------------------------------------------------------------------
    axi_reset_synchronizer: reset_synchronizer
    port map(
                clk_i=>clk_i,
                rst_a_i=>s_data_buffering_reset,
                rst_sync_o=>s_data_buffering_reset_sync
    );
    --------------------------------------------------------------------
    buffer_array: for index in 0 to (g_CHANNELS-1) generate
    begin 
        data_buffering_0:data_buffering
        generic map(
            g_DATA_IN_WIDTH => g_DATA_IN_WIDTH,
            g_DATA_OUT_WIDTH => g_DATA_OUT_WIDTH,
            g_FIFO_BURST => g_FIFO_BURST,
            g_DSP_BLOCK => g_DSP_BLOCK
        )
        port map(
                    reset_i                 => s_data_buffering_reset_sync,
                    acq_start_trig_i        => s_acq_start_trig_sync(index),
                    acq_length_i            => acq_length_i(index),
                    acq_decimation_i        => acq_decimation_i(index),
                    acq_finished_o          => s_buffer_acq_finished(index),
                    acq_debug_mode_i        => acq_debug_mode_i,
                    ch_fifo_din_i           => s_channel_data(index),
                    ch_fifo_wr_clk_i        => clk_i,
                    ch_fifo_rd_clk_i        => AXI_clk_i,
                    ch_fifo_rd_en_i         => s_fifo_rd_en(index),
                    ch_fifo_full_o          => s_fifo_full(index),
                    ch_fifo_dout_o          => s_fifo_rd_port(index).rd_data,
                    ch_fifo_empty_o         => s_fifo_rd_port(index).rd_empty,
                    ch_fifo_prog_empty_o    => s_fifo_rd_port(index).rd_prog_empty,
                    ch_fifo_data_ready_o    => open
                );
    end generate buffer_array;
    --------------------------------------------------------------------
    data_multiplexer_0:data_multiplexer
    generic map(
        g_CHANNELS => g_CHANNELS,
        g_DATA_OUT_WIDTH => g_DATA_OUT_WIDTH
    )
    port map(            
        data_in     => s_fifo_rd_port,
        data_out    => s_mux_out,
        sel         => std_logic_vector(s_channel_select)
    );
    ----------------------------------------------------------------------
    AXI_acq_start_trig_sync:CDC_block
    generic map(
            g_WIDTH => 1  
    )
    port map(
        clk_i           => AXI_clk_i,
        reset_i         => AXI_reset_i,
        data_in_i(0)    => s_trig_received,
        data_out_o(0)   => s_trig_received_sync
     );
    ----------------------------------------------------------------------
    acq_start_trig_sync:CDC_block
    generic map(
            g_WIDTH => g_CHANNELS  
    )
    port map(
        clk_i       => clk_i,
        reset_i     => reset_i,
        data_in_i   => s_acq_start_trig,
        data_out_o  => s_acq_start_trig_sync
     );
    ----------------------------------------------------------------------
    fifo_full_sync:CDC_block
    generic map(
         g_WIDTH => 1  
    )
    port map(
         clk_i          => AXI_clk_i,
         reset_i        => AXI_reset_i,
         data_in_i(0)   => s_fifo_full_or,
         data_out_o(0)  => s_fifo_full_or_sync
    );
    ----------------------------------------------------------------------
    acq_finished_sync:CDC_block
    generic map(
      g_WIDTH => g_CHANNELS    
    )
    port map(
        clk_i       => AXI_clk_i,
        reset_i     => AXI_reset_i,
        data_in_i   => s_buffer_acq_finished,
        data_out_o  => s_buffer_acq_finished_sync
    );
    --------------------------------------------------------------------
    -- channels
    s_channel_data(0)  <=  ch_0_i  when g_CHANNELS > 0  else (OTHERS => '0');
    s_channel_data(1)  <=  ch_1_i  when g_CHANNELS > 1  else (OTHERS => '0');
    s_channel_data(2)  <=  ch_2_i  when g_CHANNELS > 2  else (OTHERS => '0');
    s_channel_data(3)  <=  ch_3_i  when g_CHANNELS > 3  else (OTHERS => '0');
    s_channel_data(4)  <=  ch_4_i  when g_CHANNELS > 4  else (OTHERS => '0');
    s_channel_data(5)  <=  ch_5_i  when g_CHANNELS > 5  else (OTHERS => '0');
    s_channel_data(6)  <=  ch_6_i  when g_CHANNELS > 6  else (OTHERS => '0');
    s_channel_data(7)  <=  ch_7_i  when g_CHANNELS > 7  else (OTHERS => '0');
    s_channel_data(8)  <=  ch_8_i  when g_CHANNELS > 8  else (OTHERS => '0');
    s_channel_data(9)  <=  ch_9_i  when g_CHANNELS > 9  else (OTHERS => '0');
    s_channel_data(10) <=  ch_10_i when g_CHANNELS > 10 else (OTHERS => '0');
    s_channel_data(11) <=  ch_11_i when g_CHANNELS > 11 else (OTHERS => '0');
    --------------------------------------------------------------------
    -- AXI command
    AXI_WR_start_o          <= s_wr_cmd.start;
    AXI_WR_start_address_o  <= std_logic_vector(s_wr_cmd.address(AXI_WR_start_address_o'range));
    AXI_WR_data_size_o      <= std_logic_vector(s_wr_cmd.data_size);
    AXI_WR_burst_length_o   <= std_logic_vector(s_wr_cmd.burst_length);
    --------------------------------------------------------------------
    -- busy
    busy_o <= s_busy;
    --------------------------------------------------------------------
    -- interrupt
    acq_error_o     <= s_acq_error;
    acq_finished_o  <= s_acq_finished;
    
    acq_error_pulse_extender:pulse_extender
    Port map ( pulse_in_i => s_acq_error,
               pulse_out_o => s_acq_error_extend,
               clk_i => AXI_clk_i,
               reset_i => AXI_reset_i
          );
    --=============================================================================
    -- Begin of the main state machine
    -- (state transitions)
    --=============================================================================
    
    p_main_state_machine: process(AXI_clk_i,AXI_reset_i)
    begin
        if(AXI_reset_i = '1')then
            s_main_state                <= IDLE;
            s_wr_req                    <= '0';
            s_buffer_cnt_init           <= '0';
            s_acq_error                 <= '0';
            s_acq_finished              <= '0';
            s_busy                      <= '0';
            s_buffer_start_addr         <= (others => '0');
            s_channel_select            <= (others => '0');
            s_AXI_WR_done               <= '0';
            s_trig_received_sync_1      <= '0';
            s_buffer_enable             <= (others => '0');
        elsif (rising_edge (AXI_clk_i))then
            --default
            s_wr_req                <= '0';
            s_buffer_cnt_init       <= '0';
            s_acq_error             <= '0';
            s_AXI_WR_done           <= AXI_WR_done_i;
            s_trig_received_sync_1  <= s_trig_received_sync;
      
            -- STATE MACHINE
            case s_main_state is
                -- state "IDLE"
                when IDLE =>   
                    -- default
                    s_wr_req                    <= '0';
                    s_buffer_cnt_init           <= '0';
                    s_busy                      <= '0';
                    s_buffer_start_addr         <= (others => '0');
                    s_channel_select            <= (others => '0');

                    -- waiting start trigger rising edge
                    if(s_trig_received_sync = '1' and s_trig_received_sync_1 = '0')then
                        -- reset acquisition finished status bit
                        s_acq_finished      <= '0';
                    
                        -- copy buffer start address and channel enable
                        s_buffer_start_addr <= buffer_start_addr_i;
                        s_buffer_enable     <= buffer_enable_i;
                        
                        -- module is busy
                        s_busy  <= '1';

                        -- initialize buffer counter value;
                        s_buffer_cnt_init   <= '1';
                        
                        -- channel selector init
                        s_channel_select <= (others => '0');

                        -- change state
                        s_main_state <= SCANNING;
                    end if;
                -- state "SCANNING"
                when SCANNING =>
                    -- checking the state of each fifo, if prog empty of the 4th buffer fifo is high -> data must be read
                    if( (s_mux_out.rd_prog_empty(3) = '0' or 
                        (s_mux_out.rd_prog_empty(3) = '1' and s_buffer_acq_finished_sync(to_integer(s_channel_select)) = '1' and s_buffer_end_reached(to_integer(s_channel_select)) = '0'))and 
                        s_buffer_enable(to_integer(s_channel_select)) = '1')then
                        -- if state machine is busy -> waits for sending request
                        if(s_WR_busy = '0')then
                            -- send request 
                            s_wr_req <= '1';
                            -- change state
                            s_main_state <= WAITING;
                        end if;
                    else
                        -- channel selection
                        s_channel_select <= s_channel_select + 1;
                        
                        if(s_channel_select = g_CHANNELS-1)then 
                            s_channel_select <= (others => '0');
                        end if;  
                    end if;
                -- state "WAITING"
                when WAITING =>    
                    -- waiting loading is finished
                    if(s_wr_done = '1')then
                        -- return to scanning state
                        s_main_state <= SCANNING;

                        if(s_buffer_end_reached = s_buffer_enable)then
                            s_main_state            <= IDLE;
                            s_busy                  <= '0';
                            if(s_buffer_end_reached /= s_buffer_acq_finished_sync)then
                                s_acq_error     <= '1';
                            else
                                s_acq_finished  <= '1';
                            end if;
                        end if;
                    end if;
                -- other states 
                when others =>
                    --jump to save state (ERROR?!)
                    s_main_state <= IDLE;
                end case;
                
                -- reset state machine when an error is detected
                if(s_wr_error = '1' or s_fifo_full_or_sync = '1')then
                    s_main_state        <= IDLE;
                    s_acq_error     <= '1';
                end if;
        end if;
    end process p_main_state_machine;
    
    --=============================================================================
    -- Begin of the load state machine
    -- (state transitions)
    --=============================================================================         
    p_cmd_sending_state_machine: process(AXI_clk_i,AXI_reset_i)
    begin
        if(AXI_reset_i = '1')then
            s_wr_cmd                <= c_AXI_WR_command_init;
            s_wr_done               <= '0';
            s_wr_busy               <= '0';
            s_wr_error              <= '0';
            s_cmd_sending_state     <= IDLE;
            s_buffer_end_reached    <= (OTHERS => '0');
        elsif (rising_edge (AXI_clk_i))then
            -- default
            s_wr_cmd.start          <= '0';
            s_wr_done               <= '0';
            s_wr_error              <= '0';
        
            case s_cmd_sending_state is
                --state "IDLE"
                when IDLE =>
                    -- waiting request for main state machine
                    if(s_wr_req = '1')then
                        -- change state
                        s_cmd_sending_state     <= AXI_BUSY;
                        s_wr_busy               <= '1';
                    else
                        -- reset signals
                        s_wr_cmd                <= c_AXI_WR_command_init;
                        s_wr_done               <= '0';
                        s_wr_busy               <= '0';
                        s_wr_error              <= '0';
                        s_cmd_sending_state     <= IDLE;
                    end if;
                
                -- state "AXI BUSY"
                when AXI_BUSY =>    
                    -- END TIMER IMPLEMENTATION????
                    
                    -- check that AXI is not busy 
                    if(AXI_WR_busy_i = '0')then
                        s_cmd_sending_state     <= REQUEST;
                    end if;
                    
                -- state "REQUEST"
                when REQUEST =>
                        -- calculate max burst length
                        -- 256Bursts x 8Bytes = 2048bytes                    
                        if(s_buffer_data_cnt(to_integer(s_channel_select)) < to_unsigned(g_FIFO_BURST*c_M_AXI_DATA_WIDTH_BYTES,s_buffer_data_cnt(0)'length))then
                            -- prepare burst length and size of the next command
                            s_wr_cmd.burst_length                                     <= to_unsigned(to_integer(s_buffer_data_cnt(to_integer(s_channel_select))/c_M_AXI_DATA_WIDTH_BYTES),s_wr_cmd.burst_length'length); --max burst: 256 => 9bits
                            s_wr_cmd.data_size(s_wr_cmd.data_size'high-1 downto 15)   <= (OTHERS => '0'); --unused => 0
                            s_wr_cmd.data_size(14 downto 0)                           <= s_buffer_data_cnt(to_integer(s_channel_select))(14 downto 0); --buffer_data_cnt is smaller than (512bits/8)Bytes x 256Bursts => 16384 (15 bits needed);
                        else
                            -- max burst length(256x)
                            s_wr_cmd.burst_length     <= to_unsigned(g_FIFO_BURST,s_wr_cmd.burst_length'length);
                            s_wr_cmd.data_size        <= to_unsigned(g_FIFO_BURST*c_M_AXI_DATA_WIDTH_BYTES,s_wr_cmd.data_size'length); 
                        end if;
                        
                        s_wr_cmd.address                                   <= (others => '0'); -- reset unused bits
                        s_wr_cmd.address(s_buffer_start_addr'range)        <= unsigned(s_buffer_start_addr) + to_unsigned(g_BUFFER_SIZE*to_integer(s_channel_select),s_buffer_start_addr'length) + s_buffer(to_integer(s_channel_select)).index(s_buffer_start_addr'range);
                        
                        -- starts loading
                        s_wr_cmd.start  <= '1';
                        -- change to state "LOADING"
                        s_cmd_sending_state <= LOADING;
                        
                -- state "LOADING"  
                when LOADING =>
                    -- on rising edge of AXI rd_done input    
                    if(s_AXI_wr_done = '0' AND AXI_wr_done_i = '1')then
                        -- check the status of the counter
                        if(unsigned(s_buffer_data_cnt(to_integer(s_channel_select))) = 0)then
                            -- if 0, all vectors were read in the buffer
                            s_buffer_end_reached(to_integer(s_channel_select)) <= '1';
                        end if;
                        
                        -- change to state "DONE"
                        s_cmd_sending_state        <= DONE;
                    end if;
                -- state "DONE"  
                when DONE =>
                    if(s_wr_busy = '1')then
                        -- back to initial state
                        s_cmd_sending_state <= IDLE;
                        s_wr_busy           <= '0';
                        s_wr_done           <= '1';
                    else
                        s_wr_error                 <= '1';
                        s_cmd_sending_state        <= IDLE;
                    end if;
                    
                -- others states
                when others =>
                    s_cmd_sending_state        <= IDLE;
            end case;
            
            -- reset when error detected
            if(s_acq_error = '1' or s_acq_finished = '1')then
                s_cmd_sending_state     <= IDLE;
                s_buffer_end_reached    <= (OTHERS => '0');
            end if;
        end if;
    end process p_cmd_sending_state_machine;
    
    AXI_WR_data_o     <= s_mux_out.rd_data;
    
    
    --=============================================================================
    -- Begin of the fifo read process
    --=============================================================================  
    p_fifo_rd_en: process(AXI_reset_i,AXI_WR_wrnext_i,s_wr_cmd,s_channel_select)
    begin
        if(AXI_reset_i = '1')then  
            s_fifo_rd_en <= (others => '0');
        else
            s_fifo_rd_en <= (others => '0');
            s_fifo_rd_en(to_integer(s_channel_select)) <= AXI_WR_wrnext_i or s_wr_cmd.start; 
        end if;
    end process p_fifo_rd_en;  
       
    --=============================================================================
    -- AXI write valid 
    --============================================================================= 
    p_AXI_WR_valid: process(AXI_clk_i,AXI_reset_i)
    begin
        if(AXI_reset_i = '1')then
            AXI_WR_valid_o <= '0';
        elsif (rising_edge (AXI_clk_i))then
            AXI_WR_valid_o    <= s_fifo_rd_en(to_integer(s_channel_select));
        end if;
    end process p_AXI_WR_valid;
	--=============================================================================
    -- Begin of the process "Buffer index updater"
    -- Update the buffer counter 
    --=============================================================================
    p_buffer_cnt_updater: process(AXI_clk_i,AXI_reset_i)
    begin
        if(AXI_reset_i = '1')then
            s_buffer_data_cnt   <= (OTHERS => (OTHERS => '0'));
            
            for I in 0 to (g_CHANNELS-1) loop
                s_buffer(I).index   <= (OTHERS => '0');
            end loop;
            
        elsif (rising_edge (AXI_clk_i))then
            -----------------------------------------------------------------
            -- wait buffer cnt init signal
            if(s_buffer_cnt_init = '1')then
                --copy buffer length for each function
                for I in 0 to (g_CHANNELS-1) loop
                    -- initialize the buffer counter for each function (conversion in bytes)
                    s_buffer_data_cnt(I)  <= unsigned(acq_length_i(I)(acq_length_i(0)'HIGH-2 downto 0)) & "00";
                end loop;
            
            -----------------------------------------------------------------
            -- update buffer counter value.
            elsif(s_wr_cmd.start = '1')then
                -- update buffer index of the function
                s_buffer(to_integer(s_channel_select)).index      <= s_buffer(to_integer(s_channel_select)).index + unsigned(s_wr_cmd.data_size);
                s_buffer_data_cnt(to_integer(s_channel_select))   <= s_buffer_data_cnt(to_integer(s_channel_select)) - unsigned(s_wr_cmd.data_size); 
            end if;
            -----------------------------------------------------------------
        end if;
    end process p_buffer_cnt_updater;
	
end Behavioral;
