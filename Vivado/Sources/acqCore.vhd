-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, acqCore --
-- --
-------------------------------------------------------------------------------
--
-- unit name: acqCore
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 08/05/2018
--
-- version: 1.0
--
-- description: Acquisition core for Xilinx
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: 
-- 
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- data acquisition core package
library work;
use work.ACQ_CORE_PACK.all;

entity acqCore is
    generic (
                g_MODE              : string  := "internal";
                g_CHANNELS          : integer := 1;
                g_BUFFERS           : integer := 8;
                g_M_AXI_DATA_WIDTH  : integer := 512;
                g_M_AXI_ADDR_WIDTH  : integer := 31;
                g_DATA_IN_WIDTH     : integer := 32;
                g_BASE_ADDR_0       : integer := 0;
                g_BASE_ADDR_1       : integer := 0;
                g_BUFFER_SIZE       : integer := 8192;
                g_FIFO_BURST        : integer := 64;--256
                g_DSP_BLOCK         : boolean := TRUE
        );
  port (
    -- AXI-FULL master interface
    M_AXI_clk_i         : in    std_logic;
    M_AXI_reset_i       : in    std_logic;
    
    M_AXI_awaddr        : out   std_logic_vector ( g_M_AXI_ADDR_WIDTH-1 downto 0 );
    M_AXI_awburst       : out   std_logic_vector ( 1 downto 0 );
    M_AXI_awcache       : out   std_logic_vector ( 3 downto 0 );
    M_AXI_awid          : out   std_logic_vector ( 3 downto 0 );
    M_AXI_awlen         : out   std_logic_vector ( 7 downto 0 );
    M_AXI_awlock        : out   std_logic;
    M_AXI_awprot        : out   std_logic_vector ( 2 downto 0 );
    M_AXI_awqos         : out   std_logic_vector ( 3 downto 0 );
    M_AXI_awready       : in    std_logic;
    M_AXI_awsize        : out   std_logic_vector ( 2 downto 0 );
    M_AXI_awuser        : out   std_logic_vector ( 1 downto 0 );
    M_AXI_awvalid       : out   std_logic;
    M_AXI_bid           : in    std_logic_vector ( 1 downto 0 );
    M_AXI_bready        : out   std_logic;
    M_AXI_bresp         : in    std_logic_vector ( 1 downto 0 );
    M_AXI_buser         : in    std_logic_vector ( 1 downto 0 );
    M_AXI_bvalid        : in    std_logic;
    M_AXI_wdata         : out   std_logic_vector ( g_M_AXI_DATA_WIDTH-1 downto 0 );
    M_AXI_wlast         : out   std_logic;
    M_AXI_wready        : in    std_logic;
    M_AXI_wstrb         : out   std_logic_vector ( 63 downto 0 );
    M_AXI_wuser         : out   std_logic_vector ( 1 downto 0 );
    M_AXI_wvalid        : out   std_logic;
    
    -- AXI-LITE slave interface
    S_AXI_aclk          : in    std_logic;
    S_AXI_aresetn       : in    std_logic;
    
    -- AXI4-Lite Interface
    S_AXI_araddr        : in    std_logic_vector(31 downto 0);
    S_AXI_arprot        : in    std_logic_vector(2 downto 0);
    S_AXI_arvalid       : in    std_logic;
    S_AXI_arready       : out   std_logic;
    S_AXI_rdata         : out   std_logic_vector(31 downto 0);
    S_AXI_rresp         : out   std_logic_vector(1 downto 0);
    S_AXI_rvalid        : out   std_logic;
    S_AXI_rready        : in    std_logic;
    
    -- Register slave interface
    cheb_Addr : in std_logic_vector(9 downto 2);
    cheb_RdData : out std_logic_vector(31 downto 0);
    cheb_WrData : in std_logic_vector(31 downto 0);
    cheb_RdMem : in std_logic;
    cheb_WrMem : in std_logic;
    cheb_RdDone : out std_logic;
    cheb_WrDone : out std_logic;
    cheb_RdError : out std_logic;
    cheb_WrError : out std_logic;
    
    -- input acquistion channels
    ch_0_i  : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_1_i  : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_2_i  : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_3_i  : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_4_i  : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_5_i  : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_6_i  : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_7_i  : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_8_i  : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_9_i  : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_10_i : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    ch_11_i : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    
    -- input acquistion buffers
    buff_0_i     : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_1_i     : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_2_i     : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_3_i     : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_4_i     : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_5_i     : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_6_i     : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_7_i     : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_8_i     : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_9_i     : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_10_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_11_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_12_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_13_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_14_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_15_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_16_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_17_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_18_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_19_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_20_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_21_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_22_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_23_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_24_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_25_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_26_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_27_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_28_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_29_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_30_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    buff_31_i    : in    std_logic_vector ( g_DATA_IN_WIDTH-1 downto 0 );
    
    --Triggers
    acq_start_trig_i    : in    std_logic_vector(g_CHANNELS-1 downto 0);
    
    -- Status
    busy_o              : out   std_logic;
    irq_acq_finished_o  : out   std_logic; 
    irq_acq_error_o     : out   std_logic;
    acq_axi_busy_o      : out   std_logic;
    acq_axi_length_o    : out   std_logic_vector(8 downto 0);
    fifo_full_o         : out   std_logic;

    -- Clocks/Resets
    acq_clk_i           : in    std_logic;
    clk_i               : in    std_logic;
    reset_i             : in    std_logic
  );
    ATTRIBUTE HW_HANDOFF : string;
    ATTRIBUTE HW_HANDOFF of acqCore : entity is "acqCore.hwdef";
    ATTRIBUTE core_generation_info : string;
    ATTRIBUTE core_generation_info of acqCore : entity is "acqCore,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=acqCore,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=2,numReposBlks=2,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=2,numPkgbdBlks=0,bdsource=USER,synth_mode=OOC_per_IP}";
 
end acqCore;

architecture STRUCTURE of acqCore is
    -------------------------------------------------------------------------------
    -- attributes declaration
    
    ATTRIBUTE X_INTERFACE_INFO : STRING;
    ATTRIBUTE X_INTERFACE_PARAMETER : STRING;

    ATTRIBUTE X_INTERFACE_PARAMETER OF reset_i: SIGNAL IS "XIL_INTERFACENAME reset_i, POLARITY ACTIVE_HIGH";
    ATTRIBUTE X_INTERFACE_INFO OF reset_i: SIGNAL IS "xilinx.com:signal:reset:1.0 reset_i RST";
    ATTRIBUTE X_INTERFACE_PARAMETER OF clk_i: SIGNAL IS "XIL_INTERFACENAME clk_i, ASSOCIATED_RESET reset_i";
    ATTRIBUTE X_INTERFACE_INFO OF clk_i: SIGNAL IS "xilinx.com:signal:clock:1.0 clk_i CLK";
    
    ATTRIBUTE X_INTERFACE_PARAMETER OF M_AXI_reset_i: SIGNAL IS "XIL_INTERFACENAME M_AXI_reset_i, POLARITY ACTIVE_HIGH";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_reset_i: SIGNAL IS "xilinx.com:signal:reset:1.0 M_AXI_reset_i RST";
    ATTRIBUTE X_INTERFACE_PARAMETER OF M_AXI_clk_i: SIGNAL IS "XIL_INTERFACENAME M_AXI_clk_i, ASSOCIATED_RESET M_AXI_reset_i, ASSOCIATED_BUSIF M_AXI";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_clk_i: SIGNAL IS "xilinx.com:signal:clock:1.0 M_AXI_clk_i CLK";
    
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_wvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI WVALID";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_wuser: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI WUSER";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_wstrb: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI WSTRB";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_wready: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI WREADY";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_wlast: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI WLAST";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_wdata: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI WDATA";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_bvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI BVALID";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_buser: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI BUSER";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_bresp: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI BRESP";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_bready: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI BREADY";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_bid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI BID";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awvalid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWVALID";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awuser: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWUSER";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awsize: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWSIZE";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awready: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWREADY";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awqos: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWQOS";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awprot: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWPROT";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awlock: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWLOCK";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awlen: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWLEN";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awid: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWID";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awcache: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWCACHE";
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awburst: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWBURST";
    ATTRIBUTE X_INTERFACE_PARAMETER OF M_AXI_awaddr: SIGNAL IS "XIL_INTERFACENAME M_AXI, DATA_WIDTH 512, PROTOCOL AXI4"; 
    ATTRIBUTE X_INTERFACE_INFO OF M_AXI_awaddr: SIGNAL IS "xilinx.com:interface:aximm:1.0 M_AXI AWADDR";
    
    

    -------------------------------------------------------------------------------
    -- constant instantation
    
    -------------------------------------------------------------------------------
    -- signal instantation    
    -- general signals
    signal s_soft_reset_reg             : std_logic;
    signal s_clr_faults_reg             : std_logic;
    signal s_busy_reg                   : std_logic;
    signal s_acq_finished_reg           : std_logic;
    signal s_acq_finished               : std_logic;
    signal s_acq_error_reg              : std_logic;
    signal s_acq_fault_enable_reg       : std_logic;
    signal s_acq_length_reg             : t_STD_ARRAY_32(0 to c_MAX_CHANNELS-1);
    signal s_acq_decimation_reg         : t_STD_ARRAY_32(0 to c_MAX_CHANNELS-1);
    signal s_enable_reg                 : std_logic_vector(c_MAX_CHANNELS-1 downto 0);
    signal s_soft_start_trig_reg        : std_logic_vector(c_MAX_CHANNELS-1 downto 0);
    signal s_acq_start_trig             : std_logic_vector(g_CHANNELS-1 downto 0);
    signal s_buffer_start_addr_reg      : std_logic_vector(c_REG_WIDTH-1 downto 0);
    signal s_debug_mode_reg             : std_logic;
    
    signal s_noFaults                   : std_logic;
    signal s_faults_SRFF                : std_logic_vector(c_REG_WIDTH-1 downto 0);
    signal s_acq_error                  : std_logic;
    
    signal s_AXI_WR_start           : std_logic;
    signal s_AXI_WR_start_address   : std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
    signal s_AXI_WR_data_size       : std_logic_vector(31 downto 0);
    signal s_AXI_WR_burst_length    : std_logic_vector(8 downto 0);
    signal s_AXI_WR_done            : std_logic;
    signal s_AXI_WR_error           : std_logic;
    signal s_AXI_WR_busy            : std_logic;
    signal s_AXI_WR_data            : std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);
    signal s_AXI_WR_valid           : std_logic;
    signal s_AXI_WR_wrnext          : std_logic;
    signal s_AXI_ext_reset          : std_logic;
            
    signal s_reset                  : std_logic;
    signal s_AXI_reset_n            : std_logic;
    signal s_AXI_reset              : std_logic;
    signal s_acq_reset              : std_logic;
    signal s_acq_reset_sync         : std_logic;
    
    -- external mode signals
    signal s_buff_inputs            : t_buff_array(0 to c_MAX_BUFFERS-1);
    signal s_buff_data              : t_buff_array(0 to g_BUFFERS-1);
    
    -- internal mode signals
    signal s_BRAM_we                : std_logic;
    signal s_BRAM_addr              : std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
    signal s_acq_debug_data         : std_logic_vector(31 downto 0);
    
    ----------------------------------------------------------------------
    ---                         IP common blocks                       ---
    ----------------------------------------------------------------------
    component CDC_block is
        generic(
                g_WIDTH     : positive := 1    
        );
        port(
            clk_i       : IN std_logic;
            reset_i     : IN std_logic;
            data_in_i   : IN std_logic_vector( g_WIDTH-1 downto 0);
            data_out_o  : OUT std_logic_vector( g_WIDTH-1 downto 0)
         );
    end component;
    -----------------------------------------------------------------------
    component RegCtrl_acqCore is
        port (
            Clk : in std_logic;
            Rst : in std_logic;
            VMEAddr : in std_logic_vector(9 downto 2);
            VMERdData : out std_logic_vector(31 downto 0);
            VMEWrData : in std_logic_vector(31 downto 0);
            VMERdMem : in std_logic;
            VMEWrMem : in std_logic;
            VMERdDone : out std_logic;
            VMEWrDone : out std_logic;
            VMERdError : out std_logic;
            VMEWrError : out std_logic;
            ipInfo_ident : in std_logic_vector(63 downto 0);
            ipInfo_firmwareVersion : in std_logic_vector(31 downto 0);
            ipInfo_memMapVersion : in std_logic_vector(31 downto 0);
            ipInfo_sysControl : out std_logic_vector(15 downto 0);
            control_softReset : out std_logic;
            control_clrFaults : out std_logic;
            control_enable : out std_logic_vector(13 downto 2);
            control_softStartTrig : out std_logic_vector(25 downto 14);
            control_debugMode : out std_logic;
            status_busy : in std_logic;
            status_acqFinished : in std_logic;
            status_noFaults : in std_logic;
            status_noOverflow : in std_logic;
            faults_acqFault : in std_logic;
            faults_SRFF : out std_logic_vector(31 downto 0);
            faults_ClrSRFF : in std_logic;
            faultsEnable_acqFaultEnable : out std_logic;
            acqLength0 : out std_logic_vector(31 downto 0);
            acqLength1 : out std_logic_vector(31 downto 0);
            acqLength2 : out std_logic_vector(31 downto 0);
            acqLength3 : out std_logic_vector(31 downto 0);
            acqLength4 : out std_logic_vector(31 downto 0);
            acqLength5 : out std_logic_vector(31 downto 0);
            acqLength6 : out std_logic_vector(31 downto 0);
            acqLength7 : out std_logic_vector(31 downto 0);
            acqLength8 : out std_logic_vector(31 downto 0);
            acqLength9 : out std_logic_vector(31 downto 0);
            acqLength10 : out std_logic_vector(31 downto 0);
            acqLength11 : out std_logic_vector(31 downto 0);
            acqDecimation0 : out std_logic_vector(31 downto 0);
            acqDecimation1 : out std_logic_vector(31 downto 0);
            acqDecimation2 : out std_logic_vector(31 downto 0);
            acqDecimation3 : out std_logic_vector(31 downto 0);
            acqDecimation4 : out std_logic_vector(31 downto 0);
            acqDecimation5 : out std_logic_vector(31 downto 0);
            acqDecimation6 : out std_logic_vector(31 downto 0);
            acqDecimation7 : out std_logic_vector(31 downto 0);
            acqDecimation8 : out std_logic_vector(31 downto 0);
            acqDecimation9 : out std_logic_vector(31 downto 0);
            acqDecimation10 : out std_logic_vector(31 downto 0);
            acqDecimation11 : out std_logic_vector(31 downto 0);
            bufferStartAddress : out std_logic_vector(31 downto 0);
            bufferStartAddress_WrStrobe : out std_logic;
            bufferSize : in std_logic_vector(31 downto 0)
        );
    end component;
    -----------------------------------------------------------------------   
    component reset_synchronizer is
        Port (  clk_i        : in STD_LOGIC;     -- clock 
                rst_a_i      : in STD_LOGIC;     -- asynchronous reset
                rst_sync_o   : out STD_LOGIC);   -- synchronized reset
    end component;
    
    --====================================================================
    ---                         External mode blocks                   ---
    --====================================================================  
    -- component instantation   
    component AXI_MM_write_interface is
        generic (
            -- Width of Address Bus
            g_M_AXI_ADDR_WIDTH	: integer	:= 32;
            -- Width of Data Bus
            g_M_AXI_DATA_WIDTH	: integer	:= 512
        );
        port (
            -- AXI command interface
            AXI_WR_start_i             : in     std_logic;
            AXI_WR_start_address_i     : in     std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
            AXI_WR_data_size_i         : in     std_logic_vector(31 downto 0);
            AXI_WR_burst_length_i      : in     std_logic_vector(8 downto 0);
            AXI_WR_done_o              : out    std_logic;
            AXI_WR_error_o             : out    std_logic;
            AXI_WR_busy_o              : out    std_logic;
            AXI_WR_data_i              : in     std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);
            AXI_WR_valid_i             : in     std_logic;
            AXI_WR_wrnext_o            : out    std_logic;
            
            -- external reset control
            AXI_ext_reset_i          : in     std_logic;  
    
            -- Global Clock Signal.
            M_AXI_ACLK                   : in    std_logic;
            -- Global Reset Singal. This Signal is Active Low
            M_AXI_ARESETN               : in std_logic;
            -- Master Interface Write Address ID
            M_AXI_AWID                   : out std_logic_vector(3 downto 0);
            -- Master Interface Write Address
            M_AXI_AWADDR               : out std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
            -- Burst length. The burst length gives the exact number of transfers in a burst
            M_AXI_AWLEN                   : out std_logic_vector(7 downto 0);
            -- Burst size. This signal indicates the size of each transfer in the burst
            M_AXI_AWSIZE               : out std_logic_vector(2 downto 0);
            -- Burst type. The burst type and the size information, 
            -- determine how the address for each transfer within the burst is calculated.
            M_AXI_AWBURST               : out std_logic_vector(1 downto 0);
            -- Lock type. Provides additional information about the
            -- atomic characteristics of the transfer.
            M_AXI_AWLOCK               : out std_logic;
            -- Memory type. This signal indicates how transactions
            -- are required to progress through a system.
            M_AXI_AWCACHE               : out std_logic_vector(3 downto 0);
            -- Protection type. This signal indicates the privilege
            -- and security level of the transaction, and whether
            -- the transaction is a data access or an instruction access.
            M_AXI_AWPROT               : out std_logic_vector(2 downto 0);
            -- Quality of Service, QoS identifier sent for each write transaction.
            M_AXI_AWQOS                   : out std_logic_vector(3 downto 0);
            -- Optional User-defined signal in the write address channel.
            M_AXI_AWUSER               : out std_logic_vector(1 downto 0);
            -- Write address valid. This signal indicates that
            -- the channel is signaling valid write address and control information.
            M_AXI_AWVALID               : out std_logic;
            -- Write address ready. This signal indicates that
            -- the slave is ready to accept an address and associated control signals
            M_AXI_AWREADY               : in std_logic;
            -- Master Interface Write Data.
            M_AXI_WDATA                   : out std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);
            -- Write strobes. This signal indicates which byte
            -- lanes hold valid data. There is one write strobe
            -- bit for each eight bits of the write data bus.
            M_AXI_WSTRB                   : out std_logic_vector(g_M_AXI_DATA_WIDTH/8-1 downto 0);
            -- Write last. This signal indicates the last transfer in a write burst.
            M_AXI_WLAST                   : out std_logic;
            -- Optional User-defined signal in the write data channel.
            M_AXI_WUSER                   : out std_logic_vector(1 downto 0);
            -- Write valid. This signal indicates that valid write
            -- data and strobes are available
            M_AXI_WVALID               : out std_logic;
            -- Write ready. This signal indicates that the slave
            -- can accept the write data.
            M_AXI_WREADY               : in std_logic;
            -- Master Interface Write Response.
            M_AXI_BID                   : in std_logic_vector(1 downto 0);
            -- Write response. This signal indicates the status of the write transaction.
            M_AXI_BRESP                   : in std_logic_vector(1 downto 0);
            -- Optional User-defined signal in the write response channel
            M_AXI_BUSER                   : in std_logic_vector(1 downto 0);
            -- Write response valid. This signal indicates that the
            -- channel is signaling a valid write response.
            M_AXI_BVALID               : in std_logic;
            -- Response ready. This signal indicates that the master
            -- can accept a write response.
            M_AXI_BREADY               : out std_logic
        );
    end component AXI_MM_write_interface;
    
    -----------------------------------------------------------------------
    component buffer_manager is
        generic (
                    g_CHANNELS          : integer := 12;
                    g_DATA_IN_WIDTH     : integer := 32;
                    g_DATA_OUT_WIDTH    : integer := 512;
                    g_M_AXI_ADDR_WIDTH  : integer := 32;
                    g_FIFO_BURST        : integer := 64;--256
                    g_BUFFER_SIZE       : integer := 8192;
                    g_DSP_BLOCK         : boolean := TRUE
                );
        port    ( 
                    -- acquisition control
                    acq_start_trig_i        : in    std_logic_vector(g_CHANNELS-1 downto 0);                                          -- start (restart)
                    acq_length_i            : in    t_STD_ARRAY_32(0 to g_CHANNELS-1);
                    acq_decimation_i        : in    t_STD_ARRAY_32(0 to g_CHANNELS-1);   
                    acq_debug_mode_i        : in    std_logic;
                    
                    -- memory buffers control
                    buffer_start_addr_i     : in    std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
                    buffer_enable_i         : in    std_logic_vector(g_CHANNELS-1 downto 0);
                    
                    -- status
                    busy_o                  : out   std_logic;
                    acq_finished_o          : out   std_logic;
                    acq_error_o             : out   std_logic;
                    fifo_full_o             : out   std_logic;
                        
                    -- Channel interface
                    ch_0_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_1_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_2_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_3_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_4_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_5_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_6_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_7_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_8_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_9_i          : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_10_i         : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    ch_11_i         : in    std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    
                    -- AXI command interface
                    AXI_WR_start_o           : out  std_logic;
                    AXI_WR_start_address_o   : out  std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
                    AXI_WR_data_size_o       : out  std_logic_vector(31 downto 0);
                    AXI_WR_burst_length_o    : out  std_logic_vector(8 downto 0);
                    AXI_WR_data_o            : out  std_logic_vector(g_DATA_OUT_WIDTH-1 downto 0);
                    AXI_WR_valid_o           : out  std_logic;
                    AXI_WR_wrnext_i          : in   std_logic;
                    AXI_WR_done_i            : in   std_logic;
                    AXI_WR_error_i           : in   std_logic;  -- currently unused
                    AXI_WR_busy_i            : in   std_logic;
                    
                    clk_i                    : in   std_logic;
                    reset_i                  : in   std_logic;
                    
                    AXI_clk_i                : in   std_logic;
                    AXI_reset_i              : in   std_logic
                ); 
    end component buffer_manager;
    
    --====================================================================
    ---                         Internal mode blocks                   ---
    --====================================================================
    component acq_memory_ctrl is
        Generic (
                    -- Width of S_AXI data bus
                    g_S_AXI_DATA_WIDTH      : integer  := 32;
                    -- Width of S_AXI address bus
                    g_S_AXI_ADDR_WIDTH      : integer  := 32;
                    -- data input width
                    g_DATA_IN_WIDTH         : integer := 32;
                    -- Number of buffers
                    g_BUFFERS               : integer := 16;
                    -- Size of the buffer
                    g_BUFFER_SIZE           : integer  := 8192
    
                );
    
        Port (
                    -- Global Clock Signal
                    S_AXI_aclk          : in std_logic;
                    -- Global Reset Signal. This Signal is Active LOW
                    S_AXI_aresetn       : in std_logic;
                    
                    -- AXI4-Lite Interface
                    -- Read address (issued by master, acceped by Slave)
                    S_AXI_araddr        : in std_logic_vector(g_S_AXI_ADDR_WIDTH-1 downto 0);
                    -- Protection type. This signal indicates the privilege
                    -- and security level of the transaction, and whether the
                    -- transaction is a data access or an instruction access.
                    S_AXI_arprot        : in std_logic_vector(2 downto 0);
                    -- Read address valid. This signal indicates that the channel
                    -- is signaling valid read address and control information.
                    S_AXI_arvalid       : in std_logic;
                    -- Read address ready. This signal indicates that the slave is
                    -- ready to accept an address and associated control signals.
                    S_AXI_arready       : out std_logic;
                    -- Read data (issued by slave)
                    S_AXI_rdata         : out std_logic_vector(g_S_AXI_DATA_WIDTH-1 downto 0);
                    -- Read response. This signal indicates the status of the
                    -- read transfer.
                    S_AXI_rresp         : out std_logic_vector(1 downto 0);
                    -- Read valid. This signal indicates that the channel is
                        -- signaling the required read data.
                    S_AXI_rvalid        : out std_logic;
                    -- Read ready. This signal indicates that the master can
                    -- accept the read data and response information.
                    S_AXI_rready        : in std_logic;
            
                    -- data output of BRAMs
                    BRAM_clk_i      : in std_logic; 
                    BRAM_en_i       : in std_logic;
                    BRAM_we_i       : in std_logic;
                    BRAM_addr_i     : in std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
                    BRAM_din_i      : in t_buff_array(0 to g_BUFFERS-1)      
             );
    
    end component;
    ----------------------------------------------------------------------
    component acquisition_ctrl is
        Generic (
                    g_DATA_IN_WIDTH : integer := 32;
                    g_BUFFER_SIZE   : integer := 8192
                );
        Port    (
                    -- trigger signals
                    acq_start_i     : in    std_logic;
                    acq_stop_i      : in    std_logic;
                    
                    -- status signals
                    acq_finished_o  : out   std_logic;
                    acq_error_o     : out   std_logic;
                    acq_frz_addr_o  : out   std_logic_vector(15 downto 0); -- to be replaced
                    acq_busy_o      : out   std_logic;
                    
                    -- control signals
                    acq_method_o            : out   std_logic_vector(1 downto 0);
                    acq_wr_o                : out   std_logic;
                    acq_addr_o              : out   std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
                    acq_debug_data_o        : out   std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                    acq_length_i            : in    std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
                    acq_debug_mode_i        : in    std_logic;
                    acq_circular_buff_en_i  : in    std_logic;
                    acq_decimation_i        : in    std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
                    
                    -- general
                    clk_i           : in    std_logic;
                    reset_i         : in    std_logic
                );
    end component;
    
begin
--=======================================================================================================

    -----------------------------------------------------------------------       
    busy_o              <= s_busy_reg;
    irq_acq_finished_o  <= s_acq_finished_reg;
    irq_acq_error_o     <= s_acq_error_reg;
    s_noFaults          <= '1' when unsigned(s_faults_SRFF) /= 0 else '0';
    s_acq_start_trig    <= s_soft_start_trig_reg(g_CHANNELS-1 downto 0) or acq_start_trig_i(g_CHANNELS-1 downto 0);
   
    ----------------------------------------------------------------------- 
    acqCore_registers:RegCtrl_acqCore
    port map(
            Clk => clk_i,
            Rst => reset_i,
            VMEAddr => cheb_Addr,
            VMERdData => cheb_RdData,
            VMEWrData => cheb_WrData,
            VMERdMem => cheb_RdMem,
            VMEWrMem => cheb_WrMem,
            VMERdDone => cheb_RdDone,
            VMEWrDone => cheb_WrDone,
            VMERdError => cheb_RdError,
            VMEWrError => cheb_WrError,
		    ipInfo_ident => c_IDENT,
            ipInfo_firmwareVersion => c_FIRMWARE_VERSION,
            ipInfo_memMapVersion => c_MEM_MAP_VESRION,
            ipInfo_sysControl => open,
            control_softReset => s_soft_reset_reg,
            control_clrFaults => s_clr_faults_reg,
            control_enable => s_enable_reg,
            control_softStartTrig => s_soft_start_trig_reg,
            control_debugMode => s_debug_mode_reg,
            status_busy => s_busy_reg,
            status_acqFinished => s_acq_finished_reg,
            status_noFaults => s_noFaults,
            status_noOverflow => '0',
            faults_acqFault => s_acq_error_reg,
            faults_SRFF => s_faults_SRFF,
            faults_ClrSRFF => s_clr_faults_reg,
            faultsEnable_acqFaultEnable => s_acq_fault_enable_reg,
            acqLength0 => s_acq_length_reg(0),
            acqLength1 => s_acq_length_reg(1),
            acqLength2 => s_acq_length_reg(2),
            acqLength3 => s_acq_length_reg(3),
            acqLength4 => s_acq_length_reg(4),
            acqLength5 => s_acq_length_reg(5),
            acqLength6 => s_acq_length_reg(6),
            acqLength7 => s_acq_length_reg(7),
            acqLength8 => s_acq_length_reg(8),
            acqLength9 => s_acq_length_reg(9),
            acqLength10 => s_acq_length_reg(10),
            acqLength11 => s_acq_length_reg(11),
            acqDecimation0 => s_acq_decimation_reg(0),
            acqDecimation1 => s_acq_decimation_reg(1),
            acqDecimation2 => s_acq_decimation_reg(2),
            acqDecimation3 => s_acq_decimation_reg(3),
            acqDecimation4 => s_acq_decimation_reg(4),
            acqDecimation5 => s_acq_decimation_reg(5),
            acqDecimation6 => s_acq_decimation_reg(6),
            acqDecimation7 => s_acq_decimation_reg(7),
            acqDecimation8 => s_acq_decimation_reg(8),
            acqDecimation9 => s_acq_decimation_reg(9),
            acqDecimation10 => s_acq_decimation_reg(10),
            acqDecimation11 => s_acq_decimation_reg(11),
            bufferStartAddress => s_buffer_start_addr_reg,
            bufferStartAddress_WrStrobe => open,
            bufferSize => std_logic_vector(to_unsigned(g_BUFFER_SIZE,32))
    );

    s_acq_reset         <= reset_i or s_soft_reset_reg;

    --=====================================================================
    ---                         External mode blocks                   ---
    --===================================================================== 
    EXT_MEM: if g_MODE = "external" generate
    
        -----------------------------------------------------------------------    
        s_acq_error_reg     <= s_acq_error and s_acq_fault_enable_reg;  
        acq_axi_busy_o      <= s_AXI_WR_busy;
        acq_axi_length_o    <= s_AXI_WR_burst_length;
        
        acq_reset_synchronizer: reset_synchronizer
            port map(
                        clk_i=>acq_clk_i,
                        rst_a_i=>s_acq_reset,
                        rst_sync_o=>s_acq_reset_sync
            );
        ----------------------------------------------------------------------- 
    
        s_AXI_reset_n   <= not M_AXI_reset_i;
        -----------------------------------------------------------------------    
        AXI_clock_domain_crossing:CDC_block
             generic map(
                     g_WIDTH => 1  
             )
             port map(
                 clk_i          => M_AXI_clk_i,
                 reset_i        => M_AXI_reset_i,
                 data_in_i(0)   => s_soft_reset_reg,
                 data_out_o(0)  => s_AXI_ext_reset
              ); 
        -----------------------------------------------------------------------  
        acq_finished_CDC:CDC_block
               generic map(
                       g_WIDTH => 1  
               )
               port map(
                   clk_i          => clk_i,
                   reset_i        => reset_i,
                   data_in_i(0)   => s_acq_finished,
                   data_out_o(0)  => s_acq_finished_reg
                ); 
        -----------------------------------------------------------------------
        AXI_MM_write_interface_0: component AXI_MM_write_interface
            generic map (
                     g_M_AXI_ADDR_WIDTH => g_M_AXI_ADDR_WIDTH,
                     g_M_AXI_DATA_WIDTH => g_M_AXI_DATA_WIDTH
                 )
            
            port map (
                AXI_WR_burst_length_i =>s_AXI_WR_burst_length,
                AXI_WR_busy_o =>s_AXI_WR_busy,
                AXI_WR_data_i =>s_AXI_WR_data,
                AXI_WR_data_size_i =>s_AXI_WR_data_size,
                AXI_WR_done_o =>s_AXI_WR_done,
                AXI_WR_error_o =>s_AXI_WR_error,
                AXI_WR_start_i =>s_AXI_WR_start,
                AXI_WR_start_address_i =>s_AXI_WR_start_address,
                AXI_WR_valid_i =>s_AXI_WR_valid,
                AXI_WR_wrnext_o =>s_AXI_WR_wrnext,
                
                AXI_ext_reset_i => s_AXI_ext_reset,
                
                M_AXI_ACLK =>M_AXI_clk_i,
                M_AXI_ARESETN =>s_AXI_reset_n,
                M_AXI_AWADDR =>M_AXI_awaddr,
                M_AXI_AWBURST =>M_AXI_awburst,
                M_AXI_AWCACHE =>M_AXI_awcache,
                M_AXI_AWID =>M_AXI_awid,
                M_AXI_AWLEN =>M_AXI_awlen,
                M_AXI_AWLOCK =>M_AXI_awlock,
                M_AXI_AWPROT =>M_AXI_awprot,
                M_AXI_AWQOS =>M_AXI_awqos,
                M_AXI_AWREADY =>M_AXI_awready,
                M_AXI_AWSIZE =>M_AXI_awsize,
                M_AXI_AWUSER =>M_AXI_awuser,
                M_AXI_AWVALID =>M_AXI_awvalid,
                M_AXI_BID =>M_AXI_bid,
                M_AXI_BREADY =>M_AXI_bready,
                M_AXI_BRESP =>M_AXI_bresp,
                M_AXI_BUSER =>M_AXI_buser,
                M_AXI_BVALID =>M_AXI_bvalid,
                M_AXI_WDATA =>M_AXI_wdata,
                M_AXI_WLAST =>M_AXI_wlast,
                M_AXI_WREADY =>M_AXI_wready,
                M_AXI_WSTRB =>M_AXI_wstrb,
                M_AXI_WUSER =>M_AXI_wuser,
                M_AXI_WVALID  => M_AXI_wvalid
            );
        -----------------------------------------------------------------------    
        
        s_AXI_reset <= M_AXI_reset_i or s_AXI_ext_reset;
        buffer_manager_0: component buffer_manager
            generic map(
                     g_CHANNELS => g_CHANNELS,
                     g_DATA_IN_WIDTH => g_DATA_IN_WIDTH,
                     g_DATA_OUT_WIDTH => g_M_AXI_DATA_WIDTH,
                     g_M_AXI_ADDR_WIDTH => g_M_AXI_ADDR_WIDTH,
                     g_FIFO_BURST => g_FIFO_BURST,
                     g_BUFFER_SIZE => g_BUFFER_SIZE,
                     g_DSP_BLOCK => g_DSP_BLOCK
                )
            port map (
                AXI_WR_burst_length_o => s_AXI_WR_burst_length,
                AXI_WR_busy_i => s_AXI_WR_busy,
                AXI_WR_data_o => s_AXI_WR_data,
                AXI_WR_data_size_o => s_AXI_WR_data_size,
                AXI_WR_done_i => s_AXI_WR_done,
                AXI_WR_error_i => s_AXI_WR_error,
                AXI_WR_start_address_o => s_AXI_WR_start_address,
                AXI_WR_start_o => s_AXI_WR_start,
                AXI_WR_valid_o => s_AXI_WR_valid,
                AXI_WR_wrnext_i => s_AXI_WR_wrnext,
                buffer_enable_i => s_enable_reg(g_CHANNELS-1 downto 0),
                buffer_start_addr_i => s_buffer_start_addr_reg(g_M_AXI_ADDR_WIDTH-1 downto 0),
                busy_o  => s_busy_reg,
                acq_length_i => s_acq_length_reg(0 to g_CHANNELS-1),
                acq_decimation_i => s_acq_decimation_reg(0 to g_CHANNELS-1),
                ch_0_i => ch_0_i,
                ch_1_i => ch_1_i,
                ch_2_i => ch_2_i,
                ch_3_i => ch_3_i,
                ch_4_i => ch_4_i,
                ch_5_i => ch_5_i,
                ch_6_i => ch_6_i,
                ch_7_i => ch_7_i,
                ch_8_i => ch_8_i,
                ch_9_i => ch_9_i,
                ch_10_i => ch_10_i,
                ch_11_i => ch_11_i,
                clk_i => acq_clk_i,
                reset_i => s_acq_reset_sync,
                acq_start_trig_i => s_acq_start_trig,
                acq_debug_mode_i => s_debug_mode_reg,
                AXI_clk_i => M_AXI_clk_i,
                acq_finished_o  => s_acq_finished,
                acq_error_o     => s_acq_error,
                fifo_full_o => fifo_full_o,
                AXI_reset_i => s_AXI_reset
            );
    end generate;    
    
    --=====================================================================
    ---                         Internal mode blocks                    ---
    --===================================================================== 

    INT_MEM: if g_MODE = "internal" generate
        s_buff_inputs(0) <= buff_0_i;
        s_buff_inputs(1) <= buff_1_i;
        s_buff_inputs(2) <= buff_2_i;
        s_buff_inputs(3) <= buff_3_i;
        s_buff_inputs(4) <= buff_4_i;
        s_buff_inputs(5) <= buff_5_i;
        s_buff_inputs(6) <= buff_6_i;
        s_buff_inputs(7) <= buff_7_i;
        s_buff_inputs(8) <= buff_8_i;
        s_buff_inputs(9) <= buff_9_i;
        s_buff_inputs(10) <= buff_10_i;
        s_buff_inputs(11) <= buff_11_i;
        s_buff_inputs(12) <= buff_12_i;
        s_buff_inputs(13) <= buff_13_i;
        s_buff_inputs(14) <= buff_14_i;
        s_buff_inputs(15) <= buff_15_i;
        s_buff_inputs(16) <= buff_16_i;
        s_buff_inputs(17) <= buff_17_i;
        s_buff_inputs(18) <= buff_18_i;
        s_buff_inputs(19) <= buff_19_i;
        s_buff_inputs(20) <= buff_20_i;
        s_buff_inputs(21) <= buff_21_i;
        s_buff_inputs(22) <= buff_22_i;
        s_buff_inputs(23) <= buff_23_i;
        s_buff_inputs(24) <= buff_24_i;
        s_buff_inputs(25) <= buff_25_i;
        s_buff_inputs(26) <= buff_26_i;
        s_buff_inputs(27) <= buff_27_i;
        s_buff_inputs(28) <= buff_28_i;
        s_buff_inputs(29) <= buff_29_i;
        s_buff_inputs(30) <= buff_30_i;
        s_buff_inputs(31) <= buff_31_i;
    
        -- debug data selection
        process(s_buff_inputs,s_acq_debug_data,s_debug_mode_reg)
        begin
            for I in 0 to g_BUFFERS-1 loop
                if(s_debug_mode_reg = '0')then
                    s_buff_data(I) <= s_buff_inputs(I);
                else
                    s_buff_data(I) <= s_acq_debug_data;
                end if;
            end loop;
        end process;
    
        acq_reset_synchronizer: reset_synchronizer
        port map(
                    clk_i=>acq_clk_i,
                    rst_a_i=>s_acq_reset,
                    rst_sync_o=>s_acq_reset_sync
        );
    
        acq_memory_ctrl_0: acq_memory_ctrl
            Generic map(
                        g_S_AXI_DATA_WIDTH => 32,                                            -- AXI4-LITE 32 bit width
                        g_S_AXI_ADDR_WIDTH => f_find_width(g_BUFFERS*g_BUFFER_SIZE*4),       -- AXI4-LITE address range in bytes (not in 32-bit word)
                        g_DATA_IN_WIDTH => g_DATA_IN_WIDTH,
                        g_BUFFERS => g_BUFFERS,
                        g_BUFFER_SIZE => g_BUFFER_SIZE
                    )
        
            Port map(
                        S_AXI_aclk     => S_AXI_aclk,
                        S_AXI_aresetn  => S_AXI_aresetn,
                        S_AXI_araddr   => S_AXI_araddr(f_find_width(g_BUFFERS*g_BUFFER_SIZE*4)-1 downto 0),
                        S_AXI_arprot   => S_AXI_arprot,
                        S_AXI_arvalid  => S_AXI_arvalid,
                        S_AXI_arready  => S_AXI_arready,
                        S_AXI_rdata    => S_AXI_rdata,
                        S_AXI_rresp    => S_AXI_rresp,
                        S_AXI_rvalid   => S_AXI_rvalid,
                        S_AXI_rready   => S_AXI_rready,

                        BRAM_clk_i     => acq_clk_i,
                        BRAM_en_i      => '1',
                        BRAM_we_i      => s_BRAM_we,
                        BRAM_addr_i    => s_BRAM_addr,
                        BRAM_din_i     => s_buff_data(0 to g_BUFFERS-1)  
                 );
        ----------------------------------------------------------------------
        
        acquisition_ctrl_0:acquisition_ctrl
            Generic map(
                        g_DATA_IN_WIDTH => g_DATA_IN_WIDTH,
                        g_BUFFER_SIZE   => g_BUFFER_SIZE
                    )
            Port map(
                        acq_start_i             => s_acq_start_trig(0),
                        acq_stop_i              => '0',
                        acq_finished_o          => s_acq_finished_reg, --same clock phase than the register clock
                        acq_error_o             => s_acq_error_reg,
                        acq_frz_addr_o          => open,
                        acq_method_o            => open,
                        acq_wr_o                => s_BRAM_we,
                        acq_addr_o              => s_BRAM_addr,
                        acq_debug_data_o        => s_acq_debug_data,
                        acq_length_i            => s_acq_length_reg(0)(f_find_width(g_BUFFER_SIZE)-1 downto 0),
                        acq_debug_mode_i        => s_debug_mode_reg,
                        acq_circular_buff_en_i  => '0',
                        acq_decimation_i        => s_acq_decimation_reg(0)(f_find_width(g_BUFFER_SIZE)-1 downto 0),
                        acq_busy_o              => s_busy_reg,
                        clk_i                   => acq_clk_i,
                        reset_i                 => s_acq_reset_sync
                    );
    end generate;
    
end STRUCTURE;
