-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, acquisition_ctrl --
-- --
-------------------------------------------------------------------------------
--
-- unit name: Acquisition control
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 14/03/2019
--
-- version: 1.0
--
-- description: main block who controls the acquisition
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- data acquisition core package
library work;
use work.ACQ_CORE_PACK.all;

entity acquisition_ctrl is
    Generic (
                g_DATA_IN_WIDTH : integer := 32;
                g_BUFFER_SIZE   : integer := 8192
            );
    Port    (
                -- trigger signals
                acq_start_i     : in    std_logic;
                acq_stop_i      : in    std_logic;
                
                -- status signals
                acq_finished_o  : out   std_logic;
                acq_error_o     : out   std_logic;
                acq_frz_addr_o  : out   std_logic_vector(15 downto 0); -- to be replaced
                acq_busy_o      : out   std_logic;
                
                -- control signals
                acq_method_o            : out   std_logic_vector(1 downto 0);
                acq_wr_o                : out   std_logic;
                acq_addr_o              : out   std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
                acq_debug_data_o        : out   std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
                acq_length_i            : in    std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
                acq_debug_mode_i        : in    std_logic;
                acq_circular_buff_en_i  : in    std_logic;
                acq_decimation_i        : in    std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
                
                -- general
                clk_i           : in    std_logic;
                reset_i         : in    std_logic
            );
end acquisition_ctrl;

architecture Behavioral of acquisition_ctrl is
    ----------------------------------------------------------------------
    -- constant instantation
    constant c_DATA_WIDTH_IN_BYTES  : positive := g_DATA_IN_WIDTH/8;
    constant c_DIV_FACTOR           : positive := f_find_width(c_DATA_WIDTH_IN_BYTES);
    ----------------------------------------------------------------------
    -- type declaration
    type t_acquisition  is (IDLE,ACQUISITION);
    
    ----------------------------------------------------------------------
    -- signal instantation 
    signal s_data_counter       : unsigned(acq_length_i'range);
    signal s_acq_addr           : std_logic_vector(acq_addr_o'range);
    signal s_acq_wr             : std_logic;
    signal s_acq_debug_data     : std_logic_vector(acq_debug_data_o'range);
    signal s_acquisition        : std_logic;
    signal s_main_state         : t_acquisition;
    signal s_acq_start          : std_logic;
    signal s_dec_clk_en         : std_logic;
    signal s_dec_clk_cnt        : unsigned(acq_decimation_i'range);
    signal s_acq_finished       : std_logic;
    
begin
   
    ----------------------------------------------------------------------
    acq_debug_data_o    <= s_acq_debug_data;
    acq_addr_o          <= s_acq_addr;
    acq_wr_o            <= s_acq_wr;
    acq_busy_o          <= s_acquisition;
    acq_frz_addr_o      <= (others => '0'); --temporarly 
    acq_error_o         <= '0';             --temporarly 
    acq_method_o        <= (others => '0'); --temporarly 
    acq_finished_o      <= s_acq_finished;

    --=============================================================================
    -- Begin of the process "decimation"
    -- Generate down sampling clock for the acquisition
    --=============================================================================                 
    p_decimation: process (clk_i,reset_i) begin
        if(reset_i = '1')then
            s_dec_clk_en    <= '0';
            s_dec_clk_cnt   <= (others => '0');
        elsif rising_edge(clk_i) then
            -- default
            s_dec_clk_en    <= '0';
            s_dec_clk_cnt   <= (others => '0');
            
            -- Only when acquisition was started
            if(s_acquisition = '1')then
                s_dec_clk_cnt   <= s_dec_clk_cnt + 1;
                            
                if(s_dec_clk_cnt >= unsigned(acq_decimation_i))then
                    s_dec_clk_en    <= '1';
                    s_dec_clk_cnt   <= (others => '0');
                end if;
            end if;
        end if;
    end process p_decimation;    

    --=============================================================================
    -- Begin of the process "ACQUISITION"
    -- acquisition manager process
    -- start acquisition on start trigger, store a number of samples defined by the length 
    --=============================================================================
    p_acquisition: process (clk_i,reset_i) 
    begin    
        if(reset_i = '1')then
            s_main_state        <= IDLE;
            s_data_counter      <= (others => '0');
            s_acq_finished      <= '0';
            s_acquisition       <= '0';
        elsif rising_edge(clk_i) then  
        
            -- ADD RESTART MECHANISM HERE!!!!!!!!!!!!!!!!!!!!!!
            --default
            s_acq_start     <= acq_start_i;
            
            -- main state machine
            case s_main_state is
                when IDLE =>
                    -- waiting start trigger (on rising edge)
                    if(acq_start_i = '1' and s_acq_start = '0')then
                       s_main_state     <= ACQUISITION;  
                       s_acquisition    <= '1';
                       s_acq_finished   <= '0';
                    end if;
                    
                    s_data_counter  <= (others => '0');
                when ACQUISITION =>
                    -- perform acquisition with decimation 
                    if(s_dec_clk_en = '1')then
                        --default 
                        
                        -- count number of data
                        s_data_counter <= s_data_counter + 1;
                        
                        -- end of acquisition
                        if(s_data_counter >= (unsigned(acq_length_i)-1))then
                            s_main_state    <= IDLE;
                            s_acq_finished  <= '1';
                            s_data_counter  <= (others => '0');
                            s_acquisition   <= '0';
                        end if;
                    end if;
                when others => s_main_state  <= IDLE;
            end case;
        end if;
    end process;
    
    --=============================================================================
    -- Begin of the process "WRITING TO BRAM"
    -- writing the data into the BRAM
    --=============================================================================
    p_BRAM_WRITING:process(clk_i,reset_i)
    begin
        if(reset_i = '1')then
            s_acq_wr            <= '0';
            s_acq_addr          <= (others => '0');
            s_acq_debug_data    <= (others => '0');
        elsif rising_edge(clk_i) then  
            -- default
            s_acq_wr    <= '0';

            if(s_acquisition = '1' and s_dec_clk_en = '1')then
                s_acq_wr                                    <= '1';
                s_acq_addr                                  <= (others => '0');
                s_acq_addr                                  <= std_logic_vector(s_data_counter);
                s_acq_debug_data(s_data_counter'range)      <= std_logic_vector(s_data_counter);
            else
                s_acq_addr          <= (others => '0');
                s_acq_debug_data    <= (others => '0');
            end if;
        end if;
    end process;
end Behavioral;
