-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, AXI_MM_write_interface --
-- --
-------------------------------------------------------------------------------
--
-- unit name: AXI_MM_write_interface
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 08/05/2018
--
-- version: 1.0
--
-- description: AXI-full master interface (write only)
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: 
-- 
--
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity AXI_MM_write_interface is
	generic (
		-- Width of Address Bus
		g_M_AXI_ADDR_WIDTH	: integer	:= 32;
		-- Width of Data Bus
		g_M_AXI_DATA_WIDTH	: integer	:= 512
	);
	port (
		-- AXI command interface
        AXI_WR_start_i             : in     std_logic;
        AXI_WR_start_address_i     : in     std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
        AXI_WR_data_size_i         : in     std_logic_vector(31 downto 0);
        AXI_WR_burst_length_i      : in     std_logic_vector(8 downto 0);
        AXI_WR_done_o              : out    std_logic;
        AXI_WR_error_o             : out    std_logic;
        AXI_WR_busy_o              : out    std_logic;
        AXI_WR_data_i              : in     std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);
        AXI_WR_valid_i             : in     std_logic;
        AXI_WR_wrnext_o            : out    std_logic;
        
        -- external reset control
        AXI_ext_reset_i            : in     std_logic;  

		-- Global Clock Signal.
		M_AXI_ACLK	               : in    std_logic;
		-- Global Reset Singal. This Signal is Active Low
		M_AXI_ARESETN	           : in std_logic;
		-- Master Interface Write Address ID
		M_AXI_AWID	               : out std_logic_vector(3 downto 0);
		-- Master Interface Write Address
		M_AXI_AWADDR               : out std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
		-- Burst length. The burst length gives the exact number of transfers in a burst
		M_AXI_AWLEN	               : out std_logic_vector(7 downto 0);
		-- Burst size. This signal indicates the size of each transfer in the burst
		M_AXI_AWSIZE	           : out std_logic_vector(2 downto 0);
		-- Burst type. The burst type and the size information, 
		-- determine how the address for each transfer within the burst is calculated.
		M_AXI_AWBURST	           : out std_logic_vector(1 downto 0);
		-- Lock type. Provides additional information about the
		-- atomic characteristics of the transfer.
		M_AXI_AWLOCK	           : out std_logic;
		-- Memory type. This signal indicates how transactions
		-- are required to progress through a system.
		M_AXI_AWCACHE	           : out std_logic_vector(3 downto 0);
		-- Protection type. This signal indicates the privilege
		-- and security level of the transaction, and whether
		-- the transaction is a data access or an instruction access.
		M_AXI_AWPROT	           : out std_logic_vector(2 downto 0);
		-- Quality of Service, QoS identifier sent for each write transaction.
		M_AXI_AWQOS	               : out std_logic_vector(3 downto 0);
		-- Optional User-defined signal in the write address channel.
		M_AXI_AWUSER	           : out std_logic_vector(1 downto 0);
		-- Write address valid. This signal indicates that
		-- the channel is signaling valid write address and control information.
		M_AXI_AWVALID	           : out std_logic;
		-- Write address ready. This signal indicates that
		-- the slave is ready to accept an address and associated control signals
		M_AXI_AWREADY	           : in std_logic;
		-- Master Interface Write Data.
		M_AXI_WDATA	               : out std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);
		-- Write strobes. This signal indicates which byte
		-- lanes hold valid data. There is one write strobe
		-- bit for each eight bits of the write data bus.
		M_AXI_WSTRB	               : out std_logic_vector(g_M_AXI_DATA_WIDTH/8-1 downto 0);
		-- Write last. This signal indicates the last transfer in a write burst.
		M_AXI_WLAST	               : out std_logic;
		-- Optional User-defined signal in the write data channel.
		M_AXI_WUSER	               : out std_logic_vector(1 downto 0);
		-- Write valid. This signal indicates that valid write
		-- data and strobes are available
		M_AXI_WVALID	           : out std_logic;
		-- Write ready. This signal indicates that the slave
		-- can accept the write data.
		M_AXI_WREADY	           : in std_logic;
		-- Master Interface Write Response.
		M_AXI_BID	               : in std_logic_vector(1 downto 0);
		-- Write response. This signal indicates the status of the write transaction.
		M_AXI_BRESP	               : in std_logic_vector(1 downto 0);
		-- Optional User-defined signal in the write response channel
		M_AXI_BUSER	               : in std_logic_vector(1 downto 0);
		-- Write response valid. This signal indicates that the
		-- channel is signaling a valid write response.
		M_AXI_BVALID	           : in std_logic;
		-- Response ready. This signal indicates that the master
		-- can accept a write response.
		M_AXI_BREADY	           : out std_logic
	);
end AXI_MM_write_interface;

architecture implementation of AXI_MM_write_interface is

	-- function called clogb2 that returns an integer which has the
	--value of the ceiling of the log base 2

	function clogb2 (bit_depth : integer) return integer is            
	 	variable depth  : integer := bit_depth;                               
	 	variable count  : integer := 1;                                       
	 begin                                                                   
	 	 for clogb2 in 1 to bit_depth loop  -- Works for up to 32 bit integers
	      if (bit_depth <= 2) then                                           
	        count := 1;                                                      
	      else                                                               
	        if(depth <= 1) then                                              
	 	       count := count;                                                
	 	     else                                                             
	 	       depth := depth / 2;                                            
	          count := count + 1;                                            
	 	     end if;                                                          
	 	   end if;                                                            
	   end loop;                                                             
	   return(count);        	                                              
	 end;   
	 
    ----------------------------------------------------------------------
    -- STATE MACHINE
	 type wr_state is ( s_IDLE, 		-- This state initiates AXI4Lite transaction  
										-- after the state machine changes state to INIT_WRITE
										-- when there is 0 to 1 transition on INIT_AXI_TXN
						s_INIT_WRITE,   -- This state initializes write transaction,
										-- once writes are done, the state machine 
										-- changes state to s_INIT_DONE 
						s_INIT_DONE);

	 signal axi_write_state  : wr_state ; 
	----------------------------------------------------------------------
	
	-- AXI4FULL signals
	--AXI4 internal temp signals
	signal axi_awaddr      : std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_awvalid     : std_logic;
	signal axi_wdata       : std_logic_vector(g_M_AXI_DATA_WIDTH-1 downto 0);
	signal axi_wlast       : std_logic;
	signal axi_wvalid      : std_logic;
	signal axi_bready      : std_logic;
	signal axi_araddr      : std_logic_vector(g_M_AXI_ADDR_WIDTH-1 downto 0);
	signal axi_arvalid     : std_logic;
	signal axi_rready      : std_logic;
	--write beat count in a burst
	signal write_index     : unsigned(M_AXI_AWLEN'range);
	--size of C_M_AXI_BURST_LEN length burst in bytes
	signal burst_size_bytes	           : unsigned(15 downto 0);
	--The burst counters are used to track the number of burst transfers of C_M_AXI_BURST_LEN burst length needed to transfer 2^C_MASTER_LENGTH bytes of data.
	--signal write_burst_counter	: unsigned(31 downto 0);
	signal s_write_burst_done          : std_logic;
	
	signal start_single_burst_write	   : std_logic;
	--signal s_AXI_WR_data_size  : std_logic_vector(AXI_WR_data_size'range);
	signal s_AXI_WR_start_address      : unsigned(AXI_WR_start_address_i'range);
    signal s_AXI_WR_burst_length       : unsigned(M_AXI_AWLEN'range);
    signal s_AXI_WR_no_burst           : unsigned(AXI_WR_data_size_i'range); -- size to optimize
	signal writes_done	               : std_logic;
	signal s_AXI_WR_done               : std_logic;
    signal s_AXI_WR_error              : std_logic;
    signal s_AXI_WR_busy               : std_logic;
	signal burst_write_active	       : std_logic;
	signal burst_read_active	       : std_logic;
	--Interface response error flags
	signal write_resp_error	           : std_logic;
	signal wnext	                   : std_logic;
	signal init_txn_ff	               : std_logic;
	signal init_txn_ff2	               : std_logic;
	signal init_txn_edge	           : std_logic;
	signal init_txn_pulse	           : std_logic;
	signal s_AXI_ext_reset_waiting     : std_logic;
	signal s_AXI_ext_reset             : std_logic;
	signal s_AXI_reset_n               : std_logic;

    -- for simulation
    constant c_SIM_FIFO_DATA_WIDTH     : integer := 64;
    signal   s_sim_data_cnt            : unsigned(c_SIM_FIFO_DATA_WIDTH-1 downto 0);

begin
	-- I/O Connections assignments

	--I/O Connections. Write Address (AW)
	M_AXI_AWID     <= (others => '0');
	--The AXI address is a concatenation of the start base address + active offset range
	M_AXI_AWADDR   <= std_logic_vector( s_AXI_WR_start_address + unsigned(axi_awaddr) );
	--Burst LENgth is number of transaction beats, minus 1
	M_AXI_AWLEN	   <= std_logic_vector(s_AXI_WR_burst_length);
	--Size should be g_M_AXI_DATA_WIDTH, in 2^SIZE bytes, otherwise narrow bursts are used
	M_AXI_AWSIZE   <= std_logic_vector( to_unsigned(clogb2((g_M_AXI_DATA_WIDTH/8)-1), 3) );
	--INCR burst type is usually used, except for keyhole bursts
	M_AXI_AWBURST  <= "01";
	M_AXI_AWLOCK   <= '0';
	--Update value to 4'b0011 if coherent accesses to be used via the Zynq ACP port. Not Allocated, Modifiable, not Bufferable. Not Bufferable since this example is meant to test memory, not intermediate cache. 
	M_AXI_AWCACHE  <= "0010";
	M_AXI_AWPROT   <= "000";
	M_AXI_AWQOS    <= x"0";
	M_AXI_AWUSER   <= (others => '1');
	M_AXI_AWVALID  <= axi_awvalid;
	--Write Data(W)
	M_AXI_WDATA	   <= axi_wdata;
	--All bursts are complete and aligned in this example
	M_AXI_WSTRB	   <= (others => '1');
	M_AXI_WLAST	   <= axi_wlast;
	M_AXI_WUSER	   <= (others => '0');
	M_AXI_WVALID   <= axi_wvalid;
	--Write Response (B)
	M_AXI_BREADY   <= axi_bready;
	
	AXI_WR_done_o   <=      s_AXI_WR_done;
    AXI_WR_error_o  <=      s_AXI_WR_error;
    AXI_WR_busy_o   <=      s_AXI_WR_busy;
    AXI_WR_wrnext_o <=      wnext  and not axi_wlast; --and init_txn_pulse;

    s_AXI_reset_n   <=      (not s_AXI_ext_reset) and M_AXI_ARESETN;

    --Burst size in bytes
    burst_size_bytes <= to_unsigned((to_integer(s_AXI_WR_burst_length)+1)* (g_M_AXI_DATA_WIDTH/8),burst_size_bytes'length);
    init_txn_pulse	 <= ( not init_txn_ff2)  and  init_txn_ff;

    ------------------------------------------------------------------------------------------------------------------------
    -- external reset manager
    -- The reset is performed only if no AXI transaction is in progress
    process(M_AXI_ACLK,M_AXI_ARESETN)
    begin
        if(M_AXI_ARESETN = '0')then
            s_AXI_ext_reset_waiting <= '0';
            s_AXI_ext_reset         <= '0'; 
        elsif(rising_edge (M_AXI_ACLK)) then    
            --default
            s_AXI_ext_reset         <= '0';
            
            if(AXI_ext_reset_i = '1')then
                s_AXI_ext_reset_waiting <= '1';
            elsif(s_AXI_WR_busy = '0' and s_AXI_ext_reset_waiting = '1')then
                s_AXI_ext_reset_waiting <= '0';  
                s_AXI_ext_reset         <= '1';  
            end if;
        end if;        
    end process;

    ------------------------------------------------------------------------------------------------------------------------

	--Generate a pulse to initiate AXI transaction.
	process(M_AXI_ACLK)                                                          
	begin                                                                             
	  if (rising_edge (M_AXI_ACLK)) then                                              
	      -- Initiates AXI transaction delay        
	    if (s_AXI_reset_n = '0' ) then                                                
	       init_txn_ff <= '0';                                                   
	       init_txn_ff2 <= '0';                                                          
	    else                                                                                       
	       init_txn_ff <= AXI_WR_start_i;
	       init_txn_ff2 <= init_txn_ff;                                                                     
	    end if;                                                                       
	  end if;                                                                         
	end process; 


	----------------------
	--Write Address Channel
	----------------------

	-- The purpose of the write address channel is to request the address and 
	-- command information for the entire transaction.  It is a single beat
	-- of information.

	-- The AXI4 Write address channel in this example will continue to initiate
	-- write commands as fast as it is allowed by the slave/interconnect.
	-- The address will be incremented on each accepted address transaction,
	-- by burst_size_byte to point to the next address. 

	  process(M_AXI_ACLK)                                            
	  begin                                                                
	    if (rising_edge (M_AXI_ACLK)) then                                 
	      if (s_AXI_reset_n = '0' or init_txn_pulse = '1') then                                   
	        axi_awvalid <= '0';                                            
	      else                                                             
	        -- If previously not valid , start next transaction            
	        if (axi_awvalid = '0' and start_single_burst_write = '1') then 
	          axi_awvalid <= '1';                                          
	          -- Once asserted, VALIDs cannot be deasserted, so axi_awvalid
	          -- must wait until transaction is accepted                   
	        elsif (M_AXI_AWREADY = '1' and axi_awvalid = '1') then         
	          axi_awvalid <= '0';                                          
	        else                                                           
	          axi_awvalid <= axi_awvalid;                                  
	        end if;                                                        
	      end if;                                                          
	    end if;                                                            
	  end process;                                                         
	                                                                       
	-- Next address after AWREADY indicates previous address acceptance    
	  process(M_AXI_ACLK)                                                  
	  begin                                                                
	    if (rising_edge (M_AXI_ACLK)) then                                 
	      if (s_AXI_reset_n = '0' or init_txn_pulse = '1') then                                   
	        axi_awaddr <= (others => '0');                                 
	      else                                                             
	        if (M_AXI_AWREADY= '1' and axi_awvalid = '1') then             
	          axi_awaddr <= std_logic_vector(unsigned(axi_awaddr) + burst_size_bytes);                 
	        end if;                                                        
	      end if;                                                          
	    end if;                                                            
	  end process;                                                         


	----------------------
	--Write Data Channel
	----------------------

	--The write data will continually try to push write data across the interface.

	--The amount of data accepted will depend on the AXI slave and the AXI
	--Interconnect settings, such as if there are FIFOs enabled in interconnect.

	--Note that there is no explicit timing relationship to the write address channel.
	--The write channel has its own throttling flag, separate from the AW channel.

	--Synchronization between the channels must be determined by the user.

	--The simpliest but lowest performance would be to only issue one address write
	--and write data burst at a time.

	--Forward movement occurs when the write channel is valid and ready

	  wnext <= M_AXI_WREADY and axi_wvalid;                                       
	                                                                                    
	-- WVALID logic, similar to the axi_awvalid always block above                      
	  process(M_AXI_ACLK)                                                               
	  begin                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                              
	      if (s_AXI_reset_n = '0' or init_txn_pulse = '1') then                                                
	        axi_wvalid <= '0';                                                          
	      else                                                                          
	        if (axi_wvalid = '0' and start_single_burst_write = '1') then               
	          -- If previously not valid, start next transaction                        
	          axi_wvalid <= '1';                                                        
	          --     /* If WREADY and too many writes, throttle WVALID                  
	          --      Once asserted, VALIDs cannot be deasserted, so WVALID             
	          --      must wait until burst is complete with WLAST */                   
	        elsif (wnext = '1' and axi_wlast = '1') then                                
	          axi_wvalid <= '0';                                                        
	        else                                                                        
	          axi_wvalid <= axi_wvalid;                                                 
	        end if;                                                                     
	      end if;                                                                       
	    end if;                                                                         
	  end process;                                                                      
	                                                                                    
	--WLAST generation on the MSB of a counter underflow                                
	-- WVALID logic, similar to the axi_awvalid always block above                      
	  process(M_AXI_ACLK)                                                               
	  begin                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                              
	      if (s_AXI_reset_n = '0' or init_txn_pulse = '1') then                                                
	        axi_wlast <= '0';                                                           
	        -- axi_wlast is asserted when the write index                               
	        -- count reaches the penultimate count to synchronize                       
	        -- with the last write data when write_index is b1111                       
	        -- elsif (&(write_index[C_TRANSACTIONS_NUM-1:1])&& ~write_index[0] && wnext)
	      else                                                                          
	        if ((write_index = (s_AXI_WR_burst_length-1) and s_AXI_WR_burst_length >= 1 and wnext = '1') or s_AXI_WR_burst_length = 0) then
	          axi_wlast <= '1';                                                         
	          -- Deassrt axi_wlast when the last write data has been                    
	          -- accepted by the slave with a valid response                            
	        elsif (wnext = '1') then                                                    
	          axi_wlast <= '0';                                                         
	        elsif (axi_wlast = '1' and s_AXI_WR_burst_length = 0) then                      
	          axi_wlast <= '0';                                                         
	        end if;                                                                     
	      end if;                                                                       
	    end if;                                                                         
	  end process;                                                                      
	                                                                                    
	-- Burst length counter. Uses extra counter register bit to indicate terminal       
	-- count to reduce decode logic */                                                  
	  process(M_AXI_ACLK)                                                               
	  begin                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                              
	      if (s_AXI_reset_n = '0' or start_single_burst_write = '1' or init_txn_pulse = '1') then               
	        write_index <= (others => '0');                                             
	      else                                                                          
	        if (wnext = '1' and (write_index /= s_AXI_WR_burst_length)) then                
	          write_index <= write_index + 1;                                         
	        end if;                                                                     
	      end if;                                                                       
	    end if;                                                                         
	  end process;                                                                      
	                                                                                    
	-- Write Data                                                        
	  process(M_AXI_ACLK)                                                                                                              
	  begin                                                                             
	    if (rising_edge (M_AXI_ACLK)) then                                              
	      if (s_AXI_reset_n = '0') then                                                                                        
             axi_wdata <= (others => '0');                                           
	      else                                                             
	        -- when wnext = '1' or on rising edge of start             
	        if (wnext = '1' or (init_txn_ff = '0' and AXI_WR_start_i = '1')) then   
                axi_wdata <= AXI_WR_data_i;     
            end if;                                                                                             
	      end if;                                                                       
	    end if;                                                                         
	  end process;                                                                      


	------------------------------
	--Write Response (B) Channel
	------------------------------

	--The write response channel provides feedback that the write has committed
	--to memory. BREADY will occur when all of the data and the write address
	--has arrived and been accepted by the slave.

	--The write issuance (number of outstanding write addresses) is started by 
	--the Address Write transfer, and is completed by a BREADY/BRESP.

	--While negating BREADY will eventually throttle the AWREADY signal, 
	--it is best not to throttle the whole data channel this way.

	--The BRESP bit [1] is used indicate any errors from the interconnect or
	--slave for the entire write burst. This example will capture the error 
	--into the ERROR output. 

	  process(M_AXI_ACLK)                                             
	  begin                                                                 
	    if (rising_edge (M_AXI_ACLK)) then                                  
	      if (s_AXI_reset_n = '0' or init_txn_pulse = '1') then                                    
	        axi_bready <= '0';                                              
	        -- accept/acknowledge bresp with axi_bready by the master       
	        -- when M_AXI_BVALID is asserted by slave                       
	      else                                                              
	        if (M_AXI_BVALID = '1' and axi_bready = '0') then               
	          axi_bready <= '1';                                            
	          -- deassert after one clock cycle                             
	        elsif (axi_bready = '1') then                                   
	          axi_bready <= '0';                                            
	        end if;                                                         
	      end if;                                                           
	    end if;                                                             
	  end process;                                                          
	                                                                        
	                                                                        
	--Flag any write response errors                                        
	  write_resp_error <= axi_bready and M_AXI_BVALID and M_AXI_BRESP(1);   


	-- For maximum port throughput, this user example code will try to allow
	-- each channel to run as independently and as quickly as possible.

	-- However, there are times when the flow of data needs to be throtted by
	-- the user application. This example application requires that data is
	-- not read before it is written and that the write channels do not
	-- advance beyond an arbitrary threshold (say to prevent an 
	-- overrun of the current read address by the write address).

	-- From AXI4 Specification, 13.13.1: "If a master requires ordering between 
	-- read and write transactions, it must ensure that a response is received 
	-- for the previous transaction before issuing the next transaction."

	 -- write_burst_counter counter keeps track with the number of burst transaction initiated             
	 -- against the number of burst transactions the master needs to initiate                                    
	  process(M_AXI_ACLK)                                                                                        
	  begin                                                                                                      
	    if (rising_edge (M_AXI_ACLK)) then                                                                       
	      if (s_AXI_reset_n = '0' or init_txn_pulse = '1') then                                                                         
	        --write_burst_counter <= (others => '0');         
	        s_write_burst_done <= '0';                                                     
	      else                                                                                                   
	        if (M_AXI_AWREADY = '1' and axi_awvalid = '1') then                                                  
--	          if (write_burst_counter < s_AXI_WR_no_burst)then     --1x*x bursts                                            
--	            write_burst_counter <= write_burst_counter + 1;                      
--	          end if;    
              s_write_burst_done <= '1';                                                                                          
	        end if;                                                                                              
	      end if;                                                                                                
	    end if;                                                                                                  
	  end process;                                                                                               
	                                                                                                                                                                                                                                                                                                                     
  MASTER_EXECUTION_PROC:process(M_AXI_ACLK)                                                                  
        begin                                                                                                      
          if (rising_edge (M_AXI_ACLK)) then                                                                       
            if (s_AXI_reset_n = '0') then                                                                         
              -- reset condition                                                                                   
              -- All the signals are ed default values under reset condition                                       
              axi_write_state            <= s_IDLE;                                                                                                                                          
              start_single_burst_write  <= '0';  
              s_AXI_WR_busy                 <= '0';                                                                                                                                     
              s_AXI_WR_done                 <= '0'; 
              s_AXI_WR_error                <= '0';
              --i_AXI_WR_data_size            <= (OTHERS => '0');
              s_AXI_WR_start_address        <= (OTHERS => '0');
              s_AXI_WR_burst_length         <= (OTHERS => '0');
              --s_AXI_WR_no_burst             <= (OTHERS => '0');
            else                                                                                                   
              -- state transition                                                                                  
              case (axi_write_state) is                                                                             
                                                                                                                   
                 when s_IDLE =>                                                                              
                   -- This state is responsible to initiate                               
                   -- AXI transaction when init_txn_pulse is asserted 
                     if ( init_txn_pulse = '1') then       
                       axi_write_state  <= s_INIT_WRITE;                                                              
                       s_AXI_WR_done              <= '0'; 
                       s_AXI_WR_busy              <= '1';
                       --i_AXI_WR_data_size         <= AXI_WR_data_size;
                       s_AXI_WR_start_address     <= unsigned(AXI_WR_start_address_i);
                       s_AXI_WR_burst_length      <= unsigned(AXI_WR_burst_length_i(7 downto 0)) - 1;
                       --s_AXI_WR_no_burst          <= unsigned(AXI_WR_data_size_i)/(unsigned(AXI_WR_burst_length_i)*to_unsigned(g_M_AXI_DATA_WIDTH/8,7)); -- The buffer manager only asks for one burst transaction per request, maximum value can be 256burst.
                     else                                                                                          
                       axi_write_state  <= s_IDLE;   
                       s_AXI_WR_busy       <= '0';                                                         
                     end if;                                                                                       
                                                                                                                   
                  when s_INIT_WRITE =>                                                                               
                    -- This state is responsible to issue start_single_write pulse to                              
                    -- initiate a write transaction. Write transactions will be                                    
                    -- issued until burst_write_active signal is asserted.                                         
                    -- write controller                                                                            
                                                                                                                   
                      if (writes_done = '1') then                                                                  
                        axi_write_state <= s_INIT_DONE;                                                               
                      else                                                                                         
                        axi_write_state  <= s_INIT_WRITE;                                                             
                                                                                                                   
                      if (axi_awvalid = '0' and start_single_burst_write = '0' and burst_write_active = '0' ) then 
                        start_single_burst_write <= '1';                                                           
                      else                                                                                         
                        start_single_burst_write <= '0'; --Negate to generate a pulse                              
                      end if;                                                                                      
                    end if;                                                                                                                                                          
                  
                  when s_INIT_DONE =>    
                        axi_write_state <= s_IDLE; 
                        s_AXI_WR_done      <= '1'; 
                        s_AXI_WR_busy      <= '0';
                                                                                                                   
                  when others  =>                                                                                  
                    axi_write_state  <= s_IDLE;                                                               
                end case  ;                                                                                        
             end if;                                                                                               
          end if;                                                                                                  
        end process;     	                                                                                                             
                                                                                                                                                                                                                                                                                                       
	  -- burst_write_active signal is asserted when there is a burst write transaction                           
	  -- is initiated by the assertion of start_single_burst_write. burst_write_active                           
	  -- signal remains asserted until the burst write is accepted by the slave                                  
	  process(M_AXI_ACLK)                                                                                        
	  begin                                                                                                      
	    if (rising_edge (M_AXI_ACLK)) then                                                                       
	      if (s_AXI_reset_n = '0' or init_txn_pulse = '1') then                                                                         
	        burst_write_active <= '0';                                                                           
	                                                                                                             
	       --The burst_write_active is asserted when a write burst transaction is initiated                      
	      else                                                                                                   
	        if (start_single_burst_write = '1') then                                                             
	          burst_write_active <= '1';                                                                         
	        elsif (M_AXI_BVALID = '1' and axi_bready = '1') then                                                 
	          burst_write_active <= '0';                                                                         
	        end if;                                                                                              
	      end if;                                                                                                
	    end if;                                                                                                  
	  end process;                                                                                               
	                                                                                                             
	 -- Check for last write completion.                                                                         
	                                                                                                             
	 -- This logic is to qualify the last write count with the final write                                       
	 -- response. This demonstrates how to confirm that a write has been                                         
	 -- committed.                                                                                               
	                                                                                                             
	  process(M_AXI_ACLK)                                                                                        
	  begin                                                                                                      
	    if (rising_edge (M_AXI_ACLK)) then                                                                       
	      if (s_AXI_reset_n = '0' or init_txn_pulse = '1') then                                                                         
	       writes_done <= '0';                                                                                                                       
	      --elsif (M_AXI_RVALID && axi_rready && (read_burst_counter == {(C_NO_BURSTS_REQ-1){1}}) && axi_rlast)  
	      else                                                                                                   
	        --if (M_AXI_BVALID = '1' and (write_burst_counter = s_AXI_WR_no_burst) and axi_bready = '1') then   -- total bursts
	        if (M_AXI_BVALID = '1' and (s_write_burst_done = '1') and axi_bready = '1') then   -- total bursts
	          writes_done <= '1';                                                                                
	        end if;                                                                                              
	      end if;                                                                                                
	    end if;                                                                                                  
	  end process;                                                                                               

end implementation;
