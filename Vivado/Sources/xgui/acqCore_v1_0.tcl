
# Loading additional proc with user specified bodies to compute parameter values.
source [file join [file dirname [file dirname [info script]]] gui/acqCore_v1_0.gtcl]

# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  set g_CHANNELS [ipgui::add_param $IPINST -name "g_CHANNELS" -parent ${Page_0}]
  set_property tooltip {number of channerls} ${g_CHANNELS}
  set g_BUFFERS [ipgui::add_param $IPINST -name "g_BUFFERS" -parent ${Page_0}]
  set_property tooltip {buffers} ${g_BUFFERS}
  set g_BUFFER_SIZE [ipgui::add_param $IPINST -name "g_BUFFER_SIZE" -parent ${Page_0}]
  set_property tooltip {Size of the buffer for each channel [number of samples]} ${g_BUFFER_SIZE}
  set g_DATA_IN_WIDTH [ipgui::add_param $IPINST -name "g_DATA_IN_WIDTH" -parent ${Page_0} -widget comboBox]
  set_property tooltip {channel interface width in bits} ${g_DATA_IN_WIDTH}
  set g_FIFO_BURST [ipgui::add_param $IPINST -name "g_FIFO_BURST" -parent ${Page_0} -widget comboBox]
  set_property tooltip {Maximum number of burst when writting acquisition data to memory} ${g_FIFO_BURST}
  set g_M_AXI_ADDR_WIDTH [ipgui::add_param $IPINST -name "g_M_AXI_ADDR_WIDTH" -parent ${Page_0}]
  set_property tooltip {AXI master interface address width} ${g_M_AXI_ADDR_WIDTH}
  set g_M_AXI_DATA_WIDTH [ipgui::add_param $IPINST -name "g_M_AXI_DATA_WIDTH" -parent ${Page_0} -widget comboBox]
  set_property tooltip {AXI master interface data width} ${g_M_AXI_DATA_WIDTH}
  set g_MODE [ipgui::add_param $IPINST -name "g_MODE" -parent ${Page_0} -widget comboBox]
  set_property tooltip {Mode} ${g_MODE}
  ipgui::add_param $IPINST -name "g_BASE_ADDR_0" -parent ${Page_0}
  ipgui::add_param $IPINST -name "g_BASE_ADDR_1" -parent ${Page_0}
  ipgui::add_param $IPINST -name "g_DSP_BLOCK" -parent ${Page_0}


}

proc update_PARAM_VALUE.g_BASE_ADDR_0 { PARAM_VALUE.g_BASE_ADDR_0 PARAM_VALUE.g_MODE } {
	# Procedure called to update g_BASE_ADDR_0 when any of the dependent parameters in the arguments change
	
	set g_BASE_ADDR_0 ${PARAM_VALUE.g_BASE_ADDR_0}
	set g_MODE ${PARAM_VALUE.g_MODE}
	set values(g_MODE) [get_property value $g_MODE]
	if { [gen_USERPARAMETER_g_BASE_ADDR_0_ENABLEMENT $values(g_MODE)] } {
		set_property enabled true $g_BASE_ADDR_0
	} else {
		set_property enabled false $g_BASE_ADDR_0
	}
}

proc validate_PARAM_VALUE.g_BASE_ADDR_0 { PARAM_VALUE.g_BASE_ADDR_0 } {
	# Procedure called to validate g_BASE_ADDR_0
	return true
}

proc update_PARAM_VALUE.g_BASE_ADDR_1 { PARAM_VALUE.g_BASE_ADDR_1 PARAM_VALUE.g_MODE } {
	# Procedure called to update g_BASE_ADDR_1 when any of the dependent parameters in the arguments change
	
	set g_BASE_ADDR_1 ${PARAM_VALUE.g_BASE_ADDR_1}
	set g_MODE ${PARAM_VALUE.g_MODE}
	set values(g_MODE) [get_property value $g_MODE]
	if { [gen_USERPARAMETER_g_BASE_ADDR_1_ENABLEMENT $values(g_MODE)] } {
		set_property enabled true $g_BASE_ADDR_1
	} else {
		set_property enabled false $g_BASE_ADDR_1
	}
}

proc validate_PARAM_VALUE.g_BASE_ADDR_1 { PARAM_VALUE.g_BASE_ADDR_1 } {
	# Procedure called to validate g_BASE_ADDR_1
	return true
}

proc update_PARAM_VALUE.g_BUFFERS { PARAM_VALUE.g_BUFFERS PARAM_VALUE.g_MODE } {
	# Procedure called to update g_BUFFERS when any of the dependent parameters in the arguments change
	
	set g_BUFFERS ${PARAM_VALUE.g_BUFFERS}
	set g_MODE ${PARAM_VALUE.g_MODE}
	set values(g_MODE) [get_property value $g_MODE]
	if { [gen_USERPARAMETER_g_BUFFERS_ENABLEMENT $values(g_MODE)] } {
		set_property enabled true $g_BUFFERS
	} else {
		set_property enabled false $g_BUFFERS
	}
}

proc validate_PARAM_VALUE.g_BUFFERS { PARAM_VALUE.g_BUFFERS } {
	# Procedure called to validate g_BUFFERS
	return true
}

proc update_PARAM_VALUE.g_CHANNELS { PARAM_VALUE.g_CHANNELS PARAM_VALUE.g_MODE } {
	# Procedure called to update g_CHANNELS when any of the dependent parameters in the arguments change
	
	set g_CHANNELS ${PARAM_VALUE.g_CHANNELS}
	set g_MODE ${PARAM_VALUE.g_MODE}
	set values(g_MODE) [get_property value $g_MODE]
	if { [gen_USERPARAMETER_g_CHANNELS_ENABLEMENT $values(g_MODE)] } {
		set_property enabled true $g_CHANNELS
	} else {
		set_property enabled false $g_CHANNELS
	}
}

proc validate_PARAM_VALUE.g_CHANNELS { PARAM_VALUE.g_CHANNELS } {
	# Procedure called to validate g_CHANNELS
	return true
}

proc update_PARAM_VALUE.g_FIFO_BURST { PARAM_VALUE.g_FIFO_BURST PARAM_VALUE.g_MODE } {
	# Procedure called to update g_FIFO_BURST when any of the dependent parameters in the arguments change
	
	set g_FIFO_BURST ${PARAM_VALUE.g_FIFO_BURST}
	set g_MODE ${PARAM_VALUE.g_MODE}
	set values(g_MODE) [get_property value $g_MODE]
	if { [gen_USERPARAMETER_g_FIFO_BURST_ENABLEMENT $values(g_MODE)] } {
		set_property enabled true $g_FIFO_BURST
	} else {
		set_property enabled false $g_FIFO_BURST
	}
}

proc validate_PARAM_VALUE.g_FIFO_BURST { PARAM_VALUE.g_FIFO_BURST } {
	# Procedure called to validate g_FIFO_BURST
	return true
}

proc update_PARAM_VALUE.g_M_AXI_ADDR_WIDTH { PARAM_VALUE.g_M_AXI_ADDR_WIDTH PARAM_VALUE.g_MODE } {
	# Procedure called to update g_M_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
	
	set g_M_AXI_ADDR_WIDTH ${PARAM_VALUE.g_M_AXI_ADDR_WIDTH}
	set g_MODE ${PARAM_VALUE.g_MODE}
	set values(g_MODE) [get_property value $g_MODE]
	if { [gen_USERPARAMETER_g_M_AXI_ADDR_WIDTH_ENABLEMENT $values(g_MODE)] } {
		set_property enabled true $g_M_AXI_ADDR_WIDTH
	} else {
		set_property enabled false $g_M_AXI_ADDR_WIDTH
	}
}

proc validate_PARAM_VALUE.g_M_AXI_ADDR_WIDTH { PARAM_VALUE.g_M_AXI_ADDR_WIDTH } {
	# Procedure called to validate g_M_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.g_M_AXI_DATA_WIDTH { PARAM_VALUE.g_M_AXI_DATA_WIDTH PARAM_VALUE.g_MODE } {
	# Procedure called to update g_M_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
	
	set g_M_AXI_DATA_WIDTH ${PARAM_VALUE.g_M_AXI_DATA_WIDTH}
	set g_MODE ${PARAM_VALUE.g_MODE}
	set values(g_MODE) [get_property value $g_MODE]
	if { [gen_USERPARAMETER_g_M_AXI_DATA_WIDTH_ENABLEMENT $values(g_MODE)] } {
		set_property enabled true $g_M_AXI_DATA_WIDTH
	} else {
		set_property enabled false $g_M_AXI_DATA_WIDTH
	}
}

proc validate_PARAM_VALUE.g_M_AXI_DATA_WIDTH { PARAM_VALUE.g_M_AXI_DATA_WIDTH } {
	# Procedure called to validate g_M_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.g_BUFFER_SIZE { PARAM_VALUE.g_BUFFER_SIZE } {
	# Procedure called to update g_BUFFER_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_BUFFER_SIZE { PARAM_VALUE.g_BUFFER_SIZE } {
	# Procedure called to validate g_BUFFER_SIZE
	return true
}

proc update_PARAM_VALUE.g_DATA_IN_WIDTH { PARAM_VALUE.g_DATA_IN_WIDTH } {
	# Procedure called to update g_DATA_IN_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_DATA_IN_WIDTH { PARAM_VALUE.g_DATA_IN_WIDTH } {
	# Procedure called to validate g_DATA_IN_WIDTH
	return true
}

proc update_PARAM_VALUE.g_DSP_BLOCK { PARAM_VALUE.g_DSP_BLOCK } {
	# Procedure called to update g_DSP_BLOCK when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_DSP_BLOCK { PARAM_VALUE.g_DSP_BLOCK } {
	# Procedure called to validate g_DSP_BLOCK
	return true
}

proc update_PARAM_VALUE.g_MODE { PARAM_VALUE.g_MODE } {
	# Procedure called to update g_MODE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.g_MODE { PARAM_VALUE.g_MODE } {
	# Procedure called to validate g_MODE
	return true
}


proc update_MODELPARAM_VALUE.g_CHANNELS { MODELPARAM_VALUE.g_CHANNELS PARAM_VALUE.g_CHANNELS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_CHANNELS}] ${MODELPARAM_VALUE.g_CHANNELS}
}

proc update_MODELPARAM_VALUE.g_DATA_IN_WIDTH { MODELPARAM_VALUE.g_DATA_IN_WIDTH PARAM_VALUE.g_DATA_IN_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_DATA_IN_WIDTH}] ${MODELPARAM_VALUE.g_DATA_IN_WIDTH}
}

proc update_MODELPARAM_VALUE.g_M_AXI_DATA_WIDTH { MODELPARAM_VALUE.g_M_AXI_DATA_WIDTH PARAM_VALUE.g_M_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_M_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.g_M_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.g_M_AXI_ADDR_WIDTH { MODELPARAM_VALUE.g_M_AXI_ADDR_WIDTH PARAM_VALUE.g_M_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_M_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.g_M_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.g_FIFO_BURST { MODELPARAM_VALUE.g_FIFO_BURST PARAM_VALUE.g_FIFO_BURST } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_FIFO_BURST}] ${MODELPARAM_VALUE.g_FIFO_BURST}
}

proc update_MODELPARAM_VALUE.g_BUFFER_SIZE { MODELPARAM_VALUE.g_BUFFER_SIZE PARAM_VALUE.g_BUFFER_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_BUFFER_SIZE}] ${MODELPARAM_VALUE.g_BUFFER_SIZE}
}

proc update_MODELPARAM_VALUE.g_MODE { MODELPARAM_VALUE.g_MODE PARAM_VALUE.g_MODE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_MODE}] ${MODELPARAM_VALUE.g_MODE}
}

proc update_MODELPARAM_VALUE.g_BUFFERS { MODELPARAM_VALUE.g_BUFFERS PARAM_VALUE.g_BUFFERS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_BUFFERS}] ${MODELPARAM_VALUE.g_BUFFERS}
}

proc update_MODELPARAM_VALUE.g_BASE_ADDR_0 { MODELPARAM_VALUE.g_BASE_ADDR_0 PARAM_VALUE.g_BASE_ADDR_0 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_BASE_ADDR_0}] ${MODELPARAM_VALUE.g_BASE_ADDR_0}
}

proc update_MODELPARAM_VALUE.g_BASE_ADDR_1 { MODELPARAM_VALUE.g_BASE_ADDR_1 PARAM_VALUE.g_BASE_ADDR_1 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_BASE_ADDR_1}] ${MODELPARAM_VALUE.g_BASE_ADDR_1}
}

proc update_MODELPARAM_VALUE.g_DSP_BLOCK { MODELPARAM_VALUE.g_DSP_BLOCK PARAM_VALUE.g_DSP_BLOCK } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.g_DSP_BLOCK}] ${MODELPARAM_VALUE.g_DSP_BLOCK}
}

