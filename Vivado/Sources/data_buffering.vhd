-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, Data buffering --
-- --
-------------------------------------------------------------------------------
--
-- unit name: Data buffering
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 08/05/2018
--
-- version: 1.0
--
-- description: buffering of the acquistion data
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- acquisition core package
library work;
use work.ACQ_CORE_PACK.all;

entity data_buffering is
    generic(
                g_DATA_IN_WIDTH     : integer := 32;
                g_DATA_OUT_WIDTH    : integer := 512;
                g_FIFO_BURST        : integer := 64;
                g_DSP_BLOCK         : boolean := TRUE
        );
    port(
            -- GENERAL RESET
            reset_i             : in  std_logic;
            
            -- ACQUISTION PARAMETERS 
            acq_start_trig_i    : in  std_logic;
            acq_length_i        : in  std_logic_vector(31 downto 0);
            acq_decimation_i    : in  std_logic_vector(31 downto 0);
            acq_finished_o      : out std_logic;
            acq_debug_mode_i    : in  std_logic;
            
            -- DATA INPUT INTERFACE
            ch_fifo_din_i       : in  std_logic_vector (g_DATA_IN_WIDTH-1 downto 0);
            ch_fifo_wr_clk_i    : in  std_logic;
            
            -- DATA OUTPUT INTERFACE
            ch_fifo_rd_clk_i        : in    std_logic;
            ch_fifo_rd_en_i         : in    std_logic;
            ch_fifo_dout_o          : out   std_logic_vector(g_DATA_OUT_WIDTH-1 downto 0);
            ch_fifo_full_o          : out   std_logic;
            ch_fifo_empty_o         : out   std_logic_vector(((g_DATA_OUT_WIDTH/g_DATA_IN_WIDTH)/4)-1 downto 0);
            ch_fifo_prog_empty_o    : out   std_logic_vector(((g_DATA_OUT_WIDTH/g_DATA_IN_WIDTH)/4)-1 downto 0);
            ch_fifo_data_ready_o    : out   std_logic
         );
end data_buffering;

architecture Behavioral of data_buffering is

    ----------------------------------------------------------------------
    -- constant instantation 
    ----------------------------------------------------------------------
    constant c_DATA_WIDTH_RATIO     : positive := g_DATA_OUT_WIDTH/g_DATA_IN_WIDTH;
    constant c_FIFO_NUMBER          : positive := c_DATA_WIDTH_RATIO/4;
    constant c_DATA_WIDTH_IN_BYTES  : positive := g_DATA_IN_WIDTH/8;

    ----------------------------------------------------------------------
    -- component instantation 
    -----------------------------------------------------------------------
    component DSP_block is
        port(
                data_in_i   : in  std_logic_vector(31 downto 0);
                data_out_o  : out std_logic_vector(31 downto 0)
        );
    end component;
    -----------------------------------------------------------------------
    component fifo_generator_0
      generic(
              g_PROG_EMPTY_THRESH_NEGATE_VAL : integer := 256
      ); 
      port (
              srst          : in std_logic;
              wr_clk        : in std_logic;
              rd_clk        : in std_logic;
              din           : in std_logic_vector(31 downto 0);
              wr_en         : in std_logic;
              rd_en         : in std_logic;
              dout          : out std_logic_vector(127 downto 0);
              full          : out std_logic;
              empty         : out std_logic;
              prog_empty    : out std_logic;
              wr_rst_busy   : out std_logic;
              rd_rst_busy   : out std_logic
            );
    end component;
    ----------------------------------------------------------------------
    -- data reordering
    -- component instantation 
    component data_reordering is
        generic (
                    g_BUS_WIDTH    : positive := 512;
                    g_WORD_WIDTH   : positive := 32
                );
        port    ( 
                    data_in_i       : in  std_logic_vector(g_BUS_WIDTH-1 downto 0);
                    data_out_o      : out std_logic_vector(g_BUS_WIDTH-1 downto 0)
                ); 
    end component data_reordering;
    
    ----------------------------------------------------------------------
    -- type declaration
    ----------------------------------------------------------------------
    type t_acquistion  is (IDLE,ACQUISTION);
    ----------------------------------------------------------------------
    -- signal instantation 
    signal s_data_in            : std_logic_vector(ch_fifo_din_i'range);
    signal s_data_counter       : unsigned(acq_length_i'range);
    signal s_acq_data           : std_logic_vector(g_DATA_IN_WIDTH-1 downto 0);
    signal s_fifo_wr_en         : std_logic_vector(c_FIFO_NUMBER-1 downto 0);
    signal s_fifo_dout          : std_logic_vector(g_DATA_OUT_WIDTH-1 downto 0);
    signal s_ch_fifo_dout_o     : std_logic_vector(g_DATA_OUT_WIDTH-1 downto 0);
    signal s_fifo_full          : std_logic_vector(c_FIFO_NUMBER-1 downto 0);
    signal s_acquisition        : std_logic;
    signal s_main_state         : t_acquistion;
    signal s_acq_start_trig_1   : std_logic;
    signal s_dec_clk_en         : std_logic;
    signal s_dec_clk_cnt        : unsigned(acq_decimation_i'range);
begin
    ----------------------------------------------------------------------
    -- PORT MAP
    fifo_array: 
    for I in 0 to (c_FIFO_NUMBER-1) generate
        fifo:fifo_generator_0
        generic map
                (
                    g_PROG_EMPTY_THRESH_NEGATE_VAL => g_FIFO_BURST*c_FIFO_NUMBER
                )
        port map( 
                    wr_clk => ch_fifo_wr_clk_i,
                    rd_clk => ch_fifo_rd_clk_i,
                    srst => reset_i,
                    din => s_acq_data,
                    wr_en => s_fifo_wr_en(I),
                    rd_en => ch_fifo_rd_en_i,
                    dout(127 downto 0) => s_fifo_dout((I*128)+127 downto I*128),
                    full => s_fifo_full(I),
                    empty => ch_fifo_empty_o(I),
                    prog_empty => ch_fifo_prog_empty_o(I),
                    wr_rst_busy => open,
                    rd_rst_busy => open
                );
    end generate fifo_array; 
    ----------------------------------------------------------------------
    data_reordering_0: data_reordering
        generic map(
                    g_BUS_WIDTH => g_DATA_OUT_WIDTH,
                    g_WORD_WIDTH => g_DATA_IN_WIDTH
                )
        port map   ( 
                    data_in_i => s_fifo_dout,
                    data_out_o => s_ch_fifo_dout_o
                ); 
    ----------------------------------------------------------------------
    -- DSP block instantation
    ----------------------------------------------------------------------
    DSP_BLOCK_TRUE: 
    if g_DSP_BLOCK = TRUE generate
        DSP_block_0: component DSP_block
        port map(
                data_in_i => ch_fifo_din_i,
                data_out_o => s_data_in
        );
    end generate;
        
    DSP_BLOCK_FALSE:
    if g_DSP_BLOCK = FALSE generate   
        s_data_in <= ch_fifo_din_i;
    end generate;
    ----------------------------------------------------------------------
    -- Asychronous
    ch_fifo_dout_o          <= s_ch_fifo_dout_o;
    s_fifo_wr_en(3)         <= (not s_data_counter(3) and not s_data_counter(2))and s_dec_clk_en;
    s_fifo_wr_en(2)         <=  not s_data_counter(3) and     s_data_counter(2) and s_dec_clk_en;
    s_fifo_wr_en(1)         <=      s_data_counter(3) and not s_data_counter(2) and s_dec_clk_en;
    s_fifo_wr_en(0)         <=      s_data_counter(3) and     s_data_counter(2) and s_dec_clk_en;
    ch_fifo_data_ready_o    <= '1' when s_data_counter = c_DATA_WIDTH_RATIO else '0';
    ch_fifo_full_o          <= '1' when unsigned(s_fifo_full) /= 0 else '0';

    -- if debug mode is enabled, the debug pattern is acquired (pattern format => simple counter)
    -- TODO: should be synchronous?
    s_acq_data              <= s_data_in when acq_debug_mode_i = '0' else std_logic_vector(s_data_counter);
    
    --=============================================================================
    -- Begin of the process "decimation"
    -- Generate down sampling clock for the acquisition
    --=============================================================================                 
    p_decimation: process (ch_fifo_wr_clk_i,reset_i) begin
        if(reset_i = '1')then
            s_dec_clk_en    <= '0';
            s_dec_clk_cnt   <= (others => '0');
        elsif rising_edge(ch_fifo_wr_clk_i) then
            -- default
            s_dec_clk_en    <= '0';
            s_dec_clk_cnt   <= (others => '0');
            
            -- Only when acquisition was started
            if(s_acquisition = '1')then
                s_dec_clk_cnt   <= s_dec_clk_cnt + 1;
                            
                if(s_dec_clk_cnt >= unsigned(acq_decimation_i))then
                    s_dec_clk_en    <= '1';
                    s_dec_clk_cnt   <= (others => '0');
                end if;
            end if;
        end if;
    end process p_decimation;    

    --=============================================================================
    -- Begin of the process "ACQUISTION"
    -- acquistion manager process
    -- start acquistion on start trigger, store a number of samples defined by the length 
    --=============================================================================
    p_acquisition: process (ch_fifo_wr_clk_i,reset_i) 
    begin    
        if(reset_i = '1')then
            s_main_state        <= IDLE;
            s_data_counter      <= (others => '0');
            acq_finished_o      <= '0';
            s_acquisition       <= '0';
            s_acq_start_trig_1  <= '0';
        elsif rising_edge(ch_fifo_wr_clk_i) then  
        
            -- ADD RESTART MECHANISM HERE!!!!!!!!!!!!!!!!!!!!!!
            --default
            s_acq_start_trig_1 <= acq_start_trig_i;
        
            -- main state machine
            case s_main_state is
                when IDLE =>
                    -- waiting start trigger (on rising edge)
                    if(acq_start_trig_i = '1' and s_acq_start_trig_1 = '0')then
                       s_main_state     <= ACQUISTION;  
                       s_acquisition    <= '1';
                    end if;
                    
                    s_data_counter  <= (others => '0');
                when ACQUISTION =>
                    -- perform acquisition with decimation 
                    if(s_dec_clk_en = '1')then
                        --default 
                        acq_finished_o <= '0';
                        -- count number of data
                        s_data_counter <= s_data_counter + 1;
                        
                        -- end of acquistion
                        if(unsigned(acq_length_i)-1 = s_data_counter)then
                            s_main_state    <= IDLE;
                            acq_finished_o  <= '1';
                            s_data_counter  <= (others => '0');
                            s_acquisition   <= '0';
                        end if;
                    end if;
                when others => s_main_state  <= IDLE;
            end case;
        end if;
    end process;

end Behavioral;
