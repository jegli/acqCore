-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, Data Acquistion Core Package --
-- --
-------------------------------------------------------------------------------
--
-- unit name: Data Acquistion Core Package
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 08/05/2018
--
-- version: 1.0
--
-- description: Acquistion Core Package
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

package ACQ_CORE_PACK is
    ------------------------------------------------------------------------------------
    -- type declarations 
    ------------------------------------------------------------------------------------ 
    type t_AXI_WR_command is                                                            -- AXI write command
    record 
        start              : std_logic;
        burst_length       : unsigned (8 downto 0);
        address            : unsigned (31 downto 0);
        data_size          : unsigned (31 downto 0); 
    end record;
    constant c_AXI_WR_command_init : t_AXI_WR_command := (start => '0',burst_length => (others => '0'),address => (others => '0'),data_size => (others => '0'));
    ------------------------------------------------------------------------------------ 
    type t_fifo_rdport is                                                               -- fifo port
    record 
        rd_data       : std_logic_vector(511 downto 0);
        rd_empty      : std_logic_vector(3 downto 0); 
        rd_prog_empty : std_logic_vector(3 downto 0); 
    end record;
    constant c_fifo_rdport_init : t_fifo_rdport := (rd_data => (others => '0'),rd_empty => (others => '0'),rd_prog_empty => (others => '0'));
    ------------------------------------------------------------------------------------ 
    type t_STD_ARRAY_16 is array (integer range <>) of std_logic_vector(15 downto 0);           -- 16-bit std_logic_vector array type declaration
    type t_STD_ARRAY_32 is array (integer range <>) of std_logic_vector(31 downto 0);           -- 32-bit std_logic_vector array type declaration
    
    type t_UNSIGN_ARRAY_16 is array (integer range <>) of unsigned(15 downto 0);                -- 16-bit unsigned array type declaration
    type t_UNSIGN_ARRAY_32 is array (integer range <>) of unsigned(31 downto 0);                -- 32-bit unsigned array type declaration
    ------------------------------------------------------------------------------------ 
    -- buffer type declaration
    type t_buffer is
    record
        index       : unsigned(31 downto 0);
    end record;
       
    type    t_buffer_array is array (integer range <>) of t_buffer;
    type    t_fifo_rdport_array is array (integer range <>) of t_fifo_rdport;
    type    t_channel_array is array (integer range <>) of std_logic_vector(31 downto 0);  -- fifo read interface for x channels
    subtype t_buff_array is t_channel_array;
    ------------------------------------------------------------------------------------
    -- functions declarations 
    ------------------------------------------------------------------------------------ 
    function f_find_width (x : positive) return natural;                                -- return the number of bit to express x in binary
--    function f_2_power_y(x:positive) return natural;                                    -- calculate y of equation: x = 2^y
    
    ------------------------------------------------------------------------------------
    -- constant declarations 
    ------------------------------------------------------------------------------------ 
    constant c_MAX_CHANNELS : positive := 12;                                           -- maximum allowed channels 
    constant c_MAX_BUFFERS  : positive := 32;                                           -- maximum allowed buffers 
    constant c_REG_WIDTH    : positive := 32;                                           -- 32-bit register
    
    --IP versioning
    constant c_IDENT                : std_logic_vector(63 downto 0) := X"0000000000005555"; --temporary
    constant c_FIRMWARE_VERSION     : std_logic_vector(31 downto 0) := X"20180411";         --temporary
    constant c_MEM_MAP_VESRION      : std_logic_vector(31 downto 0) := X"00000001";         --temporary
    
end ACQ_CORE_PACK;

-- function declaration
package body ACQ_CORE_PACK is
    ---------------------------------------------------------
    function f_find_width (x : positive) return natural is 
        variable i : natural;
    begin
        if(x = 1)then
            return 1;
        else
            i := 0;  
            while (2**i < x) and i < 31 loop
                i := i + 1;
            end loop;
            return i;
        end if;
    end function;
    ---------------------------------------------------------
--    function f_2_power_y (x : positive) return natural is 
--        constant c_width  : integer := 32;
--        variable v_vector : std_logic_vector(c_width-1 downto 0);
--    begin
--        v_vector := std_logic_vector(to_unsigned(x,c_width));
--        for B in 0 to c_width-1 loop
--            if(v_vector(B) = '1')then
--                return B;
--            end if;
--        end loop;
        
--        return 0;
--    end function;
    
end ACQ_CORE_PACK;