-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, acq_memory_ctrl --
-- --
-------------------------------------------------------------------------------
--
-- unit name: Acquisition memory controller
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 14/03/2019
--
-- version: 1.0
--
-- description: control the built-in ram memory access through the AXI-full slave interface
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- acquisition core package
library work;
use work.ACQ_CORE_PACK.all;

entity acq_memory_ctrl is
    Generic (
                -- Width of S_AXI data bus
                g_S_AXI_DATA_WIDTH      : integer  := 32;
                -- Width of S_AXI address bus
                g_S_AXI_ADDR_WIDTH      : integer  := 32;
                -- data input width
                g_DATA_IN_WIDTH         : integer := 32;
                -- Number of buffers
                g_BUFFERS               : integer := 16;
                -- Size of the buffer
                g_BUFFER_SIZE           : integer  := 8192

            );

    Port (
                -- Global Clock Signal
                S_AXI_aclk          : in std_logic;
                -- Global Reset Signal. This Signal is Active LOW
                S_AXI_aresetn       : in std_logic;
                
                -- AXI4-Lite Interface
                -- Read address (issued by master, acceped by Slave)
                S_AXI_araddr        : in std_logic_vector(g_S_AXI_ADDR_WIDTH-1 downto 0);
                -- Protection type. This signal indicates the privilege
                -- and security level of the transaction, and whether the
                -- transaction is a data access or an instruction access.
                S_AXI_arprot        : in std_logic_vector(2 downto 0);
                -- Read address valid. This signal indicates that the channel
                -- is signaling valid read address and control information.
                S_AXI_arvalid       : in std_logic;
                -- Read address ready. This signal indicates that the slave is
                -- ready to accept an address and associated control signals.
                S_AXI_arready       : out std_logic;
                -- Read data (issued by slave)
                S_AXI_rdata         : out std_logic_vector(g_S_AXI_DATA_WIDTH-1 downto 0);
                -- Read response. This signal indicates the status of the
                -- read transfer.
                S_AXI_rresp         : out std_logic_vector(1 downto 0);
                -- Read valid. This signal indicates that the channel is
                    -- signaling the required read data.
                S_AXI_rvalid        : out std_logic;
                -- Read ready. This signal indicates that the master can
                -- accept the read data and response information.
                S_AXI_rready        : in std_logic;
        
                -- data output of BRAMs
                BRAM_clk_i      : in std_logic; 
                BRAM_en_i       : in std_logic;
                BRAM_we_i       : in std_logic;
                BRAM_addr_i     : in std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
                BRAM_din_i      : in t_buff_array(0 to g_BUFFERS-1)      
         );

end acq_memory_ctrl;

architecture Behavioral of acq_memory_ctrl is

    -- signal instantation
    signal s_BRAM_dout_mux  : t_STD_ARRAY_32(0 to g_BUFFERS-1);
    signal s_BRAM_dout      : std_logic_vector(31 downto 0);
    signal s_BRAM_rd_en     : std_logic;
    signal s_BRAM_rd_addr   : std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
    signal s_BRAM_rd_select : std_logic_vector(f_find_width(g_BUFFERS)-1 downto 0);
    ----------------------------------------------------------
    component SDP_RAM_mem is
        generic(
                    g_RAM_DEPTH     : integer := 8192;
                    g_DWIDTH        : integer := 32
        );
        port(
            clka_i  : in  std_logic;
            clkb_i  : in  std_logic;
            ena_i   : in  std_logic;
            enb_i   : in  std_logic;
            wea_i   : in  std_logic;
            addra_i : in  std_logic_vector(f_find_width(g_RAM_DEPTH)-1 downto 0);
            addrb_i : in  std_logic_vector(f_find_width(g_RAM_DEPTH)-1 downto 0);
            dina_i  : in  std_logic_vector(g_DWIDTH-1 downto 0);
            doutb_o : out std_logic_vector(g_DWIDTH-1 downto 0)
        );
    end component;
    ----------------------------------------------------------
    component AXI_lite_interface is
        generic (
    
            -- Width of S_AXI data bus
            g_S_AXI_DATA_WIDTH	: integer  := 32;
            -- Width of S_AXI address bus
            g_S_AXI_ADDR_WIDTH	: integer  := 32;
            -- Number of buffers
            g_BUFFERS           : integer := 16;
            -- Size of the buffer
            g_BUFFER_SIZE       : integer  := 8192
        );
        port (
    
            -- Global Clock Signal
            S_AXI_aclk	    : in std_logic;
            -- Global Reset Signal. This Signal is Active LOW
            S_AXI_aresetn	: in std_logic;
            
            -- AXI4-Lite Interface
            
            -- Read address (issued by master, acceped by Slave)
            S_AXI_araddr	: in std_logic_vector(g_S_AXI_ADDR_WIDTH-1 downto 0);
            -- Protection type. This signal indicates the privilege
            -- and security level of the transaction, and whether the
            -- transaction is a data access or an instruction access.
            S_AXI_arprot	: in std_logic_vector(2 downto 0);
            -- Read address valid. This signal indicates that the channel
            -- is signaling valid read address and control information.
            S_AXI_arvalid	: in std_logic;
            -- Read address ready. This signal indicates that the slave is
            -- ready to accept an address and associated control signals.
            S_AXI_arready	: out std_logic;
            -- Read data (issued by slave)
            S_AXI_rdata	    : out std_logic_vector(g_S_AXI_DATA_WIDTH-1 downto 0);
            -- Read response. This signal indicates the status of the
            -- read transfer.
            S_AXI_rresp	    : out std_logic_vector(1 downto 0);
            -- Read valid. This signal indicates that the channel is
                -- signaling the required read data.
            S_AXI_rvalid	: out std_logic;
            -- Read ready. This signal indicates that the master can
            -- accept the read data and response information.
            S_AXI_rready	: in std_logic;
            
            -- BRAM interface
            BRAM_rd_en_o      : out std_logic;
            BRAM_rd_addr_o    : out std_logic_vector(f_find_width(g_BUFFER_SIZE)-1 downto 0);
            BRAM_rd_select_o  : out std_logic_vector(f_find_width(g_BUFFERS)-1 downto 0);
            BRAM_data_i       : in  std_logic_vector(31 downto 0)
        );
    end component;
    ----------------------------------------------------------
begin
    cell_array:for BRAMIndex in 0 to g_BUFFERS-1 generate 
    begin
        BRAM_0:SDP_RAM_mem
        generic map(
            g_RAM_DEPTH => g_BUFFER_SIZE,
            g_DWIDTH => g_DATA_IN_WIDTH
        )
        
        port map (
            clka_i   => BRAM_clk_i,
            clkb_i   => S_AXI_aclk,
            ena_i    => BRAM_en_i,
            enb_i    => s_BRAM_rd_en,
            wea_i    => BRAM_we_i,
            addra_i  => BRAM_addr_i,
            addrb_i  => s_BRAM_rd_addr,
            dina_i   => BRAM_din_i(BRAMIndex),
            doutb_o  => s_BRAM_dout_mux(BRAMIndex)
        );
    end generate cell_array;
    ----------------------------------------------------------
    AXI_lite_interface_0: AXI_lite_interface
        generic map(
            g_S_AXI_DATA_WIDTH => g_S_AXI_DATA_WIDTH,
            g_S_AXI_ADDR_WIDTH => g_S_AXI_ADDR_WIDTH,
            g_BUFFERS => g_BUFFERS,
            g_BUFFER_SIZE => g_BUFFER_SIZE
        )
        port map(
            S_AXI_aclk  => S_AXI_aclk,
            S_AXI_aresetn => S_AXI_aresetn, 
            S_AXI_araddr => S_AXI_araddr,
            S_AXI_arprot => S_AXI_arprot,
            S_AXI_arvalid => S_AXI_arvalid,
            S_AXI_arready => S_AXI_arready,
            S_AXI_rdata => S_AXI_rdata,
            S_AXI_rresp => S_AXI_rresp,
            S_AXI_rvalid => S_AXI_rvalid,
            S_AXI_rready => S_AXI_rready,
            
            BRAM_rd_en_o => s_BRAM_rd_en,
            BRAM_rd_addr_o => s_BRAM_rd_addr,
            BRAM_rd_select_o => s_BRAM_rd_select,
            BRAM_data_i => s_BRAM_dout
        );
    
    ----------------------------------------------------------
    p_data_mux: process(s_BRAM_dout_mux,s_BRAM_rd_select)
    begin
        s_BRAM_dout <= s_BRAM_dout_mux(to_integer(unsigned(s_BRAM_rd_select)));
    end process;

end Behavioral;
