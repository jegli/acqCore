-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, Data reordering --
-- --
-------------------------------------------------------------------------------
--
-- unit name: Data reordering
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 15/10/2018
--
-- version: 1.0
--
-- description: reorder data 
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: 
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- acquisition core package
library work;
use work.ACQ_CORE_PACK.all;

entity data_reordering is
    generic (
                g_BUS_WIDTH    : positive := 512;
                g_WORD_WIDTH   : positive := 32
            );
    port    ( 
                data_in_i       : in  std_logic_vector(g_BUS_WIDTH-1 downto 0);
                data_out_o      : out std_logic_vector(g_BUS_WIDTH-1 downto 0)
            ); 
end data_reordering;

architecture Behavioral of data_reordering is
    -----------------------------------------------------------------
    -- CONSTANT DECLARATIONS
    -----------------------------------------------------------------
    constant c_NBR_OF_WORDS : positive := g_BUS_WIDTH/g_WORD_WIDTH;
begin
    process(data_in_i)
        variable v_LSB_in,v_MSB_out : natural := 0;
    begin
        for I in 0 to c_NBR_OF_WORDS-1 loop
            v_LSB_in  := g_WORD_WIDTH*I;
            v_MSB_out := g_BUS_WIDTH-v_LSB_in-1;
            data_out_o((v_MSB_out) downto v_MSB_out-(g_WORD_WIDTH-1)) <= data_in_i((v_LSB_in+g_WORD_WIDTH-1) downto v_LSB_in);
        end loop;
	end process;
end Behavioral;
