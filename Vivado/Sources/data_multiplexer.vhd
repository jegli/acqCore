-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, Data multiplexer --
-- --
-------------------------------------------------------------------------------
--
-- unit name: Data multiplexer
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 08/05/2018
--
-- version: 1.0
--
-- description: bus multiplexer
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do> 
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- acquisition core package
library work;
use work.ACQ_CORE_PACK.all;

entity data_multiplexer is
    generic (   g_CHANNELS          : positive;
                g_DATA_OUT_WIDTH    : positive
            );
    
    port(
            data_in     : in  t_fifo_rdport_array(0 to g_CHANNELS-1);
            data_out    : out t_fifo_rdport;
            sel         : in  std_logic_vector (f_find_width(g_CHANNELS)-1 downto 0)
    );

end data_multiplexer;

architecture Behavioral of data_multiplexer is
    
begin
    process(data_in,sel)
    begin
        data_out <= data_in(to_integer(unsigned(sel)));
    end process;

end Behavioral;
