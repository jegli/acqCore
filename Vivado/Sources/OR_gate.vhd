-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, OR gate --
-- --
-------------------------------------------------------------------------------
--
-- unit name: reset synchronizer
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 17/01/2019
--
-- version: 1.0
--
-- description: OR gate with variable inputs width
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: 
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity OR_gate is
    Generic( g_WIDTH : integer := 2);
    Port ( input_i : in STD_LOGIC_VECTOR (g_WIDTH-1 downto 0);
           output_o : out STD_LOGIC);
end OR_gate;

architecture Behavioral of OR_gate is

begin
    OR_gate:process(input_i)
        variable v_output : std_logic := '0';
    begin
        v_output := '0';
        
        for I in 0 to g_WIDTH-1 loop
            if(input_i(I) = '1')then
                v_output := '1';
            end if;
        end loop;
        
        output_o <= v_output;
    end process;
end Behavioral;
