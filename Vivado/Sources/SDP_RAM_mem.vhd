-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, SDP_RAM_MEM --
-- --
-------------------------------------------------------------------------------
--
-- unit name: SDP_RAM_MEM
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 16/04/2019
--
-- version: 1.0
--
-- description: Simple Dual-Port Block RAM with Two Clocks
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- acquisition core package
library work;
use work.ACQ_CORE_PACK.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity SDP_RAM_mem is
    generic(
                g_RAM_DEPTH     : integer := 8192;
                g_DWIDTH        : integer := 32
    );
    port(
        clka_i  : in  std_logic;
        clkb_i  : in  std_logic;
        ena_i   : in  std_logic;
        enb_i   : in  std_logic;
        wea_i   : in  std_logic;
        addra_i : in  std_logic_vector(f_find_width(g_RAM_DEPTH)-1 downto 0);
        addrb_i : in  std_logic_vector(f_find_width(g_RAM_DEPTH)-1 downto 0);
        dina_i  : in  std_logic_vector(g_DWIDTH-1 downto 0);
        doutb_o : out std_logic_vector(g_DWIDTH-1 downto 0)
    );
end SDP_RAM_mem;

architecture Behavioral of SDP_RAM_mem is
    type ram_type is array (g_RAM_DEPTH-1 downto 0) of std_logic_vector(g_DWIDTH-1 downto 0);
    signal RAM : ram_type;
begin
    process(clka_i)
    begin
        if rising_edge(clka_i) then
            if ena_i = '1' then
                if wea_i = '1' then
                    RAM(to_integer(unsigned(addra_i))) <= dina_i;
                end if;
            end if;
        end if;
    end process;
    
    process(clkb_i)
    begin
        if rising_edge(clkb_i) then
            if enb_i = '1' then
                doutb_o <= RAM(to_integer(unsigned(addrb_i)));
            end if;
        end if;
    end process;
end Behavioral;
