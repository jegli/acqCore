#creating timing constaints
#set_clock_groups -asynchronous -group [get_clocks -of_objects [get_ports clk_i]] -group [get_clocks -of_objects [get_ports M_AXI_clk_i]]

# clk_i = 62.5MHz
# acq_clk_i = 125MHz
# M_AXI_clk_i = 200MHz

set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports clk_i]] -to [get_clocks -of_objects [get_ports M_AXI_clk_i]] 16.000
set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports M_AXI_clk_i]] -to [get_clocks -of_objects [get_ports clk_i]] 16.000

set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports acq_clk_i]] -to [get_clocks -of_objects [get_ports clk_i]] 16.000
set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports clk_i]] -to [get_clocks -of_objects [get_ports acq_clk_i]] 16.000

set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports acq_clk_i]] -to [get_clocks -of_objects [get_ports M_AXI_clk_i]] 8.000
set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports M_AXI_clk_i]] -to [get_clocks -of_objects [get_ports acq_clk_i]] 8.000

set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports clk_i]] -to [get_clocks -of_objects [get_ports S_AXI_aclk]] 16.000
set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports S_AXI_aclk]] -to [get_clocks -of_objects [get_ports clk_i]] 16.000

set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports acq_clk_i]] -to [get_clocks -of_objects [get_ports S_AXI_aclk]] 16.000
set_max_delay -datapath_only -from [get_clocks -of_objects [get_ports S_AXI_aclk]] -to [get_clocks -of_objects [get_ports acq_clk_i]] 16.000