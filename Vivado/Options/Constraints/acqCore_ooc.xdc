# creating 125MHz and 200MHz clocks
create_clock -name clk -period 16.000 [get_ports clk_i]
create_clock -name M_AXI_clk -period 5.000 [get_ports M_AXI_clk_i]
create_clock -name S_AXI_aclk -period 16.000 [get_ports S_AXI_aclk]
create_clock -name acq_clk -period 8.000 [get_ports acq_clk_i]
