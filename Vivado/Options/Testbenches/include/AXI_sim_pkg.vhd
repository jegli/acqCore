-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, AXI-lite interface Package --
-- --
-------------------------------------------------------------------------------
--
-- unit name: AXI-lite interface Package
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 03/08/2018
--
-- version: 1.0
--
-- description: function generator simulation package
--
-- dependencies: <entity name>, ...
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

use STD.textio.all;
use ieee.std_logic_textio.all;

package AXI_SIM_PACK is
    --=====================================================================
    -- Constants definition 
    --=====================================================================

    --=====================================================================
    -- AXI read/write bus type definition
    --=====================================================================
    type t_AXI_WR_bus_out is
    record 
        AWVALID :  std_logic;
        BREADY  :  std_logic;
        WVALID  :  std_logic;
        AWADDR  :  std_logic_vector (31 downto 0);
        WDATA   :  std_logic_vector (31 downto 0);
        WSTRB   :  std_logic_vector (3 downto 0);
    end record;
    
    type t_AXI_RD_bus_out is
    record 
        ARVALID :  std_logic;
        RREADY  :  std_logic;
        ARADDR  :  std_logic_vector (31 downto 0);
    end record;
    
    type t_AXI_WR_bus_in is
    record 
        AWREADY : std_logic;
        BVALID  : std_logic;
        WREADY  : std_logic;
        BRESP   : std_logic_vector (1 downto 0);
    end record;
    
    type t_AXI_RD_bus_in is
    record 
        ARREADY : std_logic;
        RVALID  : std_logic;
        RRESP   : std_logic_vector (1 downto 0);
        RDATA   : std_logic_vector (31 downto 0);
    end record;
    
    --=====================================================================
    -- procedure definition
    --=====================================================================
        -- not generic for the moment
        procedure AXI_WRITE
        (    
            addr_i      : in std_logic_vector;
            data_i      : in integer;
            signal      AXI_bus_in_i    : in  t_AXI_WR_bus_in;
            signal      AXI_bus_out_o   : out t_AXI_WR_bus_out;
            signal      clk_i           : in  std_logic
        );
        -- not generic for the moment
        procedure AXI_READ
        (    
            addr_i      : in std_logic_vector;
            data_o      : out std_logic_vector (31 downto 0);
            signal      AXI_bus_in_i    : in  t_AXI_RD_bus_in;
            signal      AXI_bus_out_o   : out t_AXI_RD_bus_out;
            signal      clk_i           : in  std_logic
        );

    end AXI_SIM_PACK;
    
--=====================================================================
-- procedure declaration
--=====================================================================
    
package body AXI_SIM_PACK is
        --=====================================================================================
        --  32-bit axi write procedure
        --=====================================================================================
        procedure AXI_WRITE (   addr_i          : in std_logic_vector;
                                data_i          : in integer;
                                signal   AXI_bus_in_i    : in  t_AXI_WR_bus_in;
                                signal   AXI_bus_out_o   : out t_AXI_WR_bus_out;
                                signal   clk_i           : in  std_logic
                            ) is
                            
        variable valid           : std_logic_vector(2 downto 0) := (others => '0');
        
        begin
            --init
            AXI_bus_out_o.AWVALID   <='0';
            AXI_bus_out_o.WVALID    <='0';
            AXI_bus_out_o.BREADY    <='0';
        
            AXI_bus_out_o.AWADDR                <= (others => '0');
            AXI_bus_out_o.AWADDR(addr_i'range)  <= addr_i;
            AXI_bus_out_o.WSTRB                 <= (others => '1');
            AXI_bus_out_o.WDATA                 <= std_logic_vector(to_unsigned(data_i, AXI_bus_out_o.WDATA'length));
            
            --send write
            wait until clk_i = '1';
                AXI_bus_out_o.AWVALID   <= '1';
                AXI_bus_out_o.WVALID    <= '1';
                AXI_bus_out_o.BREADY    <='1';
            
            -- waiting on reads signals    
            while valid /= "111" loop
                wait until clk_i = '1';
                
                if(AXI_bus_in_i.AWREADY = '1')then
                    valid(0) := '1';
                    AXI_bus_out_o.AWVALID   <='0';
                end if;
                
                if(AXI_bus_in_i.WREADY = '1')then
                    valid(1) := '1';
                    AXI_bus_out_o.WVALID    <='0';
                end if;
                
                if(AXI_bus_in_i.BVALID = '1')then
                    valid(2) := '1';
                    AXI_bus_out_o.BREADY    <='0';
                end if;
            end loop;    
            
            AXI_bus_out_o.WSTRB     <= (others => '0');
            AXI_bus_out_o.WDATA     <= (others => '0');
            AXI_bus_out_o.AWADDR    <= (others => '0'); 

            wait for 100 ns;
        end AXI_WRITE;
        
        --=====================================================================================
        --  32-bit axi read procedure
        --=====================================================================================
        procedure AXI_READ (    addr_i      : in std_logic_vector;
                                data_o      : out std_logic_vector (31 downto 0);
                                signal      AXI_bus_in_i    : in  t_AXI_RD_bus_in;
                                signal      AXI_bus_out_o   : out t_AXI_RD_bus_out;
                                signal      clk_i           : in  std_logic
                             ) is
        begin
            --init
            AXI_bus_out_o.ARVALID   <='0';
            AXI_bus_out_o.RREADY    <='0';

            --send read 
            wait until clk_i = '1';
                AXI_bus_out_o.ARADDR                <= (others => '0');
                AXI_bus_out_o.ARADDR(addr_i'range)  <= addr_i;
                AXI_bus_out_o.ARVALID   <= '1';
                AXI_bus_out_o.RREADY    <= '1';
            wait until (AXI_bus_in_i.ARREADY = '1' and clk_i = '1');  --Client provided data 
            wait until clk_i = '1';
                AXI_bus_out_o.ARVALID   <= '0';
            wait until (AXI_bus_in_i.RVALID  = '1' and clk_i = '1');  --Client provided data  
            wait until clk_i = '1';
                AXI_bus_out_o.RREADY    <='0';
                data_o                  :=  AXI_bus_in_i.RDATA;
            wait for 1 us;
        end AXI_READ;

end AXI_SIM_PACK;