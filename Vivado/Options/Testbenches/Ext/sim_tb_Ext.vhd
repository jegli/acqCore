-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, sim_tb_DataAcquisitonCore --
-- --
-------------------------------------------------------------------------------
--
-- unit name: sim_tb_DataAcquisitonCore
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 09/07/2018
--
-- version: 1.0
--
-- description: Data acquisition simulation testbench
--
-- dependencies: 
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 
use std.textio.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use ieee.math_real.all;

-- function generator package
library work;
use work.AXI_SIM_PACK.all;
use work.MemMap_acqCore.all;
use work.MemMap_ipInfo.all;
use work.ACQ_CORE_PACK.all;

library std;
use std.env.stop;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sim_tb_Ext is

end sim_tb_Ext;

architecture testbench of sim_tb_Ext is
    --=============================================================================
    -- constant instantation 
    -- clock frequency
    constant c_CLK_PERIOD        : time := 16 ns;            --100MHz
    constant c_ACQ_CLK_PERIOD    : time := 8 ns;             --125MHz
    constant c_PCIe_CLK_PERIOD   : time := 8 ns;             --125MHz
    constant c_DDR_UI_CLK_PERIOD : time := 5 ns;             --200MHz
    constant c_DDR_UI_CLK_FREQ   : integer := 200000000;    --200MHz
    
    -- reset time
    constant c_RESET_TIME       : time := 200 ns;
    
    -- start trigger timing
    constant c_START_TIME       : time := 500 ns;

    -- Data Acquisition IP core settings
    constant c_CHANNELS         : integer := 12;   -- [channels]       
    constant c_DATA_IN_WIDTH    : integer := 32;   -- channel input width [bits]
    constant c_ACQ_AXI_WIDTH    : integer := 512;  -- [bits]

    -- Acquisition simulation parameters
    constant c_BUFFER_START_ADDRESS     : integer := 16#00000000#;
    constant c_ACQ_LENGTH               : integer := 8192; --524288;
    constant c_ACQ_LENGTH_IN_BYTES      : integer := c_ACQ_LENGTH*4; --[conversion in bytes]
    
    -- AXI interface parameters
    constant c_PCIE_AXI_WIDTH               : integer := 256;                                               -- [bits]       
    constant c_PCIE_RD_BURST_LENGTH         : integer := 64;                                                -- [bursts]
    constant c_PCIE_AXI_WIDTH_BYTES         : integer := c_PCIE_AXI_WIDTH/8;                                -- [bytes]
    constant c_PCIE_MAX_BURST_REQUEST_BYTES : integer := c_PCIE_RD_BURST_LENGTH*c_PCIE_AXI_WIDTH_BYTES;     -- [bytes]

    -- Data Acquistion core register default value
    constant c_DEFAULT_CONTROL          : integer := 16#04003FFF#; --16#04000007#;
    constant c_DEFAULT_CONTROL_START    : integer := 16#07FFFFFC#; --16#04000007#;
    constant c_DEFAULT_FAULT_ENABLE     : integer := 16#00000001#;
    constant c_DEFAULT_BUFFER_ADDR      : integer := c_BUFFER_START_ADDRESS;
    constant c_DEFAULT_ACQ_LENGTH       : integer := c_ACQ_LENGTH;
    constant c_DEFAULT_ACQ_DECIMATION   : integer := 10;
    
    -- AXI perfomance monitor register map
    constant c_APM_CONTROL_REGISTER         : std_logic_vector(31 downto 0) := x"00000300";
    constant c_APM_SAMPLE_REGISTER          : std_logic_vector(31 downto 0) := x"0000002C";
    constant c_APM_SLOT_0_WR_BYTE_COUNT     : std_logic_vector(31 downto 0) := x"00000200";
    constant c_APM_SLOT_0_WR_TRANS_COUNT    : std_logic_vector(31 downto 0) := x"00000210";
    constant c_APM_SLOT_0_WR_LATENCY_COUNT  : std_logic_vector(31 downto 0) := x"00000220";

    -- signal instantation
    signal s_clk                : std_logic := '1';
    signal s_acq_clk            : std_logic := '1';
    signal s_PCIe_clk           : std_logic := '1';
    signal s_m_axi_clk          : std_logic := '0';
    signal s_m_axi_reset        : std_logic := '0';
    signal s_start_trig         : std_logic_vector(11 downto 0) := (others => '0');
    signal s_data               : std_logic_vector(31 downto 0) := (others => '0');
    signal s_irq_acq_finished   : std_logic := '0';
    signal s_irq_acq_error      : std_logic := '0';
    signal s_buffer_data_cnt    : integer := 0;
    signal s_PCIe_err_cnt       : integer := 0;
    
    --ddr4 signals
    signal s_sys_rst : std_logic := '0';

    signal s_c0_ddr4_ui_clk  : std_logic := '0';

    signal s_c0_init_calib_complete : std_logic := '0';
    signal s_c0_sys_clk_p    : std_logic := '0';
    signal s_c0_sys_clk_n    : std_logic := '0';
   
    signal s_c0_ddr4_act_n          : std_logic  := '0';
    signal s_c0_ddr4_adr            : std_logic_vector(16 downto 0);
    signal s_c0_ddr4_ba             : std_logic_vector(1 downto 0);
    signal s_c0_ddr4_bg             : std_logic_vector(0 downto 0);
    signal s_c0_ddr4_cke            : std_logic_vector(0 downto 0);
    signal s_c0_ddr4_odt            : std_logic_vector(0 downto 0);
    signal s_c0_ddr4_cs_n           : std_logic_vector(0 downto 0);
    signal s_c0_ddr4_ck_t           : std_logic  := '0';
    signal s_c0_ddr4_ck_c           : std_logic  := '0';
    signal s_c0_ddr4_reset_n        : std_logic  := '0';
    
    signal s_c0_ddr4_dm_dbi_n       : std_logic_vector(7 downto 0) := (others => 'Z');
    signal s_c0_ddr4_dq             : std_logic_vector(63 downto 0) := (others => 'Z');
    signal s_c0_ddr4_dqs_c          : std_logic_vector(7 downto 0) := (others => 'Z');
    signal s_c0_ddr4_dqs_t          : std_logic_vector(7 downto 0) := (others => 'Z');
    
    -- axi master interface
    signal s_M_AXI_awaddr        : std_logic_vector ( 30 downto 0 );
    signal s_M_AXI_awburst       : std_logic_vector ( 1 downto 0 );
    signal s_M_AXI_awcache       : std_logic_vector ( 3 downto 0 );
    signal s_M_AXI_awid          : std_logic_vector ( 3 downto 0 );
    signal s_M_AXI_awlen         : std_logic_vector ( 7 downto 0 );
    signal s_M_AXI_awlock        : std_logic := '0';
    signal s_M_AXI_awprot        : std_logic_vector ( 2 downto 0 );
    signal s_M_AXI_awqos         : std_logic_vector ( 3 downto 0 );
    signal s_M_AXI_awready       : std_logic := '0';
    signal s_M_AXI_awsize        : std_logic_vector ( 2 downto 0 );
    signal s_M_AXI_awuser        : std_logic_vector ( 1 downto 0 );
    signal s_M_AXI_awvalid       : std_logic := '0';
    signal s_M_AXI_bid           : std_logic_vector ( 3 downto 0 );
    signal s_M_AXI_bready        : std_logic := '0'; 
    signal s_M_AXI_bresp         : std_logic_vector ( 1 downto 0 );
    signal s_M_AXI_buser         : std_logic_vector ( 1 downto 0 );
    signal s_M_AXI_bvalid        : std_logic := '0';
    signal s_M_AXI_wdata         : std_logic_vector ( c_ACQ_AXI_WIDTH-1 downto 0 );
    signal s_M_AXI_wlast         : std_logic := '0';
    signal s_M_AXI_wready        : std_logic := '0';
    signal s_M_AXI_wstrb         : std_logic_vector ( 63 downto 0 );
    signal s_M_AXI_wuser         : std_logic_vector ( (c_ACQ_AXI_WIDTH/8)-1 downto 0 );
    signal s_M_AXI_wvalid        : std_logic := '0';
    
    -- axi-lite slave interface
    signal s_reg_AXI_WR_out     : t_AXI_WR_bus_out := (AWVALID =>'0',BREADY => '0',WVALID => '0',AWADDR => (others=>'0'),WDATA => (others=>'0'),WSTRB => (others=>'0'));
    signal s_reg_AXI_WR_in      : t_AXI_WR_bus_in;
    signal s_reg_AXI_RD_out     : t_AXI_RD_bus_out := (ARVALID=>'0',RREADY => '0',ARADDR => (others=>'0'));
    signal s_reg_AXI_RD_in      : t_AXI_RD_bus_in;
    
    signal s_APM_AXI_WR_out     : t_AXI_WR_bus_out := (AWVALID =>'0',BREADY => '0',WVALID => '0',AWADDR => (others=>'0'),WDATA => (others=>'0'),WSTRB => (others=>'0'));
    signal s_APM_AXI_WR_in      : t_AXI_WR_bus_in;
    signal s_APM_AXI_RD_out     : t_AXI_RD_bus_out := (ARVALID=>'0',RREADY => '0',ARADDR => (others=>'0'));
    signal s_APM_AXI_RD_in      : t_AXI_RD_bus_in;
    
    -- PCIe interface 
    signal s_PCIe_AXI_RD_start              : std_logic := '0';
    signal s_PCIe_AXI_RD_start_address      : std_logic_vector ( 31 downto 0 ) := (others => '0');
    signal s_PCIe_AXI_RD_data_size          : std_logic_vector ( 31 downto 0 ) := (others => '0'); 
    signal s_PCIe_AXI_RD_burst_length       : std_logic_vector ( 8 downto 0 ) := (others => '0');
    signal s_PCIe_AXI_RD_done               : std_logic;
    signal s_PCIe_AXI_RD_error              : std_logic;
    signal s_PCIe_AXI_RD_busy               : std_logic;
    signal s_PCIe_AXI_RD_data_valid         : std_logic;
    signal s_PCIe_AXI_RD_data               : std_logic_vector ( c_PCIE_AXI_WIDTH-1 downto 0 );
    
    signal s_PCIe_AXI_WR_start              : std_logic := '0';
    signal s_PCIe_AXI_WR_start_address      : std_logic_vector ( 31 downto 0 ) := (others => '0');
    signal s_PCIe_AXI_WR_data_size          : std_logic_vector ( 31 downto 0 ) := (others => '0');
    signal s_PCIe_AXI_WR_burst_length       : std_logic_vector ( 8 downto 0 ) := (others => '0');
    signal s_PCIe_AXI_WR_debug_data_offset  : std_logic_vector ( 3 downto 0 ) := (others => '0');
    signal s_PCIe_AXI_WR_done               : std_logic;
    signal s_PCIe_AXI_WR_error              : std_logic;
    signal s_PCIe_AXI_WR_busy               : std_logic;
    
    --=============================================================================
    -- component instantation
    
    component sim_Ext_wrapper is
    port (
        AXI_reg_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_reg_arready : out STD_LOGIC;
        AXI_reg_arvalid : in STD_LOGIC;
        AXI_reg_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_reg_awready : out STD_LOGIC;
        AXI_reg_awvalid : in STD_LOGIC;
        AXI_reg_bready : in STD_LOGIC;
        AXI_reg_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_reg_bvalid : out STD_LOGIC;
        AXI_reg_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_reg_rlast : out STD_LOGIC;
        AXI_reg_rready : in STD_LOGIC;
        AXI_reg_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_reg_rvalid : out STD_LOGIC;
        AXI_reg_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_reg_wlast : in STD_LOGIC;
        AXI_reg_wready : out STD_LOGIC;
        AXI_reg_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
        AXI_reg_wvalid : in STD_LOGIC;
        AXI_APM_araddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
        AXI_APM_arready : out STD_LOGIC;
        AXI_APM_arvalid : in STD_LOGIC;
        AXI_APM_awaddr : in STD_LOGIC_VECTOR ( 15 downto 0 );
        AXI_APM_awready : out STD_LOGIC;
        AXI_APM_awvalid : in STD_LOGIC;
        AXI_APM_bready : in STD_LOGIC;
        AXI_APM_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_APM_bvalid : out STD_LOGIC;
        AXI_APM_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_APM_rready : in STD_LOGIC;
        AXI_APM_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
        AXI_APM_rvalid : out STD_LOGIC;
        AXI_APM_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
        AXI_APM_wready : out STD_LOGIC;
        AXI_APM_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
        AXI_APM_wvalid : in STD_LOGIC;
        PCIe_AXI_RD_burst_length : in STD_LOGIC_VECTOR ( 8 downto 0 );
        PCIe_AXI_RD_busy : out STD_LOGIC;
        PCIe_AXI_RD_data : out STD_LOGIC_VECTOR ( c_PCIE_AXI_WIDTH-1 downto 0 );
        PCIe_AXI_RD_data_size : in STD_LOGIC_VECTOR ( 31 downto 0 );
        PCIe_AXI_RD_data_valid : out STD_LOGIC;
        PCIe_AXI_RD_done : out STD_LOGIC;
        PCIe_AXI_RD_error : out STD_LOGIC;
        PCIe_AXI_RD_start : in STD_LOGIC;
        PCIe_AXI_RD_start_address : in STD_LOGIC_VECTOR ( 31 downto 0 );
        PCIe_AXI_WR_burst_length : in STD_LOGIC_VECTOR ( 8 downto 0 );
        PCIe_AXI_WR_busy : out STD_LOGIC;
        PCIe_AXI_WR_data_size : in STD_LOGIC_VECTOR ( 31 downto 0 );
        PCIe_AXI_WR_done : out STD_LOGIC;
        PCIe_AXI_WR_error : out STD_LOGIC;
        PCIe_AXI_WR_start : in STD_LOGIC;
        PCIe_AXI_WR_start_address : in STD_LOGIC_VECTOR ( 31 downto 0 );
        PCIe_debug_data_offset : in STD_LOGIC_VECTOR ( 3 downto 0 );
        acq_axi_busy_o : out STD_LOGIC;
        acq_axi_length_o : out STD_LOGIC_VECTOR ( 8 downto 0 );
        acq_clk_i : in STD_LOGIC;
        acq_start_trig_i : in STD_LOGIC_VECTOR ( 11 downto 0 );
        busy_o : out STD_LOGIC;
        c0_init_calib_complete_0 : out STD_LOGIC;
        c0_sys_clk_n : in STD_LOGIC;
        c0_sys_clk_p : in STD_LOGIC;
        ch_0_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_10_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_11_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_1_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_2_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_3_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_4_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_5_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_6_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_7_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_8_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        ch_9_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        clk_i : in STD_LOGIC;
        ddr4_0_act_n : out STD_LOGIC;
        ddr4_0_adr : out STD_LOGIC_VECTOR ( 16 downto 0 );
        ddr4_0_ba : out STD_LOGIC_VECTOR ( 1 downto 0 );
        ddr4_0_bg : out STD_LOGIC_VECTOR ( 0 to 0 );
        ddr4_0_ck_c : out STD_LOGIC_VECTOR ( 0 to 0 );
        ddr4_0_ck_t : out STD_LOGIC_VECTOR ( 0 to 0 );
        ddr4_0_cke : out STD_LOGIC_VECTOR ( 0 to 0 );
        ddr4_0_cs_n : out STD_LOGIC_VECTOR ( 0 to 0 );
        ddr4_0_dm_n : inout STD_LOGIC_VECTOR ( 7 downto 0 );
        ddr4_0_dq : inout STD_LOGIC_VECTOR ( 63 downto 0 );
        ddr4_0_dqs_c : inout STD_LOGIC_VECTOR ( 7 downto 0 );
        ddr4_0_dqs_t : inout STD_LOGIC_VECTOR ( 7 downto 0 );
        ddr4_0_odt : out STD_LOGIC_VECTOR ( 0 to 0 );
        ddr4_0_reset_n : out STD_LOGIC;
        irq_acq_error_o : out STD_LOGIC;
        irq_acq_finished_o : out STD_LOGIC;
        sys_rst : in STD_LOGIC;
        c0_ddr4_ui_clk : out STD_LOGIC;
        PCIe_clk_i : in STD_LOGIC
    );
    end component;
    -----------------------------------------------------------------------
    component ddr4_mem_tb is
    port 
        (
            sys_rst                : out std_logic;
            
            c0_ddr4_ui_clk         : in std_logic;
            
            c0_init_calib_complete : in std_logic;
            
            c0_sys_clk_p           : out std_logic;
            c0_sys_clk_n           : out std_logic;
            c0_ddr4_act_n          : in std_logic;
            c0_ddr4_adr            : in std_logic_vector(16 downto 0);
            c0_ddr4_ba             : in std_logic_vector(1 downto 0);
            c0_ddr4_bg             : in std_logic_vector(0 downto 0);
            c0_ddr4_cke            : in std_logic_vector(0 downto 0);
            c0_ddr4_odt            : in std_logic_vector(0 downto 0);
            c0_ddr4_cs_n           : in std_logic_vector(0 downto 0);
            c0_ddr4_ck_t           : out std_logic;
            c0_ddr4_ck_c           : out std_logic;
            c0_ddr4_reset_n        : in std_logic;
            c0_ddr4_dm_dbi_n       : inout std_logic_vector(7 downto 0);
            c0_ddr4_dq             : inout std_logic_vector(63 downto 0);
            c0_ddr4_dqs_c          : inout std_logic_vector(7 downto 0);
            c0_ddr4_dqs_t          : inout std_logic_vector(7 downto 0)
        );
    end component ddr4_mem_tb;
    --=============================================================================
begin

      
    ddr4_mem_tb_0:ddr4_mem_tb
    port map(
        sys_rst => s_sys_rst,
        
        c0_ddr4_ui_clk => s_c0_ddr4_ui_clk,
        c0_init_calib_complete => s_c0_init_calib_complete,
        
        c0_sys_clk_p => s_c0_sys_clk_p,
        c0_sys_clk_n => s_c0_sys_clk_n,
        c0_ddr4_act_n => s_c0_ddr4_act_n,
        c0_ddr4_adr => s_c0_ddr4_adr,
        c0_ddr4_ba => s_c0_ddr4_ba,
        c0_ddr4_bg => s_c0_ddr4_bg,
        c0_ddr4_cke => s_c0_ddr4_cke,
        c0_ddr4_odt => s_c0_ddr4_odt,
        c0_ddr4_cs_n => s_c0_ddr4_cs_n,
        c0_ddr4_ck_t => s_c0_ddr4_ck_t,
        c0_ddr4_ck_c => s_c0_ddr4_ck_c,
        c0_ddr4_reset_n => s_c0_ddr4_reset_n,

        c0_ddr4_dm_dbi_n => s_c0_ddr4_dm_dbi_n,
        c0_ddr4_dq => s_c0_ddr4_dq,
        c0_ddr4_dqs_c => s_c0_ddr4_dqs_c,
        c0_ddr4_dqs_t => s_c0_ddr4_dqs_t
    );

sim_Ext_wrapper_0: sim_Ext_wrapper
  port map(
        AXI_reg_awaddr => s_reg_AXI_WR_out.AWADDR,
        AXI_reg_awvalid => s_reg_AXI_WR_out.AWVALID,
        AXI_reg_awready => s_reg_AXI_WR_in.AWREADY,
        AXI_reg_wdata => s_reg_AXI_WR_out.WDATA,
        AXI_reg_wstrb => s_reg_AXI_WR_out.WSTRB,
        AXI_reg_wlast => '0',
        AXI_reg_wvalid => s_reg_AXI_WR_out.WVALID,
        AXI_reg_wready => s_reg_AXI_WR_in.WREADY,
        AXI_reg_bresp => s_reg_AXI_WR_in.BRESP,
        AXI_reg_bvalid => s_reg_AXI_WR_in.BVALID,
        AXI_reg_bready => s_reg_AXI_WR_out.BREADY,
        AXI_reg_araddr => s_reg_AXI_RD_out.ARADDR,
        AXI_reg_arvalid => s_reg_AXI_RD_out.ARVALID,
        AXI_reg_arready => s_reg_AXI_RD_in.ARREADY,
        AXI_reg_rdata => s_reg_AXI_RD_in.RDATA,
        AXI_reg_rresp => s_reg_AXI_RD_in.RRESP,
        AXI_reg_rlast => open,
        AXI_reg_rvalid => s_reg_AXI_RD_in.RVALID,
        AXI_reg_rready => s_reg_AXI_RD_out.RREADY,
        
        AXI_APM_awaddr => s_APM_AXI_WR_out.AWADDR(15 downto 0),
        AXI_APM_awvalid => s_APM_AXI_WR_out.AWVALID,
        AXI_APM_awready => s_APM_AXI_WR_in.AWREADY,
        AXI_APM_wdata => s_APM_AXI_WR_out.WDATA,
        AXI_APM_wstrb => s_APM_AXI_WR_out.WSTRB,
        AXI_APM_wvalid => s_APM_AXI_WR_out.WVALID,
        AXI_APM_wready => s_APM_AXI_WR_in.WREADY,
        AXI_APM_bresp => s_APM_AXI_WR_in.BRESP,
        AXI_APM_bvalid => s_APM_AXI_WR_in.BVALID,
        AXI_APM_bready => s_APM_AXI_WR_out.BREADY,
        AXI_APM_araddr => s_APM_AXI_RD_out.ARADDR(15 downto 0),
        AXI_APM_arvalid => s_APM_AXI_RD_out.ARVALID,
        AXI_APM_arready => s_APM_AXI_RD_in.ARREADY,
        AXI_APM_rdata => s_APM_AXI_RD_in.RDATA,
        AXI_APM_rresp => s_APM_AXI_RD_in.RRESP,
        AXI_APM_rvalid => s_APM_AXI_RD_in.RVALID,
        AXI_APM_rready => s_APM_AXI_RD_out.RREADY,
        
        PCIe_AXI_RD_burst_length => s_PCIe_AXI_RD_burst_length,
        PCIe_AXI_RD_busy => s_PCIe_AXI_RD_busy,
        PCIe_AXI_RD_data => s_PCIe_AXI_RD_data,
        PCIe_AXI_RD_data_size => s_PCIe_AXI_RD_data_size,
        PCIe_AXI_RD_data_valid => s_PCIe_AXI_RD_data_valid,
        PCIe_AXI_RD_done => s_PCIe_AXI_RD_done,
        PCIe_AXI_RD_error => s_PCIe_AXI_RD_error,
        PCIe_AXI_RD_start => s_PCIe_AXI_RD_start,
        PCIe_AXI_RD_start_address => s_PCIe_AXI_RD_start_address,
        PCIe_AXI_WR_burst_length => s_PCIe_AXI_WR_burst_length,
        PCIe_AXI_WR_busy => s_PCIe_AXI_WR_busy,
        PCIe_AXI_WR_data_size => s_PCIe_AXI_WR_data_size,
        PCIe_AXI_WR_done => s_PCIe_AXI_WR_done,
        PCIe_AXI_WR_error => s_PCIe_AXI_WR_error,
        PCIe_AXI_WR_start => s_PCIe_AXI_WR_start,
        PCIe_AXI_WR_start_address => s_PCIe_AXI_WR_start_address,
        PCIe_debug_data_offset => s_PCIe_AXI_WR_debug_data_offset,
  
        acq_axi_busy_o => open,
        acq_axi_length_o => open,
        acq_start_trig_i => s_start_trig,
        busy_o => open,
        
        c0_init_calib_complete_0 => s_c0_init_calib_complete,
        c0_sys_clk_n => s_c0_sys_clk_n,
        c0_sys_clk_p => s_c0_sys_clk_p,
        
        ch_0_i => s_data,
        ch_1_i => s_data,
        ch_2_i => s_data,
        ch_3_i => s_data,
        ch_4_i => s_data,
        ch_5_i => s_data,
        ch_6_i => s_data,
        ch_7_i => s_data,
        ch_8_i => s_data,
        ch_9_i => s_data,
        ch_10_i => s_data,
        ch_11_i => s_data,

        clk_i => s_clk,
        acq_clk_i => s_acq_clk,
        PCIe_clk_i => s_PCIe_clk,

        ddr4_0_act_n    => s_c0_ddr4_act_n,
        ddr4_0_adr      => s_c0_ddr4_adr,
        ddr4_0_ba       => s_c0_ddr4_ba,
        ddr4_0_bg       => s_c0_ddr4_bg,
        ddr4_0_ck_c(0)     => s_c0_ddr4_ck_c,
        ddr4_0_ck_t(0)     => s_c0_ddr4_ck_t,
        ddr4_0_cke      => s_c0_ddr4_cke,
        ddr4_0_cs_n     => s_c0_ddr4_cs_n,
        ddr4_0_dm_n     => s_c0_ddr4_dm_dbi_n,
        ddr4_0_dq       => s_c0_ddr4_dq,
        ddr4_0_dqs_c    => s_c0_ddr4_dqs_c,
        ddr4_0_dqs_t    => s_c0_ddr4_dqs_t,
        ddr4_0_odt      => s_c0_ddr4_odt,
        ddr4_0_reset_n  => s_c0_ddr4_reset_n,
        
        irq_acq_finished_o => s_irq_acq_finished,
        irq_acq_error_o => s_irq_acq_error,
        sys_rst => s_sys_rst,
        
        c0_ddr4_ui_clk => s_c0_ddr4_ui_clk
  );

    --=============================================================================
    -- clock generation
    --=============================================================================
    s_clk       <= not s_clk        after c_CLK_PERIOD/2;
    s_acq_clk   <= not s_acq_clk    after c_ACQ_CLK_PERIOD/2;
    s_PCIe_clk  <= not s_PCIe_clk   after c_PCIe_CLK_PERIOD/2;
    
    --=============================================================================
    -- generate data flow (only with external start trigger)
    --=============================================================================
    process
        variable v_data         : integer := 0;
    begin
        wait until s_c0_init_calib_complete = '1';
        wait for 1 us;
        
        wait until unsigned(s_start_trig) /= 0;
        -- add 2 clock period delays
        wait until s_acq_clk = '1';
        wait until s_acq_clk = '1';
        loop
            v_data := v_data + 1;
            s_data <= std_logic_vector(to_unsigned(v_data,s_data'length));
            wait until s_acq_clk = '1';
        end loop;
        
        wait;
    end process;
    
    --===========================================================================
    -- PCIe data reading validation 
    --===========================================================================
--    p_test: process
--        variable v_test_err_cnt : integer := 0;
--        variable v_test_cnt     : integer := 0;
--        variable v_test_err     : std_logic := '0';
--    begin    
--            v_test_cnt := 0;
--            -- comparing all data
--            for d in 0 to 63 loop
--                v_test_err := '0';
--                --waiting a valid incoming data
--                wait until s_PCIe_AXI_RD_data_valid = '1' and rising_edge(s_PCIe_clk);
                
--                --comparing each 32bit word of each burst content
--                for l in 0 to 7 loop
--                    --32bit word testing
--                    if (unsigned(s_PCIe_AXI_RD_data(32*(l+1)-1 downto 32*l)) /= to_unsigned(v_test_cnt,32))then
--                         v_test_err_cnt := v_test_err_cnt + 1; 
--                         v_test_err := '1';
--                    end if;    
--                    v_test_cnt := v_test_cnt + 1;
--                end loop;
                
--                if ( v_test_err = '1')then
--                    report " - word " &integer'image(d) & " - corrupted data: PCIe_AXI_RD_data = " & integer'image(to_integer(unsigned(s_PCIe_AXI_RD_data))) &" for data " & integer'image(v_test_cnt) severity warning; 
--                end if;
--            end loop;

--            report "INFO: Data comparing finished for one channel"; 
--    end process;
    
    --===========================================================================
    -- Acquistion data reading validation 
    --===========================================================================
    p_acq_data_reading_validation: process
        variable v_PCIe_err_cnt : integer := 0;
        variable v_PCIe_cnt     : integer := 0;
        variable v_PCIe_err     : std_logic := '0';
    begin    
        for I in 0 to c_CHANNELS-1 loop
            v_PCIe_cnt := 0;
            -- comparing all data
            for d in 0 to (c_ACQ_LENGTH_IN_BYTES/(c_PCIE_AXI_WIDTH/8))-1 loop
                v_PCIe_err := '0';
                --waiting a valid incoming data
                wait until s_PCIe_AXI_RD_data_valid = '1' and rising_edge(s_PCIe_clk);
                
                --comparing each 32bit word of each burst content
                for l in 0 to (c_PCIE_AXI_WIDTH/c_DATA_IN_WIDTH)-1 loop
                    --32bit word testing
                    if (unsigned(s_PCIe_AXI_RD_data(c_DATA_IN_WIDTH*(l+1)-1 downto c_DATA_IN_WIDTH*l)) /= to_unsigned(v_PCIe_cnt,c_DATA_IN_WIDTH))then
                         v_PCIe_err_cnt := v_PCIe_err_cnt + 1; 
                         v_PCIe_err := '1';
                    end if;    
                    v_PCIe_cnt := v_PCIe_cnt + 1;
                end loop;
                
                if ( v_PCIe_err = '1')then
                    report "channel " &integer'image(I) & " - word " &integer'image(d) & " - corrupted data: PCIe_AXI_RD_data = " & integer'image(to_integer(unsigned(s_PCIe_AXI_RD_data))) &" for data " & integer'image(v_PCIe_cnt) severity warning; 
                end if;
                s_PCIe_err_cnt <= v_PCIe_err_cnt;
            end loop;

            report "INFO: Data comparing finished for one channel"; 
        end loop;
    end process;
    
    --=============================================================================
    -- DUT testbench
    --=============================================================================
    process
        variable v_data         : std_logic_vector (31 downto 0) := (others => '0');
        variable v_buffer_size  : std_logic_vector (31 downto 0) := (others => '0');
        variable v_index        : integer := 0;
        variable v_acq_bytes    : integer := 0;
        variable v_acq_latency  : real    := 0.0;
        variable v_acq_bandwith : real    := 0.0;
    begin
        wait until s_c0_init_calib_complete = '1';

        ------------------------------------------------------------
        -- dummy memory write at address 0 to remove warnings (hack)
        s_PCIe_AXI_WR_data_size             <= std_logic_vector(to_unsigned(4096,s_PCIe_AXI_WR_data_size'length));
        s_PCIe_AXI_WR_burst_length          <= std_logic_vector(to_unsigned(64,s_PCIe_AXI_WR_burst_length'length));
        s_PCIe_AXI_WR_start_address         <= std_logic_vector(to_unsigned(0,s_PCIe_AXI_WR_start_address'length));
        
        wait until s_PCIe_clk = '1';
        s_PCIe_AXI_WR_start <= '1';
        wait until s_PCIe_clk = '1';
        s_PCIe_AXI_WR_start <= '0';
        
        wait until s_PCIe_AXI_WR_done = '1';
        
        wait for 1 us;
        
        ------------------------------------------------------------
        report lf & "*******************************************" & lf &
                    "Reading the IP core information registers..."; -- severity note
        AXI_READ(C_Reg_ipInfo_firmwareVersion,v_data,s_reg_AXI_RD_in,s_reg_AXI_RD_out,s_clk); 
        if(unsigned(v_data) /= unsigned(c_FIRMWARE_VERSION))then
            report "IP firmware version error!" severity error;
            stop;
        else
            report "IP firmware version:" & integer'image(to_integer(unsigned(c_FIRMWARE_VERSION)));
        end if;
        
        AXI_READ(C_Reg_ipInfo_memMapVersion,v_data,s_reg_AXI_RD_in,s_reg_AXI_RD_out,s_clk); 
        if(unsigned(v_data) /= unsigned(c_MEM_MAP_VESRION))then
            report "IP memory map version error!" severity error;
            stop;
        else
            report "IP memory map version:" & integer'image(to_integer(unsigned(c_MEM_MAP_VESRION)));
        end if;
        ------------------------------------------------------------
        report lf & "*******************************************" & lf & -- severity note
                    "Initialize the registers..."; -- severity note
        -- Initializing main acq registers
        AXI_WRITE(C_Reg_AcqCore_control,c_DEFAULT_CONTROL,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);                   -- control register
        AXI_WRITE(C_Reg_AcqCore_faultsEnable,c_DEFAULT_FAULT_ENABLE,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk); 
        AXI_WRITE(C_Reg_AcqCore_bufferStartAddress,c_DEFAULT_BUFFER_ADDR,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);     -- buffer address register
        
        -- Acquisition Lengths 
        AXI_WRITE(C_Reg_AcqCore_acqLength0,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength1,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength2,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength3,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength4,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength5,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength6,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength7,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength8,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength9,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength10,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqLength11,c_DEFAULT_ACQ_LENGTH,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register

        --decimation
        AXI_WRITE(C_Reg_AcqCore_acqDecimation0,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation1,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation2,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation3,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation4,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation5,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation6,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation7,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation8,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation9,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation10,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        AXI_WRITE(C_Reg_AcqCore_acqDecimation11,0,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);    -- buffer address register
        
        
        -- Start acquistion
--        wait until s_clk = '1';
--        s_start_trig    <= (others => '1'); 
--        wait until s_clk = '1';
--        s_start_trig    <= (others => '0'); 
        report "starting acquisition!"; -- severity note
        AXI_WRITE(C_Reg_AcqCore_control,c_DEFAULT_CONTROL_START,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);  
        wait for 20 us;
        
        report "Interrupting acquisition with software reset!"; -- severity note
        AXI_WRITE(C_Reg_AcqCore_control,c_DEFAULT_CONTROL,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);  
        wait for 1 us;
        
        -- APM configure
        -- reset counters
        AXI_WRITE(c_APM_CONTROL_REGISTER,2,s_APM_AXI_WR_in,s_APM_AXI_WR_out,s_c0_ddr4_ui_clk);
        -- enable counters
        AXI_WRITE(c_APM_CONTROL_REGISTER,1,s_APM_AXI_WR_in,s_APM_AXI_WR_out,s_c0_ddr4_ui_clk);
        
        report "starting new acquisition!"; -- severity note
        AXI_WRITE(C_Reg_AcqCore_control,c_DEFAULT_CONTROL_START,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);  
        

        report "Waiting the end of the acquisition..."; -- severity note
        while s_irq_acq_finished = '0' loop
            AXI_READ(C_Reg_AcqCore_faults,v_data,s_reg_AXI_RD_in,s_reg_AXI_RD_out,s_clk); 
            if v_data(0) = '1' then
                --clear faults
                AXI_WRITE(C_Reg_AcqCore_control,c_DEFAULT_CONTROL,s_reg_AXI_WR_in,s_reg_AXI_WR_out,s_clk);  
                report "ERROR: acquisition failed!" severity error; 
                wait for 10 us;
                stop; 
            end if;
            
            -- perform PCIe WR access to reduce acquisition bandwidth
--            wait until s_PCIe_clk = '1';
--            s_PCIe_AXI_WR_start <= '1';
--            wait until s_PCIe_clk = '1';
--            s_PCIe_AXI_WR_start <= '0';
            
--            wait until s_PCIe_AXI_WR_done = '1';
            
--            wait for 1 us;
        end loop;
        wait for 1 us;
        
        -- read sample register
        AXI_READ(c_APM_SAMPLE_REGISTER,v_data,s_APM_AXI_RD_in,s_APM_AXI_RD_out,s_c0_ddr4_ui_clk); 
        wait for 1 us;
        
        AXI_READ(c_APM_SLOT_0_WR_BYTE_COUNT,v_data,s_APM_AXI_RD_in,s_APM_AXI_RD_out,s_c0_ddr4_ui_clk);  
        v_acq_bytes := to_integer(unsigned(v_data));
        AXI_READ(c_APM_SLOT_0_WR_TRANS_COUNT,v_data,s_APM_AXI_RD_in,s_APM_AXI_RD_out,s_c0_ddr4_ui_clk); 
        AXI_READ(c_APM_SLOT_0_WR_LATENCY_COUNT,v_data,s_APM_AXI_RD_in,s_APM_AXI_RD_out,s_c0_ddr4_ui_clk);  
        v_acq_latency := real(to_integer(unsigned(v_data)));  
        v_acq_latency := v_acq_latency/200.0;
        v_acq_bandwith := real(v_acq_bytes)/v_acq_latency;
        wait for 1 ps;
        report "Acquisition average bandwith: " & integer'image (integer(v_acq_bandwith)) & "MB/s";  
        
        --stop;
        
        -- readingd acquisition from the DDR4 memory
        report "*******************************************"; -- severity note
        report "Starting reading data through PCIe interface..."; 
        --Reading acquisition data from the ddr4 memory 
        AXI_READ(C_Reg_AcqCore_bufferSize,v_buffer_size,s_reg_AXI_RD_in,s_reg_AXI_RD_out,s_clk);  
        
        for x in 0 to c_CHANNELS-1 loop
            s_buffer_data_cnt <= c_ACQ_LENGTH_IN_BYTES;
            v_index := 0;
            wait for 100 ns; 
            while s_buffer_data_cnt /= 0 loop
            
                if(s_buffer_data_cnt < c_PCIE_MAX_BURST_REQUEST_BYTES )then
                    s_PCIe_AXI_RD_burst_length          <= std_logic_vector(to_unsigned(s_buffer_data_cnt/c_PCIE_AXI_WIDTH_BYTES,s_PCIe_AXI_RD_burst_length'length));
                    s_PCIe_AXI_RD_data_size             <= std_logic_vector(to_unsigned(s_buffer_data_cnt,s_PCIe_AXI_RD_data_size'length));
                else
                    s_PCIe_AXI_RD_burst_length          <= std_logic_vector(to_unsigned(c_PCIE_RD_BURST_LENGTH,s_PCIe_AXI_RD_burst_length'length));
                    s_PCIe_AXI_RD_data_size             <= std_logic_vector(to_unsigned(c_PCIE_MAX_BURST_REQUEST_BYTES,s_PCIe_AXI_RD_data_size'length));
                end if;
            
                s_PCIe_AXI_RD_start_address         <= std_logic_vector(to_unsigned(c_BUFFER_START_ADDRESS + x*to_integer(unsigned(v_buffer_size)) + v_index,s_PCIe_AXI_RD_start_address'length));
    
                --$display("INFO: Reading at buffer start address 0x%h, buffer number %d",PCIe_AXI_RD_start_address_r, x); 
                --report "Reading at buffer start address 0x" & to_hstring(to_unsigned(s_PCIe_AXI_RD_start_address,32)) & ", buffer number " & integer'image(x); 
                
                wait until s_PCIe_clk = '1';
                v_index := v_index + to_integer(unsigned(s_PCIe_AXI_RD_data_size));
                s_PCIe_AXI_RD_start <= '1';
                wait until s_PCIe_clk = '1';
                s_PCIe_AXI_RD_start <= '0';
                
                s_buffer_data_cnt <= s_buffer_data_cnt - to_integer(unsigned(s_PCIe_AXI_RD_data_size));
                
                wait until s_PCIe_AXI_RD_done = '1';
            end loop;    
            wait for 500 ns;
                
            report "Finished reading acquisition data through PCIe interface for channel "& integer'image(x) & "!"; 
            if(s_PCIe_err_cnt = 0)then
                report "Finished with no corrupted data!"; 
            else
                report "PCIe readed data error ( corrupted data:" & integer'image (s_PCIe_err_cnt) & ")" severity error;     
            end if;  
            wait for 100 ns; 
        end loop;    
        
        stop;
        wait;
    end process;
    
end testbench;
