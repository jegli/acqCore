
################################################################
# This is a generated script based on design: sim_Ext
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2018.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source sim_Ext_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# AXI_MM_bidir_interface, axi4lite_cheburashka_bridge, reset_synchronizer

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xcku040-ffva1156-1-c
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name sim_Ext

# This script was generated for a remote BD. To create a non-remote design,
# change the variable <run_remote_bd_flow> to <0>.

set run_remote_bd_flow 1
if { $run_remote_bd_flow == 1 } {
  # Set the reference directory for source file relative paths (by default 
  # the value is script directory path)
  set origin_dir .

  # Use origin directory path location variable, if specified in the tcl shell
  if { [info exists ::origin_dir_loc] } {
     set origin_dir $::origin_dir_loc
  }

  set str_bd_folder [file normalize ${origin_dir}]
  set str_bd_filepath ${str_bd_folder}/${design_name}/${design_name}.bd

  # Check if remote design exists on disk
  if { [file exists $str_bd_filepath ] == 1 } {
     catch {common::send_msg_id "BD_TCL-110" "ERROR" "The remote BD file path <$str_bd_filepath> already exists!"}
     common::send_msg_id "BD_TCL-008" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0>."
     common::send_msg_id "BD_TCL-009" "INFO" "Also make sure there is no design <$design_name> existing in your current project."

     return 1
  }

  # Check if design exists in memory
  set list_existing_designs [get_bd_designs -quiet $design_name]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-111" "ERROR" "The design <$design_name> already exists in this project! Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-010" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Check if design exists on disk within project
  set list_existing_designs [get_files -quiet */${design_name}.bd]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-112" "ERROR" "The design <$design_name> already exists in this project at location:
    $list_existing_designs"}
     catch {common::send_msg_id "BD_TCL-113" "ERROR" "Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-011" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Now can create the remote BD
  # NOTE - usage of <-dir> will create <$str_bd_folder/$design_name/$design_name.bd>
  create_bd_design -dir $str_bd_folder $design_name
} else {

  # Create regular design
  if { [catch {create_bd_design $design_name} errmsg] } {
     common::send_msg_id "BD_TCL-012" "INFO" "Please set a different value to variable <design_name>."

     return 1
  }
}

current_bd_design $design_name

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
cern.ch:user:acqCore:1.0\
xilinx.com:ip:axi_perf_mon:5.0\
xilinx.com:ip:ddr4:2.2\
xilinx.com:ip:proc_sys_reset:5.0\
xilinx.com:ip:util_vector_logic:2.0\
xilinx.com:ip:xlconstant:1.1\
xilinx.com:ip:xlslice:1.0\
"

   set list_ips_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

##################################################################
# CHECK Modules
##################################################################
set bCheckModules 1
if { $bCheckModules == 1 } {
   set list_check_mods "\ 
AXI_MM_bidir_interface\
axi4lite_cheburashka_bridge\
reset_synchronizer\
"

   set list_mods_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following modules exist in the project's sources: $list_check_mods ."

   foreach mod_vlnv $list_check_mods {
      if { [can_resolve_reference $mod_vlnv] == 0 } {
         lappend list_mods_missing $mod_vlnv
      }
   }

   if { $list_mods_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following module(s) are not found in the project: $list_mods_missing" }
      common::send_msg_id "BD_TCL-008" "INFO" "Please add source files for the missing module(s) above."
      set bCheckIPsPassed 0
   }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set AXI_APM [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_APM ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {16} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.FREQ_HZ {200000000} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {0} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $AXI_APM
  set AXI_reg [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 AXI_reg ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {32} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.FREQ_HZ {62500000} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {0} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $AXI_reg
  set ddr4_0 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddr4_rtl:1.0 ddr4_0 ]

  # Create ports
  set PCIe_AXI_RD_burst_length [ create_bd_port -dir I -from 8 -to 0 PCIe_AXI_RD_burst_length ]
  set PCIe_AXI_RD_busy [ create_bd_port -dir O PCIe_AXI_RD_busy ]
  set PCIe_AXI_RD_data [ create_bd_port -dir O -from 255 -to 0 PCIe_AXI_RD_data ]
  set PCIe_AXI_RD_data_size [ create_bd_port -dir I -from 31 -to 0 PCIe_AXI_RD_data_size ]
  set PCIe_AXI_RD_data_valid [ create_bd_port -dir O PCIe_AXI_RD_data_valid ]
  set PCIe_AXI_RD_done [ create_bd_port -dir O PCIe_AXI_RD_done ]
  set PCIe_AXI_RD_error [ create_bd_port -dir O PCIe_AXI_RD_error ]
  set PCIe_AXI_RD_start [ create_bd_port -dir I PCIe_AXI_RD_start ]
  set PCIe_AXI_RD_start_address [ create_bd_port -dir I -from 31 -to 0 PCIe_AXI_RD_start_address ]
  set PCIe_AXI_WR_burst_length [ create_bd_port -dir I -from 8 -to 0 PCIe_AXI_WR_burst_length ]
  set PCIe_AXI_WR_busy [ create_bd_port -dir O PCIe_AXI_WR_busy ]
  set PCIe_AXI_WR_data_size [ create_bd_port -dir I -from 31 -to 0 PCIe_AXI_WR_data_size ]
  set PCIe_AXI_WR_done [ create_bd_port -dir O PCIe_AXI_WR_done ]
  set PCIe_AXI_WR_error [ create_bd_port -dir O PCIe_AXI_WR_error ]
  set PCIe_AXI_WR_start [ create_bd_port -dir I PCIe_AXI_WR_start ]
  set PCIe_AXI_WR_start_address [ create_bd_port -dir I -from 31 -to 0 PCIe_AXI_WR_start_address ]
  set PCIe_clk_i [ create_bd_port -dir I -type clk PCIe_clk_i ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {125000000} \
 ] $PCIe_clk_i
  set PCIe_debug_data_offset [ create_bd_port -dir I -from 3 -to 0 PCIe_debug_data_offset ]
  set acq_axi_busy_o [ create_bd_port -dir O acq_axi_busy_o ]
  set acq_axi_length_o [ create_bd_port -dir O -from 8 -to 0 acq_axi_length_o ]
  set acq_clk_i [ create_bd_port -dir I -type clk acq_clk_i ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {125000000} \
 ] $acq_clk_i
  set acq_start_trig_i [ create_bd_port -dir I -from 11 -to 0 acq_start_trig_i ]
  set busy_o [ create_bd_port -dir O busy_o ]
  set c0_ddr4_ui_clk [ create_bd_port -dir O -type clk c0_ddr4_ui_clk ]
  set c0_init_calib_complete_0 [ create_bd_port -dir O c0_init_calib_complete_0 ]
  set c0_sys_clk_n [ create_bd_port -dir I -type clk c0_sys_clk_n ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {125000000} \
 ] $c0_sys_clk_n
  set c0_sys_clk_p [ create_bd_port -dir I -type clk c0_sys_clk_p ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {125000000} \
 ] $c0_sys_clk_p
  set ch_0_i [ create_bd_port -dir I -from 31 -to 0 ch_0_i ]
  set ch_10_i [ create_bd_port -dir I -from 31 -to 0 ch_10_i ]
  set ch_11_i [ create_bd_port -dir I -from 31 -to 0 ch_11_i ]
  set ch_1_i [ create_bd_port -dir I -from 31 -to 0 ch_1_i ]
  set ch_2_i [ create_bd_port -dir I -from 31 -to 0 ch_2_i ]
  set ch_3_i [ create_bd_port -dir I -from 31 -to 0 ch_3_i ]
  set ch_4_i [ create_bd_port -dir I -from 31 -to 0 ch_4_i ]
  set ch_5_i [ create_bd_port -dir I -from 31 -to 0 ch_5_i ]
  set ch_6_i [ create_bd_port -dir I -from 31 -to 0 ch_6_i ]
  set ch_7_i [ create_bd_port -dir I -from 31 -to 0 ch_7_i ]
  set ch_8_i [ create_bd_port -dir I -from 31 -to 0 ch_8_i ]
  set ch_9_i [ create_bd_port -dir I -from 31 -to 0 ch_9_i ]
  set clk_i [ create_bd_port -dir I -type clk clk_i ]
  set_property -dict [ list \
   CONFIG.ASSOCIATED_BUSIF {AXI_reg} \
   CONFIG.FREQ_HZ {62500000} \
 ] $clk_i
  set irq_acq_error_o [ create_bd_port -dir O irq_acq_error_o ]
  set irq_acq_finished_o [ create_bd_port -dir O irq_acq_finished_o ]
  set sys_rst [ create_bd_port -dir I -type rst sys_rst ]
  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $sys_rst

  # Create instance: AXI_MM_bidir_interfa_0, and set properties
  set block_name AXI_MM_bidir_interface
  set block_cell_name AXI_MM_bidir_interfa_0
  if { [catch {set AXI_MM_bidir_interfa_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $AXI_MM_bidir_interfa_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
    set_property -dict [ list \
   CONFIG.g_M_AXI_DATA_WIDTH {256} \
 ] $AXI_MM_bidir_interfa_0

  # Create instance: acqCore_0, and set properties
  set acqCore_0 [ create_bd_cell -type ip -vlnv cern.ch:user:acqCore:1.0 acqCore_0 ]
  set_property -dict [ list \
   CONFIG.g_BUFFER_SIZE {134217728} \
   CONFIG.g_CHANNELS {12} \
   CONFIG.g_DSP_BLOCK {true} \
   CONFIG.g_FIFO_BURST {64} \
   CONFIG.g_MODE {external} \
 ] $acqCore_0

  # Create instance: axi4lite_cheburashka_0, and set properties
  set block_name axi4lite_cheburashka_bridge
  set block_cell_name axi4lite_cheburashka_0
  if { [catch {set axi4lite_cheburashka_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $axi4lite_cheburashka_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: axi_interconnect_0, and set properties
  set axi_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.ENABLE_ADVANCED_OPTIONS {1} \
   CONFIG.M00_HAS_DATA_FIFO {1} \
   CONFIG.NUM_MI {1} \
   CONFIG.NUM_SI {2} \
   CONFIG.S00_ARB_PRIORITY {15} \
   CONFIG.S00_HAS_DATA_FIFO {1} \
   CONFIG.S00_HAS_REGSLICE {3} \
   CONFIG.S01_HAS_DATA_FIFO {1} \
   CONFIG.S01_HAS_REGSLICE {3} \
   CONFIG.XBAR_DATA_WIDTH {512} \
 ] $axi_interconnect_0

  # Create instance: axi_perf_mon_0, and set properties
  set axi_perf_mon_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_perf_mon:5.0 axi_perf_mon_0 ]
  set_property -dict [ list \
   CONFIG.C_ENABLE_ADVANCED {0} \
   CONFIG.C_ENABLE_EVENT_COUNT {0} \
   CONFIG.C_ENABLE_PROFILE {1} \
   CONFIG.C_NUM_MONITOR_SLOTS {2} \
   CONFIG.C_REG_ALL_MONITOR_SIGNALS {0} \
   CONFIG.ENABLE_EXT_TRIGGERS {1} \
 ] $axi_perf_mon_0

  # Create instance: ddr4_0, and set properties
  set ddr4_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:ddr4:2.2 ddr4_0 ]
  set_property -dict [ list \
   CONFIG.C0.BANK_GROUP_WIDTH {1} \
   CONFIG.C0.DDR4_AxiAddressWidth {31} \
   CONFIG.C0.DDR4_AxiArbitrationScheme {ROUND_ROBIN} \
   CONFIG.C0.DDR4_AxiDataWidth {512} \
   CONFIG.C0.DDR4_CLKOUT0_DIVIDE {5} \
   CONFIG.C0.DDR4_CasLatency {11} \
   CONFIG.C0.DDR4_DataWidth {64} \
   CONFIG.C0.DDR4_InputClockPeriod {8000} \
   CONFIG.C0.DDR4_MemoryPart {EDY4016AABG-DR-F} \
   CONFIG.C0.DDR4_TimePeriod {1250} \
 ] $ddr4_0

  # Create instance: reset_synchronizer_0, and set properties
  set block_name reset_synchronizer
  set block_cell_name reset_synchronizer_0
  if { [catch {set reset_synchronizer_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $reset_synchronizer_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: rst_ddr4_0_200M, and set properties
  set rst_ddr4_0_200M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_ddr4_0_200M ]

  # Create instance: util_vector_logic_1, and set properties
  set util_vector_logic_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_1 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_1

  # Create instance: util_vector_logic_2, and set properties
  set util_vector_logic_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_2 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_2

  # Create instance: util_vector_logic_3, and set properties
  set util_vector_logic_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_3 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_3

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $xlconstant_0

  # Create instance: xlslice_0, and set properties
  set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {9} \
   CONFIG.DIN_TO {2} \
   CONFIG.DOUT_WIDTH {8} \
 ] $xlslice_0

  # Create interface connections
  connect_bd_intf_net -intf_net AXI_MM_bidir_interfa_0_M_AXI [get_bd_intf_pins AXI_MM_bidir_interfa_0/M_AXI] [get_bd_intf_pins axi_interconnect_0/S01_AXI]
connect_bd_intf_net -intf_net [get_bd_intf_nets AXI_MM_bidir_interfa_0_M_AXI] [get_bd_intf_pins AXI_MM_bidir_interfa_0/M_AXI] [get_bd_intf_pins axi_perf_mon_0/SLOT_1_AXI]
  connect_bd_intf_net -intf_net acqCore_0_M_AXI [get_bd_intf_pins acqCore_0/M_AXI] [get_bd_intf_pins axi_interconnect_0/S00_AXI]
connect_bd_intf_net -intf_net [get_bd_intf_nets acqCore_0_M_AXI] [get_bd_intf_pins acqCore_0/M_AXI] [get_bd_intf_pins axi_perf_mon_0/SLOT_0_AXI]
  connect_bd_intf_net -intf_net S_AXI_0_1 [get_bd_intf_ports AXI_APM] [get_bd_intf_pins axi_perf_mon_0/S_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M00_AXI [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins ddr4_0/C0_DDR4_S_AXI]
  connect_bd_intf_net -intf_net ddr4_0_C0_DDR4 [get_bd_intf_ports ddr4_0] [get_bd_intf_pins ddr4_0/C0_DDR4]
  connect_bd_intf_net -intf_net interface_aximm_0_1 [get_bd_intf_ports AXI_reg] [get_bd_intf_pins axi4lite_cheburashka_0/interface_aximm]

  # Create port connections
  connect_bd_net -net AXI_MM_bidir_interfa_0_AXI_RD_busy [get_bd_ports PCIe_AXI_RD_busy] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_RD_busy]
  connect_bd_net -net AXI_MM_bidir_interfa_0_AXI_RD_data [get_bd_ports PCIe_AXI_RD_data] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_RD_data]
  connect_bd_net -net AXI_MM_bidir_interfa_0_AXI_RD_data_valid [get_bd_ports PCIe_AXI_RD_data_valid] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_RD_data_valid]
  connect_bd_net -net AXI_MM_bidir_interfa_0_AXI_RD_done [get_bd_ports PCIe_AXI_RD_done] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_RD_done]
  connect_bd_net -net AXI_MM_bidir_interfa_0_AXI_RD_error [get_bd_ports PCIe_AXI_RD_error] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_RD_error]
  connect_bd_net -net AXI_MM_bidir_interfa_0_AXI_WR_busy [get_bd_ports PCIe_AXI_WR_busy] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_WR_busy]
  connect_bd_net -net AXI_MM_bidir_interfa_0_AXI_WR_done [get_bd_ports PCIe_AXI_WR_done] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_WR_done]
  connect_bd_net -net AXI_MM_bidir_interfa_0_AXI_WR_error [get_bd_ports PCIe_AXI_WR_error] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_WR_error]
  connect_bd_net -net AXI_RD_burst_length_0_1 [get_bd_ports PCIe_AXI_RD_burst_length] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_RD_burst_length]
  connect_bd_net -net AXI_RD_data_size_0_1 [get_bd_ports PCIe_AXI_RD_data_size] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_RD_data_size]
  connect_bd_net -net AXI_RD_start_0_1 [get_bd_ports PCIe_AXI_RD_start] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_RD_start]
  connect_bd_net -net AXI_RD_start_address_0_1 [get_bd_ports PCIe_AXI_RD_start_address] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_RD_start_address]
  connect_bd_net -net AXI_WR_burst_length_0_1 [get_bd_ports PCIe_AXI_WR_burst_length] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_WR_burst_length]
  connect_bd_net -net AXI_WR_data_size_0_1 [get_bd_ports PCIe_AXI_WR_data_size] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_WR_data_size]
  connect_bd_net -net AXI_WR_start_0_1 [get_bd_ports PCIe_AXI_WR_start] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_WR_start]
  connect_bd_net -net AXI_WR_start_address_0_1 [get_bd_ports PCIe_AXI_WR_start_address] [get_bd_pins AXI_MM_bidir_interfa_0/AXI_WR_start_address]
  connect_bd_net -net acqCore_0_acq_axi_busy_o [get_bd_ports acq_axi_busy_o] [get_bd_pins acqCore_0/acq_axi_busy_o]
  connect_bd_net -net acqCore_0_acq_axi_length_o [get_bd_ports acq_axi_length_o] [get_bd_pins acqCore_0/acq_axi_length_o]
  connect_bd_net -net acqCore_0_busy_o [get_bd_ports busy_o] [get_bd_pins acqCore_0/busy_o]
  connect_bd_net -net acqCore_0_cheb_RdData [get_bd_pins acqCore_0/cheb_RdData] [get_bd_pins axi4lite_cheburashka_0/ch_rd_data_i]
  connect_bd_net -net acqCore_0_cheb_RdDone [get_bd_pins acqCore_0/cheb_RdDone] [get_bd_pins axi4lite_cheburashka_0/ch_rd_done_i]
  connect_bd_net -net acqCore_0_cheb_WrDone [get_bd_pins acqCore_0/cheb_WrDone] [get_bd_pins axi4lite_cheburashka_0/ch_wr_done_i]
  connect_bd_net -net acqCore_0_irq_acq_error_o [get_bd_ports irq_acq_error_o] [get_bd_pins acqCore_0/irq_acq_error_o]
  connect_bd_net -net acqCore_0_irq_acq_finished_o [get_bd_ports irq_acq_finished_o] [get_bd_pins acqCore_0/irq_acq_finished_o]
  connect_bd_net -net Net [get_bd_pins axi_perf_mon_0/capture_event] [get_bd_pins axi_perf_mon_0/reset_event] [get_bd_pins axi_perf_mon_0/slot_0_ext_trig] [get_bd_pins axi_perf_mon_0/slot_0_ext_trig_stop] [get_bd_pins axi_perf_mon_0/slot_1_ext_trig] [get_bd_pins axi_perf_mon_0/slot_1_ext_trig_stop] [get_bd_pins xlconstant_0/dout]
  connect_bd_net -net acq_clk_i_1 [get_bd_ports acq_clk_i] [get_bd_pins acqCore_0/acq_clk_i]
  connect_bd_net -net acq_start_trig_i_1 [get_bd_ports acq_start_trig_i] [get_bd_pins acqCore_0/acq_start_trig_i]
  connect_bd_net -net axi4lite_cheburashka_0_ch_addr_o [get_bd_pins axi4lite_cheburashka_0/ch_addr_o] [get_bd_pins xlslice_0/Din]
  connect_bd_net -net axi4lite_cheburashka_0_ch_rd_mem_o [get_bd_pins acqCore_0/cheb_RdMem] [get_bd_pins axi4lite_cheburashka_0/ch_rd_mem_o]
  connect_bd_net -net axi4lite_cheburashka_0_ch_wr_data_o [get_bd_pins acqCore_0/cheb_WrData] [get_bd_pins axi4lite_cheburashka_0/ch_wr_data_o]
  connect_bd_net -net axi4lite_cheburashka_0_ch_wr_mem_o [get_bd_pins acqCore_0/cheb_WrMem] [get_bd_pins axi4lite_cheburashka_0/ch_wr_mem_o]
  connect_bd_net -net c0_sys_clk_n_1 [get_bd_ports c0_sys_clk_n] [get_bd_pins ddr4_0/c0_sys_clk_n]
  connect_bd_net -net c0_sys_clk_p_1 [get_bd_ports c0_sys_clk_p] [get_bd_pins ddr4_0/c0_sys_clk_p]
  connect_bd_net -net ch_0_i_1 [get_bd_ports ch_0_i] [get_bd_pins acqCore_0/ch_0_i]
  connect_bd_net -net ch_10_i_1 [get_bd_ports ch_10_i] [get_bd_pins acqCore_0/ch_10_i]
  connect_bd_net -net ch_11_i_1 [get_bd_ports ch_11_i] [get_bd_pins acqCore_0/ch_11_i]
  connect_bd_net -net ch_1_i_1 [get_bd_ports ch_1_i] [get_bd_pins acqCore_0/ch_1_i]
  connect_bd_net -net ch_2_i_1 [get_bd_ports ch_2_i] [get_bd_pins acqCore_0/ch_2_i]
  connect_bd_net -net ch_3_i_1 [get_bd_ports ch_3_i] [get_bd_pins acqCore_0/ch_3_i]
  connect_bd_net -net ch_4_i_1 [get_bd_ports ch_4_i] [get_bd_pins acqCore_0/ch_4_i]
  connect_bd_net -net ch_5_i_1 [get_bd_ports ch_5_i] [get_bd_pins acqCore_0/ch_5_i]
  connect_bd_net -net ch_6_i_1 [get_bd_ports ch_6_i] [get_bd_pins acqCore_0/ch_6_i]
  connect_bd_net -net ch_7_i_1 [get_bd_ports ch_7_i] [get_bd_pins acqCore_0/ch_7_i]
  connect_bd_net -net ch_8_i_1 [get_bd_ports ch_8_i] [get_bd_pins acqCore_0/ch_8_i]
  connect_bd_net -net ch_9_i_1 [get_bd_ports ch_9_i] [get_bd_pins acqCore_0/ch_9_i]
  connect_bd_net -net clk_i_0_1 [get_bd_ports PCIe_clk_i] [get_bd_pins AXI_MM_bidir_interfa_0/M_AXI_ACLK] [get_bd_pins axi_interconnect_0/S01_ACLK] [get_bd_pins axi_perf_mon_0/slot_1_axi_aclk] [get_bd_pins reset_synchronizer_0/clk_i]
  connect_bd_net -net clk_i_1 [get_bd_ports clk_i] [get_bd_pins acqCore_0/clk_i] [get_bd_pins axi4lite_cheburashka_0/ACLK]
  connect_bd_net -net ddr4_0_c0_ddr4_ui_clk [get_bd_ports c0_ddr4_ui_clk] [get_bd_pins acqCore_0/M_AXI_clk_i] [get_bd_pins axi_interconnect_0/ACLK] [get_bd_pins axi_interconnect_0/M00_ACLK] [get_bd_pins axi_interconnect_0/S00_ACLK] [get_bd_pins axi_perf_mon_0/core_aclk] [get_bd_pins axi_perf_mon_0/s_axi_aclk] [get_bd_pins axi_perf_mon_0/slot_0_axi_aclk] [get_bd_pins ddr4_0/c0_ddr4_ui_clk] [get_bd_pins rst_ddr4_0_200M/slowest_sync_clk]
  connect_bd_net -net ddr4_0_c0_ddr4_ui_clk_sync_rst [get_bd_pins ddr4_0/c0_ddr4_ui_clk_sync_rst] [get_bd_pins rst_ddr4_0_200M/ext_reset_in]
  connect_bd_net -net ddr4_0_c0_init_calib_complete [get_bd_ports c0_init_calib_complete_0] [get_bd_pins ddr4_0/c0_init_calib_complete]
  connect_bd_net -net debug_data_offset_0_1 [get_bd_ports PCIe_debug_data_offset] [get_bd_pins AXI_MM_bidir_interfa_0/debug_data_offset]
  connect_bd_net -net reset_rtl_0_1 [get_bd_ports sys_rst] [get_bd_pins acqCore_0/reset_i] [get_bd_pins ddr4_0/sys_rst] [get_bd_pins util_vector_logic_1/Op1]
  connect_bd_net -net reset_synchronizer_0_rst_sync_o [get_bd_pins acqCore_0/M_AXI_reset_i] [get_bd_pins reset_synchronizer_0/rst_sync_o] [get_bd_pins util_vector_logic_2/Op1]
  connect_bd_net -net rst_ddr4_0_200M_peripheral_aresetn [get_bd_pins axi_interconnect_0/ARESETN] [get_bd_pins axi_interconnect_0/M00_ARESETN] [get_bd_pins axi_interconnect_0/S00_ARESETN] [get_bd_pins axi_perf_mon_0/core_aresetn] [get_bd_pins axi_perf_mon_0/s_axi_aresetn] [get_bd_pins axi_perf_mon_0/slot_0_axi_aresetn] [get_bd_pins ddr4_0/c0_ddr4_aresetn] [get_bd_pins rst_ddr4_0_200M/peripheral_aresetn] [get_bd_pins util_vector_logic_3/Op1]
  connect_bd_net -net util_vector_logic_1_Res [get_bd_pins axi4lite_cheburashka_0/ARESETN] [get_bd_pins util_vector_logic_1/Res]
  connect_bd_net -net util_vector_logic_2_Res [get_bd_pins AXI_MM_bidir_interfa_0/M_AXI_ARESETN] [get_bd_pins axi_interconnect_0/S01_ARESETN] [get_bd_pins axi_perf_mon_0/slot_1_axi_aresetn] [get_bd_pins util_vector_logic_2/Res]
  connect_bd_net -net util_vector_logic_3_Res [get_bd_pins reset_synchronizer_0/rst_a_i] [get_bd_pins util_vector_logic_3/Res]
  connect_bd_net -net xlslice_0_Dout [get_bd_pins acqCore_0/cheb_Addr] [get_bd_pins xlslice_0/Dout]

  # Create address segments
  create_bd_addr_seg -range 0x80000000 -offset 0x00000000 [get_bd_addr_spaces AXI_MM_bidir_interfa_0/M_AXI] [get_bd_addr_segs ddr4_0/C0_DDR4_MEMORY_MAP/C0_DDR4_ADDRESS_BLOCK] SEG_ddr4_0_C0_DDR4_ADDRESS_BLOCK
  create_bd_addr_seg -range 0x80000000 -offset 0x00000000 [get_bd_addr_spaces acqCore_0/M_AXI] [get_bd_addr_segs ddr4_0/C0_DDR4_MEMORY_MAP/C0_DDR4_ADDRESS_BLOCK] SEG_ddr4_0_C0_DDR4_ADDRESS_BLOCK
  create_bd_addr_seg -range 0x00002000 -offset 0x00000000 [get_bd_addr_spaces AXI_reg] [get_bd_addr_segs axi4lite_cheburashka_0/interface_aximm/reg0] SEG_axi4lite_cheburashka_0_reg0
  create_bd_addr_seg -range 0x00010000 -offset 0x00000000 [get_bd_addr_spaces AXI_APM] [get_bd_addr_segs axi_perf_mon_0/S_AXI/Reg] SEG_axi_perf_mon_0_Reg


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


