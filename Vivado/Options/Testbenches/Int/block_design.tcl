
################################################################
# This is a generated script based on design: sim_Int
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2018.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source sim_Int_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# axi4lite_cheburashka_bridge

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xcku040-ffva1156-1-c
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name sim_Int

# This script was generated for a remote BD. To create a non-remote design,
# change the variable <run_remote_bd_flow> to <0>.

set run_remote_bd_flow 1
if { $run_remote_bd_flow == 1 } {
  # Set the reference directory for source file relative paths (by default 
  # the value is script directory path)
  set origin_dir .

  # Use origin directory path location variable, if specified in the tcl shell
  if { [info exists ::origin_dir_loc] } {
     set origin_dir $::origin_dir_loc
  }

  set str_bd_folder [file normalize ${origin_dir}]
  set str_bd_filepath ${str_bd_folder}/${design_name}/${design_name}.bd

  # Check if remote design exists on disk
  if { [file exists $str_bd_filepath ] == 1 } {
     catch {common::send_msg_id "BD_TCL-110" "ERROR" "The remote BD file path <$str_bd_filepath> already exists!"}
     common::send_msg_id "BD_TCL-008" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0>."
     common::send_msg_id "BD_TCL-009" "INFO" "Also make sure there is no design <$design_name> existing in your current project."

     return 1
  }

  # Check if design exists in memory
  set list_existing_designs [get_bd_designs -quiet $design_name]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-111" "ERROR" "The design <$design_name> already exists in this project! Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-010" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Check if design exists on disk within project
  set list_existing_designs [get_files -quiet */${design_name}.bd]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-112" "ERROR" "The design <$design_name> already exists in this project at location:
    $list_existing_designs"}
     catch {common::send_msg_id "BD_TCL-113" "ERROR" "Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-011" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Now can create the remote BD
  # NOTE - usage of <-dir> will create <$str_bd_folder/$design_name/$design_name.bd>
  create_bd_design -dir $str_bd_folder $design_name
} else {

  # Create regular design
  if { [catch {create_bd_design $design_name} errmsg] } {
     common::send_msg_id "BD_TCL-012" "INFO" "Please set a different value to variable <design_name>."

     return 1
  }
}

current_bd_design $design_name

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
cern.ch:user:acqCore:1.0\
xilinx.com:ip:xlslice:1.0\
"

   set list_ips_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

##################################################################
# CHECK Modules
##################################################################
set bCheckModules 1
if { $bCheckModules == 1 } {
   set list_check_mods "\ 
axi4lite_cheburashka_bridge\
"

   set list_mods_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following modules exist in the project's sources: $list_check_mods ."

   foreach mod_vlnv $list_check_mods {
      if { [can_resolve_reference $mod_vlnv] == 0 } {
         lappend list_mods_missing $mod_vlnv
      }
   }

   if { $list_mods_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following module(s) are not found in the project: $list_mods_missing" }
      common::send_msg_id "BD_TCL-008" "INFO" "Please add source files for the missing module(s) above."
      set bCheckIPsPassed 0
   }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set S00_AXI [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {32} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.FREQ_HZ {10000000} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {1} \
   CONFIG.HAS_CACHE {1} \
   CONFIG.HAS_LOCK {1} \
   CONFIG.HAS_PROT {1} \
   CONFIG.HAS_QOS {1} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {2} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {2} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $S00_AXI

  # Create ports
  set ACLK [ create_bd_port -dir I -type clk ACLK ]
  set_property -dict [ list \
   CONFIG.ASSOCIATED_BUSIF {S00_AXI:interface_aximm_0} \
   CONFIG.FREQ_HZ {10000000} \
 ] $ACLK
  set ARESETN [ create_bd_port -dir I -type rst ARESETN ]
  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_LOW} \
 ] $ARESETN
  set acq_clk_i [ create_bd_port -dir I -type clk acq_clk_i ]
  set acq_start_trig_i [ create_bd_port -dir I -from 0 -to 0 acq_start_trig_i ]
  set buff_0_i [ create_bd_port -dir I -from 31 -to 0 buff_0_i ]
  set buff_1_i [ create_bd_port -dir I -from 31 -to 0 buff_1_i ]
  set buff_2_i [ create_bd_port -dir I -from 31 -to 0 buff_2_i ]
  set buff_3_i [ create_bd_port -dir I -from 31 -to 0 buff_3_i ]
  set buff_4_i [ create_bd_port -dir I -from 31 -to 0 buff_4_i ]
  set buff_5_i [ create_bd_port -dir I -from 31 -to 0 buff_5_i ]
  set buff_6_i [ create_bd_port -dir I -from 31 -to 0 buff_6_i ]
  set buff_7_i [ create_bd_port -dir I -from 31 -to 0 buff_7_i ]
  set busy_o [ create_bd_port -dir O busy_o ]
  set clk_i [ create_bd_port -dir I -type clk clk_i ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {62500000} \
 ] $clk_i
  set irq_acq_error_o [ create_bd_port -dir O irq_acq_error_o ]
  set irq_acq_finished_o [ create_bd_port -dir O irq_acq_finished_o ]
  set reset_i [ create_bd_port -dir I -type rst reset_i ]
  set_property -dict [ list \
   CONFIG.POLARITY {ACTIVE_HIGH} \
 ] $reset_i

  # Create instance: acqCore_0, and set properties
  set acqCore_0 [ create_bd_cell -type ip -vlnv cern.ch:user:acqCore:1.0 acqCore_0 ]
  set_property -dict [ list \
   CONFIG.g_BUFFER_SIZE {8192} \
   CONFIG.g_CHANNELS {1} \
   CONFIG.g_DSP_BLOCK {true} \
   CONFIG.g_MODE {internal} \
 ] $acqCore_0

  # Create instance: axi4lite_cheburashka_0, and set properties
  set block_name axi4lite_cheburashka_bridge
  set block_cell_name axi4lite_cheburashka_0
  if { [catch {set axi4lite_cheburashka_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $axi4lite_cheburashka_0 eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create instance: axi_interconnect_0, and set properties
  set axi_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 axi_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.NUM_MI {2} \
 ] $axi_interconnect_0

  # Create instance: xlslice_0, and set properties
  set xlslice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice:1.0 xlslice_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {9} \
   CONFIG.DIN_TO {2} \
   CONFIG.DOUT_WIDTH {8} \
 ] $xlslice_0

  # Create interface connections
  connect_bd_intf_net -intf_net S00_AXI_0_1 [get_bd_intf_ports S00_AXI] [get_bd_intf_pins axi_interconnect_0/S00_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M00_AXI [get_bd_intf_pins axi4lite_cheburashka_0/interface_aximm] [get_bd_intf_pins axi_interconnect_0/M00_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M01_AXI [get_bd_intf_pins acqCore_0/S_AXI] [get_bd_intf_pins axi_interconnect_0/M01_AXI]

  # Create port connections
  connect_bd_net -net acqCore_0_busy_o [get_bd_ports busy_o] [get_bd_pins acqCore_0/busy_o]
  connect_bd_net -net acqCore_0_cheb_RdData [get_bd_pins acqCore_0/cheb_RdData] [get_bd_pins axi4lite_cheburashka_0/ch_rd_data_i]
  connect_bd_net -net acqCore_0_cheb_RdDone [get_bd_pins acqCore_0/cheb_RdDone] [get_bd_pins axi4lite_cheburashka_0/ch_rd_done_i]
  connect_bd_net -net acqCore_0_cheb_WrDone [get_bd_pins acqCore_0/cheb_WrDone] [get_bd_pins axi4lite_cheburashka_0/ch_wr_done_i]
  connect_bd_net -net acqCore_0_irq_acq_error_o [get_bd_ports irq_acq_error_o] [get_bd_pins acqCore_0/irq_acq_error_o]
  connect_bd_net -net acqCore_0_irq_acq_finished_o [get_bd_ports irq_acq_finished_o] [get_bd_pins acqCore_0/irq_acq_finished_o]
  connect_bd_net -net M01_ACLK_1 [get_bd_ports ACLK] [get_bd_pins acqCore_0/S_AXI_aclk] [get_bd_pins axi4lite_cheburashka_0/ACLK] [get_bd_pins axi_interconnect_0/ACLK] [get_bd_pins axi_interconnect_0/M00_ACLK] [get_bd_pins axi_interconnect_0/M01_ACLK] [get_bd_pins axi_interconnect_0/S00_ACLK]
  connect_bd_net -net M01_ARESETN_1 [get_bd_ports ARESETN] [get_bd_pins acqCore_0/S_AXI_aresetn] [get_bd_pins axi4lite_cheburashka_0/ARESETN] [get_bd_pins axi_interconnect_0/ARESETN] [get_bd_pins axi_interconnect_0/M00_ARESETN] [get_bd_pins axi_interconnect_0/M01_ARESETN] [get_bd_pins axi_interconnect_0/S00_ARESETN]
  connect_bd_net -net acq_clk_i_0_1 [get_bd_ports acq_clk_i] [get_bd_pins acqCore_0/acq_clk_i]
  connect_bd_net -net acq_start_trig_i_0_1 [get_bd_ports acq_start_trig_i] [get_bd_pins acqCore_0/acq_start_trig_i]
  connect_bd_net -net axi4lite_cheburashka_0_ch_addr_o [get_bd_pins axi4lite_cheburashka_0/ch_addr_o] [get_bd_pins xlslice_0/Din]
  connect_bd_net -net axi4lite_cheburashka_0_ch_rd_mem_o [get_bd_pins acqCore_0/cheb_RdMem] [get_bd_pins axi4lite_cheburashka_0/ch_rd_mem_o]
  connect_bd_net -net axi4lite_cheburashka_0_ch_wr_data_o [get_bd_pins acqCore_0/cheb_WrData] [get_bd_pins axi4lite_cheburashka_0/ch_wr_data_o]
  connect_bd_net -net axi4lite_cheburashka_0_ch_wr_mem_o [get_bd_pins acqCore_0/cheb_WrMem] [get_bd_pins axi4lite_cheburashka_0/ch_wr_mem_o]
  connect_bd_net -net buff_0_i_0_1 [get_bd_ports buff_0_i] [get_bd_pins acqCore_0/buff_0_i]
  connect_bd_net -net buff_1_i_0_1 [get_bd_ports buff_1_i] [get_bd_pins acqCore_0/buff_1_i]
  connect_bd_net -net buff_2_i_0_1 [get_bd_ports buff_2_i] [get_bd_pins acqCore_0/buff_2_i]
  connect_bd_net -net buff_3_i_0_1 [get_bd_ports buff_3_i] [get_bd_pins acqCore_0/buff_3_i]
  connect_bd_net -net buff_4_i_0_1 [get_bd_ports buff_4_i] [get_bd_pins acqCore_0/buff_4_i]
  connect_bd_net -net buff_5_i_0_1 [get_bd_ports buff_5_i] [get_bd_pins acqCore_0/buff_5_i]
  connect_bd_net -net buff_6_i_0_1 [get_bd_ports buff_6_i] [get_bd_pins acqCore_0/buff_6_i]
  connect_bd_net -net buff_7_i_0_1 [get_bd_ports buff_7_i] [get_bd_pins acqCore_0/buff_7_i]
  connect_bd_net -net clk_i_0_1 [get_bd_ports clk_i] [get_bd_pins acqCore_0/clk_i]
  connect_bd_net -net reset_i_0_1 [get_bd_ports reset_i] [get_bd_pins acqCore_0/reset_i]
  connect_bd_net -net xlslice_0_Dout [get_bd_pins acqCore_0/cheb_Addr] [get_bd_pins xlslice_0/Dout]

  # Create address segments
  create_bd_addr_seg -range 0x20000000 -offset 0x20000000 [get_bd_addr_spaces S00_AXI] [get_bd_addr_segs acqCore_0/S_AXI/reg0] SEG_acqCore_0_reg0
  create_bd_addr_seg -range 0x20000000 -offset 0x00000000 [get_bd_addr_spaces S00_AXI] [get_bd_addr_segs axi4lite_cheburashka_0/interface_aximm/reg0] SEG_axi4lite_cheburashka_0_reg0


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


