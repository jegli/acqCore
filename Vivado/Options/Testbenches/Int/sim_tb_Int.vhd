-------------------------------------------------------------------------------
-- --
-- CERN, BE-RF-FB, sim_tb_DataAcquisitonInt --
-- --
-------------------------------------------------------------------------------
--
-- unit name: sim_tb_DataAcquisitonInt
--
-- author: Julien Egli (julien.egli@cern.ch)
--
-- date: 09/07/2018
--
-- version: 1.0
--
-- description: Data acquisition (internal mode) simulation testbench
--
-- dependencies: 
--
-- references: <reference one>
-- <reference two> ...
--
-- modified by: $Author:: $:
--
-------------------------------------------------------------------------------
-- last changes: <date> <initials> <log>
-- <extended description>
-------------------------------------------------------------------------------
-- TODO: <next thing to do>
-- <another thing to do>
--
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 
use std.textio.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
--use ieee.math_real.all;

-- function generator package
library work;
use work.AXI_SIM_PACK.all;
use work.MemMap_acqCore.all;
use work.MemMap_ipInfo.all;
use work.ACQ_CORE_PACK.all;

library std;
use std.env.stop;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sim_tb_Int is

end sim_tb_Int;

architecture testbench of sim_tb_Int is
    --=============================================================================
    -- constant instantation 
    -- clock frequency
    constant c_CLK_PERIOD        : time := 16 ns;            --100MHz
    constant c_ACQ_CLK_PERIOD    : time := 8 ns;             --125MHz
    
    -- reset time
    constant c_RESET_TIME       : time := 20*c_CLK_PERIOD;
    
    -- start trigger timing
    constant c_START_TIME       : time := 500 ns;

    -- Data Acquisition IP core settings
    constant c_BUFFERS              : integer := 8;   -- [buffers]  
    constant c_BUFFER_SIZE          : integer := 8192;
    constant c_AXI_RAM_BASE_ADDR    : integer := 16#20000000#;     

    -- Acquisition simulation parameters
    constant c_ACQ_LENGTH               : integer := 8192; --524288;
    constant c_BUFFER_SIZE_IN_BYTES     : integer := c_BUFFER_SIZE*4; --524288;
    
    -- Data Acquistion core register default value
    constant c_DEFAULT_CONTROL_DEBUG        : integer := 16#04000007#; 
    constant c_DEFAULT_CONTROL_DEBUG_START  : integer := 16#04004004#; 
    constant c_DEFAULT_CONTROL_NORMAL       : integer := 16#00000007#; 
    constant c_DEFAULT_CONTROL_NORMAL_START : integer := 16#00004004#; 
    
    constant c_DEFAULT_FAULT_ENABLE     : integer := 16#00000001#;
    constant c_DEFAULT_ACQ_LENGTH       : integer := c_ACQ_LENGTH;
    constant c_DEFAULT_ACQ_DECIMATION   : integer := 10;

    -- signal instantation
    signal s_AXI_clk            : std_logic := '1';
    signal s_clk                : std_logic := '1';
    signal s_acq_clk            : std_logic := '1';
    signal s_AXI_resetn         : std_logic := '0';
    signal s_reset              : std_logic := '1';
    signal s_start_trig         : std_logic_vector(0 downto 0) := (others => '0');
    signal s_data               : t_STD_ARRAY_32(0 to c_BUFFERS-1) := (others => (others=> '0'));
    signal s_irq_acq_finished   : std_logic := '0';
    signal s_irq_acq_error      : std_logic := '0';
    signal s_buffer_data_cnt    : integer := 0;
    signal s_busy               : std_logic;  

    -- axi-lite slave interface
    signal s_AXI_WR_out     : t_AXI_WR_bus_out := (AWVALID =>'0',BREADY => '0',WVALID => '0',AWADDR => (others=>'0'),WDATA => (others=>'0'),WSTRB => (others=>'0'));
    signal s_AXI_WR_in      : t_AXI_WR_bus_in;
    signal s_AXI_RD_out     : t_AXI_RD_bus_out := (ARVALID=>'0',RREADY => '0',ARADDR => (others=>'0'));
    signal s_AXI_RD_in      : t_AXI_RD_bus_in;

    --=============================================================================
    -- component instantation
    
    component sim_Int_wrapper is
      port (
        ACLK : in STD_LOGIC;
        ARESETN : in STD_LOGIC;
        S00_AXI_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
        S00_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
        S00_AXI_arready : out STD_LOGIC_VECTOR ( 0 to 0 );
        S00_AXI_arvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
        S00_AXI_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
        S00_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
        S00_AXI_awready : out STD_LOGIC_VECTOR ( 0 to 0 );
        S00_AXI_awvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
        S00_AXI_bready : in STD_LOGIC_VECTOR ( 0 to 0 );
        S00_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
        S00_AXI_bvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
        S00_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
        S00_AXI_rready : in STD_LOGIC_VECTOR ( 0 to 0 );
        S00_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
        S00_AXI_rvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
        S00_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
        S00_AXI_wready : out STD_LOGIC_VECTOR ( 0 to 0 );
        S00_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
        S00_AXI_wvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
        acq_clk_i : in STD_LOGIC;
        acq_start_trig_i : in STD_LOGIC_VECTOR ( 0 downto 0 );
        buff_0_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        buff_1_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        buff_2_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        buff_3_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        buff_4_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        buff_5_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        buff_6_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        buff_7_i : in STD_LOGIC_VECTOR ( 31 downto 0 );
        busy_o : out STD_LOGIC;
        clk_i : in STD_LOGIC;
        irq_acq_error_o : out STD_LOGIC;
        irq_acq_finished_o : out STD_LOGIC;
        reset_i : in STD_LOGIC
      );
    end component sim_Int_wrapper;

    --=============================================================================
begin

sim_Int_wrapper_0: sim_Int_wrapper
  port map(
            ACLK => s_AXI_clk,
            ARESETN => s_AXI_resetn,

            S00_AXI_awaddr => s_AXI_WR_out.AWADDR,
            S00_AXI_awvalid(0) => s_AXI_WR_out.AWVALID,
            S00_AXI_awready(0) => s_AXI_WR_in.AWREADY,
            S00_AXI_wdata => s_AXI_WR_out.WDATA,
            S00_AXI_wstrb => s_AXI_WR_out.WSTRB,
            S00_AXI_wvalid(0) => s_AXI_WR_out.WVALID,
            S00_AXI_wready(0) => s_AXI_WR_in.WREADY,
            S00_AXI_bresp => s_AXI_WR_in.BRESP,
            S00_AXI_bvalid(0) => s_AXI_WR_in.BVALID,
            S00_AXI_bready(0) => s_AXI_WR_out.BREADY,
            S00_AXI_araddr => s_AXI_RD_out.ARADDR,
            S00_AXI_arvalid(0) => s_AXI_RD_out.ARVALID,
            S00_AXI_arready(0) => s_AXI_RD_in.ARREADY,
            S00_AXI_rdata => s_AXI_RD_in.RDATA,
            S00_AXI_rresp => s_AXI_RD_in.RRESP,
            S00_AXI_rvalid(0) => s_AXI_RD_in.RVALID,
            S00_AXI_rready(0) => s_AXI_RD_out.RREADY,
            S00_AXI_arprot => (others => '0'),
            S00_AXI_awprot => (others => '0'),
    
            acq_clk_i => s_acq_clk,    
            acq_start_trig_i => s_start_trig,
            
            buff_0_i => s_data(0),
            buff_1_i => s_data(1),
            buff_2_i => s_data(2),
            buff_3_i => s_data(3),
            buff_4_i => s_data(4),
            buff_5_i => s_data(5),
            buff_6_i => s_data(6),
            buff_7_i => s_data(7),
    
            busy_o => s_busy,
            clk_i => s_clk,
            irq_acq_finished_o => s_irq_acq_finished,
            irq_acq_error_o => s_irq_acq_error,
            reset_i => s_reset
        );

    --=============================================================================
    -- clock generation
    --=============================================================================
    s_AXI_clk   <= s_clk;
    s_clk       <= not s_clk        after c_CLK_PERIOD/2;
    s_acq_clk   <= not s_acq_clk    after c_ACQ_CLK_PERIOD/2;
    
    p_reset:process
    begin
        wait until s_clk = '1';
        s_reset     <= '1';
        wait for c_RESET_TIME;
        s_reset     <= '0';
        wait;
    end process;
    
    p_AXI_reset:process
    begin
        wait until s_AXI_clk = '1';
        s_AXI_resetn     <= '0';
        wait for c_RESET_TIME;
        s_AXI_resetn     <= '1';
        wait;
    end process;
        
    --=============================================================================
    -- generate data flow (only with external start trigger)
    --=============================================================================
--    process
--        variable v_data         : integer := 0;
--    begin
--        wait for 1 us;
        
--        wait until unsigned(s_start_trig) /= 0;
--        -- add 2 clock period delays
--        wait until s_acq_clk = '1';
--        wait until s_acq_clk = '1';
--        loop
--            v_data := v_data + 1;
--            for I in 0 to c_BUFFERS-1 loop
--                s_data(I) <= std_logic_vector(to_unsigned(v_data,s_data(0)'length));
--            end loop;
--            wait until s_acq_clk = '1';
--        end loop;
        
--        wait;
--    end process;
    
    process
    begin
        for I in 0 to c_BUFFERS-1 loop
            s_data(I) <= std_logic_vector(to_unsigned(I,s_data(0)'length));
        end loop;
        wait;
    end process;
    --=============================================================================
    -- DUT testbench
    --=============================================================================
    process
        variable v_data, v_addr : std_logic_vector (31 downto 0) := (others => '0');
        variable v_data_cnt : integer := 0;
        variable v_err,v_debug_failed,v_normal_failed      : boolean := false;
    begin

        if(c_BUFFER_SIZE < c_ACQ_LENGTH)then
            report "Acquisition length bigger than the buffer size! Please reduce acquisition length" severity error; 
            stop;
        end if;
        
        ------------------------------------------------------------
        report lf & "*******************************************" & lf &
                    "Reading the IP core information registers..."; -- severity note
        AXI_READ(C_Reg_ipInfo_firmwareVersion,v_data,s_AXI_RD_in,s_AXI_RD_out,s_clk); 
        if(unsigned(v_data) /= unsigned(c_FIRMWARE_VERSION))then
            report "IP firmware version error!" severity error;
            stop;
        else
            report "IP firmware version:" & integer'image(to_integer(unsigned(c_FIRMWARE_VERSION)));
        end if;
        
        AXI_READ(C_Reg_ipInfo_memMapVersion,v_data,s_AXI_RD_in,s_AXI_RD_out,s_clk); 
        if(unsigned(v_data) /= unsigned(c_MEM_MAP_VESRION))then
            report "IP memory map version error!" severity error;
            stop;
        else
            report "IP memory map version:" & integer'image(to_integer(unsigned(c_MEM_MAP_VESRION)));
        end if;

        --=============================================================================
        -- Testing the debug mode
        --=============================================================================
        report lf & "*****************************************" & lf &
                    "***         DEBUG MODE TESTING        ***" & lf &
                    "*****************************************";

        report "Initialize the registers..."; 
        -- Initializing main acq registers
        AXI_WRITE(C_Reg_acqCore_control,c_DEFAULT_CONTROL_DEBUG,s_AXI_WR_in,s_AXI_WR_out,s_AXI_clk);                   -- control register
        AXI_WRITE(C_Reg_acqCore_faultsEnable,c_DEFAULT_FAULT_ENABLE,s_AXI_WR_in,s_AXI_WR_out,s_AXI_clk); 
        
        -- Acquisition Lengths 
        AXI_WRITE(C_Reg_acqCore_acqLength0,c_DEFAULT_ACQ_LENGTH,s_AXI_WR_in,s_AXI_WR_out,s_AXI_clk);    -- buffer address register

        --decimation
        AXI_WRITE(C_Reg_acqCore_acqDecimation0,2,s_AXI_WR_in,s_AXI_WR_out,s_AXI_clk);    -- buffer address register

        wait for 1 us;
        report "starting new acquisition!"; -- severity note
        AXI_WRITE(C_Reg_acqCore_control,c_DEFAULT_CONTROL_DEBUG_START,s_AXI_WR_in,s_AXI_WR_out,s_AXI_clk); 

        report "Waiting the end of the acquisition...";
        
        while(v_data(14) /= '1')loop
            AXI_READ(C_Reg_acqCore_status,v_data,s_AXI_RD_in,s_AXI_RD_out,s_AXI_clk);  
        end loop;

        report "Acquisition successfully finished!";

        wait for 1 us;
        
        report "Reading out the acquisition data from BRAM memories...";
        
        for B in 0 to c_BUFFERS-1 loop
            report "Reading buffer " & integer'image(B) & "...";
            v_err := false;
            
            while v_data_cnt < c_ACQ_LENGTH loop
                -- Here addressing is in bytes
                v_addr := std_logic_vector(to_unsigned(c_AXI_RAM_BASE_ADDR+(B*c_BUFFER_SIZE_IN_BYTES)+(v_data_cnt*4),v_addr'length));
                AXI_READ(v_addr,v_data,s_AXI_RD_in,s_AXI_RD_out,s_AXI_clk);  
--                report "Value:" & integer'image(to_integer(unsigned(v_data))) & " Addr:" & integer'image(v_data_cnt);

                if to_integer(unsigned(v_data)) /= v_data_cnt  then
                    report "Missmatch: Value:" & integer'image(to_integer(unsigned(v_data)))& "excpected value:" & integer'image(v_data_cnt/4) &" Addr:" & integer'image(v_data_cnt) severity error;  
                    v_err := true;
                end if;
                v_data_cnt := v_data_cnt + 1;
            end loop;
            v_data_cnt := 0;
            if v_err = true then
                report "Corrupted data detected!" severity error;
                v_debug_failed := true;
            else
                report "No corrupted data detected!" severity note;
            end if;
        end loop;
        
        --=============================================================================
        -- Testing normal mode
        --=============================================================================
        report lf & "*****************************************" & lf &
                    "***        NORMAL MODE TESTING        ***" & lf &
                    "*****************************************";

        report "Initialize the registers..."; 
        -- Initializing main acq registers
        AXI_WRITE(C_Reg_acqCore_control,c_DEFAULT_CONTROL_NORMAL,s_AXI_WR_in,s_AXI_WR_out,s_AXI_clk);                   -- control register
        AXI_WRITE(C_Reg_acqCore_faultsEnable,c_DEFAULT_FAULT_ENABLE,s_AXI_WR_in,s_AXI_WR_out,s_AXI_clk); 
        
        -- Acquisition Lengths 
        AXI_WRITE(C_Reg_acqCore_acqLength0,c_DEFAULT_ACQ_LENGTH,s_AXI_WR_in,s_AXI_WR_out,s_AXI_clk);    -- buffer address register

        --decimation
        AXI_WRITE(C_Reg_acqCore_acqDecimation0,2,s_AXI_WR_in,s_AXI_WR_out,s_AXI_clk);    -- buffer address register

        wait for 1 us;
        report "starting new acquisition!"; -- severity note
        AXI_WRITE(C_Reg_acqCore_control,c_DEFAULT_CONTROL_NORMAL_START,s_AXI_WR_in,s_AXI_WR_out,s_AXI_clk); 

        report "Waiting the end of the acquisition...";
        
        while(v_data(14) /= '1')loop
            AXI_READ(C_Reg_acqCore_status,v_data,s_AXI_RD_in,s_AXI_RD_out,s_AXI_clk);  
        end loop;

        report "Acquisition successfully finished!";

        wait for 1 us;
        
        report "Reading out the acquisition data from BRAM memories...";
        
        for B in 0 to c_BUFFERS-1 loop
            report "Reading buffer " & integer'image(B) & "...";
            v_err := false;
            
            while v_data_cnt < c_ACQ_LENGTH loop
                -- Here addressing is in bytes
                v_addr := std_logic_vector(to_unsigned(c_AXI_RAM_BASE_ADDR+(B*c_BUFFER_SIZE_IN_BYTES)+(v_data_cnt*4),v_addr'length));
                AXI_READ(v_addr,v_data,s_AXI_RD_in,s_AXI_RD_out,s_AXI_clk);  
--                report "Value:" & integer'image(to_integer(unsigned(v_data))) & " Addr:" & integer'image(v_data_cnt);

                if to_integer(unsigned(v_data)) /= B  then
                    report "Missmatch: Value:" & integer'image(to_integer(unsigned(v_data)))& "excpected value:" & integer'image(B) &" Addr:" & integer'image(v_data_cnt) severity error;  
                    v_err := true;
                end if;
                v_data_cnt := v_data_cnt + 1;
            end loop;
            v_data_cnt := 0;
            if v_err = true then
                report "Corrupted data detected!" severity error;
                v_normal_failed := true;
            else
                report "No corrupted data detected!" severity note;
            end if;
        end loop;
        
        --=============================================================================
        
        report lf & "*****************************************" & lf &
                    "***        Simulation finished!       ***" & lf &
                    "*****************************************";
        
        if(v_debug_failed)then
            report "Debug mode test failed!" severity error; 
        else  
            report "Debug mode test passed!" severity note; 
        end if;     
        
        if(v_normal_failed)then
            report "Normal mode test failed!" severity error; 
        else  
            report "Normal mode test passed!" severity note; 
        end if;    
        report "*****************************************";  
           
        wait for 1 ms;

        stop;
        wait;
    end process;
    
end testbench;
