Build the testbench projects:

1) Go into Ext or Int folder (Ext: Ip core configured for external memory mode
(DDR4), Int: IP core configured for internal memory mode (BRAM).

2) Open Vivado 2018.2
3) In the Tcl Console, change the working directory to the script path
4) Execute the command: source ./build_project.tcl
5) The project is ready